module.exports = {

      TestingEnvironment: 'QAI',
    // TestingEnvironment: 'CITEST',
   // TestingEnvironment: 'CSO',
   //   TestingEnvironment: 'PROD',
   //  TestingEnvironment: 'CIDEV',
   NameofThebrowser: "chrome",

    shortWait: 2000,
    longWait: 5000,

    urlQAI: 'https://qai.ibenefitcenter.com/QAWORK/login.tpz',
    // urlQAI: 'https://qai.ibenefitcenter.com/PBSPHL/login.tpz',
    urlCITEST: 'https://ctesti.ibenefitcenter.com/SLSDM/login.tpz',
    urlCSO: 'https://csoi.ibenefitcenter.com/SLSDM/login.tpz',
    urlPROD: 'https://ibenefitcenter2.mercerhrs.com/SLSDM/login.tpz',
    urlCIDEV: 'https://cdevi.ibenefitcenter.com/login.tpz',


    usersQAI: {
        IBCUSER: {
            username: 'test4988', password: 'test0001',
            username1: 'PBSPHL3738', password1: 'PBSPHLpass1'
        },
    },
    usersCITEST: {
        IBCUSER: {
            username: 'testqa1201', password: 'test0001' },
    },
    usersCSO: {
        IBCUSER: {
            username: 'testqa1003', password: 'test0001' },
    },
    usersPROD: {
        IBCUSER: {
            username: 'testqa1201', password: 'test0001' },
    },
    usersCIDEV: {
        IBCUSER: {
            username: 'testqa1002', password: 'Test0001' },
    },

    // QAI Environment - Test data for Resetting Password
    SSN : 4988,
    SSNforProxyQAI: 892670507,  // QAI Environment - Client - PBSPHL
    LastName : "LNAME4988",
    DOB : '10/12/1963',
    PostalCode : '02062',
    NewPassword: 'test0002',
    DefaultPassword: 'test0001',

    SSNforProxyCIDEV: 200001201,  // CIDEV Environment


};
