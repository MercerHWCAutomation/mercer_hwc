module.exports = {

    TestingEnvironment: 'QA',
    //TestingEnvironment: 'SANDBOX',
    //TestingEnvironment: 'STRESS',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',

    NameofThebrowser: "internet explorer",

    miniWait: 2000,
    shortWait: 5000,
    longWait: 10000,

    url:{
        QA:   'http://usfkl13as460v/ESR/#/',
        SANDBOX:  'http://usfkl13as460v/ESRUI/#/',
        STRESS:  'http://esr.mrshmc.com/#/home',
        CITEST: 'http://usfkl14as210v/ESR/#/',
        PROD: 'http://usfkl14as215v/ESR/#/'
    }

};


