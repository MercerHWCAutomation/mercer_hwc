module.exports = {

    TestingEnvironment: 'QAI',
   // TestingEnvironment: 'CITEST',
   // TestingEnvironment: 'CSO',
    // TestingEnvironment: 'CIDEV',
   // TestingEnvironment: 'PROD',

    NameofThebrowser: "chrome",
    // NameofThebrowser: "internet explorer",

    shortpause: 3000,
    averagepause: 15000,
    longpause: 30000,
    shortwaittime: 17000,
    averagewaittime: 40000,
    longwaittime: 80000,

    urlQAI: 'https://afcore-qai.mercer.com/home.tpz',
    urlCITEST: 'https://afcore-ctesti.mercer.com/home.tpz',
    urlCSO: 'https://afcore-csoi.mercer.com/home.tpz',
    urlCIDEV: 'http://afcore-cdevi.mercer.com/home.tpz',
    urlPROD: 'https://af-us.mercer.com/home.tpz',

    // QAI Environment - Test data
    SSN : 555000016,  // For Participant Search
    ClientName: "QAWORK",    // For Client Search
    AdministratorName: "Kolte, Nishant",   // For Administrator Search
    SelectSSOConfiguration: "AFToQAWORK",   // For External SSO Test
    RedirectedURL: "https://qai.ibenefitcenter.com/QAWORK"  // For External SSO Test

 };
