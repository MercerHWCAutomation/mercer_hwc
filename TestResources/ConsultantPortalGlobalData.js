/**
 * Created by subhajit-chakraborty on 4/25/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    TestingEnvironment: 'QAF',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',
    //TestingEnvironment: 'CSO',


    shortWait: 5000,
    longWait: 10000,
    LoginWait: 30000,
    AverageWait: 15000,
    ProfileCreationTime:60000,

    //Environment User details

    //QAF
    BrokerPortalURL: 'https://consultant-qaf.mercermarketplace365plus.com',
    BrokerPortalUsername: 'qa11.triveni.gherde@gisqa.mercer.com',
    BrokerPortalPassword: 'Welcome123@',
    //BrokerPortalUsername: 'qa41.triveni.gherde@gisqa.mercer.com',
    //BrokerPortalPassword: 'Welcome1!',

    //Prod
    //BrokerPortalURL: 'https://consultant.mercermarketplace365plus.com',
    //BrokerPortalUsername: 'Barbara.meldrum@analystmercer.com',
    //BrokerPortalPassword: 'Mercer2!',

    //CSO
    //BrokerPortalURL: 'https://consultant-cso.mercermarketplace365plus.com/Account/Login',
    //BrokerPortalUsername: '4.arun.rajendiran@gisqa.mercer.com',
    //BrokerPortalPassword: 'Mercer06',

    //CITest
    //BrokerPortalURL: 'https://consultant-cit.mercermarketplace365plus.com/Account/Login',
    //BrokerPortalUsername: 'qacit.triveni.gherde@gisqa.mercer.com',
    //BrokerPortalPassword: 'Mercer06',

    //Client Wizard View:
    ClientName: 'Client1',
    Address1USHeadQuarter: '15th Street',
    CityUSHeadquarter: 'Arizona',
    ZipCodeUSHeadquarter: '96354',

    //HR Details
    HRName: 'DemoHR',
    HREmail: 'hr1@abc.com',
    HRContactNumber: '9654123870',

    //Client Profile 2nd Page
    EnrollmentStartDate: '08/01/2017',
    PlanYearEffDate: '09/01/2017',
    FederalTaxID: '658792541',
    ClientOneCode: '659823',

    //Payroll Freequency
    WeeklyDate: '08/01/2017',
    PendingCobraMember: '1',
    EnrolledCobraMem: '1',











}
