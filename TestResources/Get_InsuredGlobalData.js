/**
 * Created by subhajit-chakraborty on 7/6/2017.
 */

module.exports = {

    //Specify Your Testing Environment

    TestingEnvironment: 'PROD',

    FooterPara1: "Mercer Marketplace is provided by Mercer Health & Benefits Administration LLC. This website is owned by Mercer Health & Benefits Administration LLC. The Centers for Medicare & Medicaid Services has not endorsed the information contained in this website.",
    FooterPara2: "The HRA contribution listed on this website reflects your 2017 HRA contribution at the beginning of the plan year and may not reflect your current balance. Most recent HRA Account contribution, balance, and activity can be viewed by clicking on the HRA Retiree Account link at the top of the page. You will then need to input your HRA Account Username and Password. Please refer to your employer HRA plan document for a complete description of eligibility and benefits.",

   //Non Gated Data for Exchange flows:
    Zipcode: 60601,
    Month: 10,
    Day: 07,
    Year: 1965,
    HouseholdIncomeValue: 40000,



    shortWait: 5000,
    longWait: 10000,
    LoginWait: 30000,


}
