module.exports = {
    sections: {
        HomePage: {
            selector: 'body',
            elements: {
                HomeTab: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Home")]/../../a'
                },
                ParticipantsTab : {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Participants")]/../../a'
                },
                ClientsTab : {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Clients")]/../../a'
                },
                AdministrationTab : {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Administration")]/../../a'
                },
                Administrationheading: {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Administration Foundation")]'
                },
                WelcomeTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Welcome")]'
                },
                RolesandPermissiontext : {locateStrategy: 'xpath',
                    selector: '//td[@class="partStyle"]'
                },
            }
        },
        ClientsPage: {
            selector: 'body',
            elements: {
                ClientSearchTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client Search")]'
                },
            }
        },
        ClientsSearchPage: {
            selector: 'body',
            elements: {
                inputClientName : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Client Name")]/../input'
                },
                Searchbutton : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Client Name")]/../input/../../../../../table[2]/tbody/tr[2]/td/b/input'
                },
                SearchbuttonPROD : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Client Name")]/../input/../../../../../table[2]/tbody/tr[2]/td/input'
                },

            }
        },
        ClientsResultsPage: {
            selector: 'body',
            elements: {
                ClientSearchPageTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client Search")]'
                },
                ClientSearchSubTitle : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Search Result(s)")]'
                },
                Clientnamelink : {locateStrategy: 'xpath',
                    selector: '//*[text()="Client Name"]/../../..//td[2]/a'
                },
                OneCodeValue : {locateStrategy: 'xpath',
                    selector: '//*[text()="Client Name"]/../../..//td[1]'
                },

            }
        },
        ClientSummaryPage: {
            selector: 'body',
            elements: {
                ProfileSummaryTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Profile Summary")]'
                },
                LOBTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Line of Business")]'
                },
                FeaturesTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Features and Services")]'
                },
                ParticipantAuthenticTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Participant Authentication Rules")]'
                },
                RecentContactTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Recently Added Contacts")]'
                },
                ClientNameTitleInSummaryPage : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client Name")]'
                },
                ClientNameValueInSummaryPage : {locateStrategy: 'xpath',
                    selector: '(//*[text()="Client Name"]/../../..//td)[2]'
                },
                OneCodeValueInSummaryPage : {locateStrategy: 'xpath',
                    selector: '//*[text()="Client Name"]/../../../tr[2]/td[2]'
                },
                OneCodeTitleInSummaryPage : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"One Code")]'
                },
                AddressTitleInSummaryPage : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Address")]'
                },
                AddressValueInSummaryPage : {locateStrategy: 'xpath',
                    selector: '(//*[text()="Client Name"]/../../..//td)[10]'
                },
                ViewDetailslinkInSummaryPage : {locateStrategy: 'xpath',
                    selector: '//span[text()="Features and Services"]/../../..//td[2]/a'
                },

                FeaturesandServicesheader : {locateStrategy: 'xpath',
                    selector: '//span[text()="Features and Services"]'
                },
                AssignedFeaturesandServicesheader : {locateStrategy: 'xpath',
                    selector: '//span[text()="Assigned Features And Services"]'
                },
                AvailableFeaturesandServicesheader : {locateStrategy: 'xpath',
                    selector: '//span[text()="Available Features And Services"]'
                },
                Impersonatebtn : {locateStrategy: 'xpath',
                    selector: '//*[@value="Impersonate"]'
                },



                LOBColumnName : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Line of Business")]'
                },
                ClientorControlIdColumnName : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client ID")]'
                },
                ApplicationColumnName : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client ID")]'
                },
                ApplicationEnabledColumnName : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Application Enabled")]'
                },
                ApplicationLiveDateColumnName : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Application Live Date")]'
                },
                LOBSecondrowFirstColumnValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryLOBApplications"]/tbody/tr[2]/td[1]/a'
                },
                ClientIdSecondrowSecondColumnValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryLOBApplications"]/tbody/tr[2]/td[2]/span'
                },
                ApplicationSecondrowThirdColumnValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryLOBApplications"]/tbody/tr[2]/td[3]/a'
                },
                ApplicationSecondrowFourthColumnValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryLOBApplications"]/tbody/tr[2]/td[4]/a'
                },
                ApplicationNameThirdrowFirstColumnValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryLOBApplications"]/tbody/tr[3]/td[1]/a'
                },
                ClientIdThirdrowSecondColumnValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryLOBApplications"]/tbody/tr[2]/td[2]/span'
                },
                AuthenticationLink: {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryServicingOptions"]/tbody/tr/td//a[contains(text(),"Authentication")]'
                },
                SecondLink: {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryServicingOptions"]/tbody/tr/td/li[3]/a'
                },
                WebsiteTitle : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[1]/td[1]/span'
                },
                WebsiteValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[1]/td[2]'
                },
                IVRValidationTitle : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[2]/td[1]/span'
                },
                IVRValidationValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[2]/td[2]'
                },
                WebUsernameFormatTitle : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[4]/td[1]/span'
                },
                WebUsernameFormatValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[4]/td[2]'
                },
                WebPasswordFormatTitle : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[6]/td[1]/span'
                },
                WebPasswordFormatValue : {locateStrategy: 'xpath',
                    selector: '//table[@id="ClientSummaryAuthenticationRules"]/tbody/tr[6]/td[2]'
                },
            }
        },
        ParticipantSearchPage: {
            selector: 'body',
            elements: {
                Titleparticipantsearch : {locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Enter the search")]/../../tr[1]/td/table/tbody/tr/td/span'
                },
                inputsocialid : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/../../td[2]/input'
                },
                inputlastname : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Last Name")]/../../td[2]/input'
                },
                inputFirstname: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"First Name")]/../../td[2]/input'
                },


                inputalternativeusername: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Alternate User Name")]/../../td[2]/input'
                },
                btnSearchbySocialId : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/../../td[2]/b/input'
                },
                btnSearchbySocialIdPROD : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/../../td[2]/input[2]'
                },
                btnSearchbyAlternativeUsername: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Alternate User Name")]/../../td[2]/b/input'
                },


                btnSearchByLastname: {selector: 'tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(4) > td > table > tbody > tr:nth-child(11) > td:nth-child(2) > b > input[type="button"]'
                },
            }
        },
        ParticipantResultsPage: {
            selector: 'body',
            elements: {
                TitleParticipantCode : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Participant Code")]'
                },
                SearchTitlebySocialID : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/..'
                },
                TitleParticipantStatus : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Participant Status")]'
                },
                TitleClientName : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Client Name")]'
                },
                Titlesearchcriteria : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Search Criteria")]/../../span[3]'
                },
                TitleAlternativeUserName : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Alternate User Name")]'
                },
                TitleFirstNameinParticipantResults : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"First Name")]'
                },
                TitleLastNameinParticipantResults : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Last Name:")]'
                },
                NoResultsMessage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"There are no rows which match your search criteria")]'
                },

                linkParticipantCodeforQAWORK : {locateStrategy: 'xpath',
                    selector: '//*[text()="QAWORK"]/../td/a'
                },
                linkParticipantCodeforSLSDM : {locateStrategy: 'xpath',
                    selector: '//*[text()="SLSDM"]/../td/a'
                },

                linkParticipantCodeforanyotherclients : {locateStrategy: 'xpath',
                    selector: '//*[text()="Active"]/../td/a'
                },

                FirstSocialId: {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[2]/td[2]'
                },
                RowOneFirstandLastName: {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[2]/td[3]'
                },
                FirstParticipantStatus : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[2]/td[4]'
                },
                FirstClientName : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[2]/td[5]'
                },
                linkSecondParticipantCode : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[1]/a'
                },
                SecondSocialId: {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[2]'
                },
                SecondRowFirstandLastName: {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[3]'
                },
                SceondParticipantStatus : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[4]'
                },
                SecondClientName : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[5]'
                },

            }
        },
        ParticipantSummaryPage: {
            selector: 'body',
            elements: {
                linkWebLoginManagement : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Web Login Management")]'
                },
                TitleParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Participant Profile")]'
                },
                NameTitleinParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Name")]'
                },
                ClientTitleinParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client")]'
                },
                SSNTitleinParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"SSN")]'
                },
                DOBTitleinParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Date of Birth")]'
                },
                AddressTitleinParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Address")]'
                },
                EmailTitleinParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Email")]'
                },
                NameValueInParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//*[text()="Name"]/../../td[2]'
                },
                ClientValueInParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//*[text()="Client"]/../../td[2]'
                },
                SSNValueInParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//*[text()="SSN"]/../../td[2]'
                },
                DOBValueInParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//*[text()="Date of Birth"]/../../td[2]'
                },
                AddressValueInParticipantProfile : {locateStrategy: 'xpath',
                    selector: '//*[text()="Address"]/../../td[2]'
                },

                TitleParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Participant Web Access")]'
                },
                LoBTitleinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"LOB")]'
                },
                AccessAllowedTitleinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Access Allowed")]'
                },
                DefinedBenefitsTitleinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Defined Benefits")]'
                },
                DefinedContributionsTitleinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Defined Contributions")]'
                },
                HealthandBenefitsTitleinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Health and Benefits")]'
                },
                SingleSignOnTitleinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Single Sign On")]'
                },
                DefinedBenefitsValueinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Defined Benefits")]/../td[2]'
                },
                HealthandBenefitsValueinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Health and Benefits")]/../td[2]'
                },
                SingleSignOnValueinParticipantWebAccess : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Single Sign On")]/../td[2]'
                },


                TitleWebLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Web Login Management")]'
                },
                LastsuccessfulloginTitleinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Last successful login")]'
                },
                LastsuccessfulloginValueinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Last successful login")]/../../td[2]'
                },
                ValidationMethodTitleinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Validation method")]'
                },
                ValidationMethodValueinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Validation method")]/../../td[2]'
                },

                AlternateUsernameTitleinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Alternate User Name")]'
                },
                AlternateUsernameValueinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Alternate User Name")]/../../td[2]'
                },

                ChallengeQuestionsTitleinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Challenge Questions")]'
                },
                ChallengeQuestionsValueinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Challenge Questions")]/../../td[2]'
                },
                IntialUsernameTitleinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Initial User Name Value")]'
                },
                IntialUsernameValueinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Initial User Name Value")]/../../td[2]'
                },
                IntialPasswordValueTitleinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Initial Password Value")]'
                },
                IntialPasswordValueinLoginManagement : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Initial Password Value")]/../../td[2]'
                },

                TitleAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Administration History")]'
                },
                EventDateTitleinAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Event Date")]'
                },
                EventTypeTitleinAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Event Type")]'
                },
                EventIntialtedByTitleinAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Event Initiated By")]'
                },
                FirstEventDateinAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[1]'
                },
                FirstEventTypeinAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[2]/a'
                },
                FirstEventIntiatedinAdministrationHistory : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[3]'
                },

            }
        },
        WebLoginManagementPage: {
            selector: 'body',
            elements: {
                NameTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[1]/td[1]/span'
                },
                NameValueinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[1]/td[2]'
                },
                ClientTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[1]/span'
                },
                ClientValueinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[2]'
                },
                AddressTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[3]/td[1]/span'
                },
                AddressValueinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[3]/td[2]'
                },
                SSNTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[1]/td[3]/span'
                },
                SSNValueinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[1]/td[4]'
                },
                EmailTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[3]/span'
                },
                SSNValueinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//table[@class="dataTable"]/tbody/tr[2]/td[4]'
                },
                AuthenticationTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Authentication Credentials")]'
                },
                DescriptionTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Description")]'
                },
                CurrentValueTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Current Value")]'
                },
                CurrentValueforUsernameinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"User Name")]/../../../td[2]/span'
                },
                UsernameTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"User Name")]/../../b/span'
                },
                PasswordTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Password")]/../../b/span'
                },
                CurrentValueforPasswordinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Password")]/../../../td[2]/span'
                },
                ChallengeQuestionsTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Challenge Questions")]/../../b/span'
                },
                CurrentValueforChallengeQuestionsinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Challenge Questions")]/../../../td[2]/span'
                },
                AdministrationActionsTitleinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Administrative Actions")]'
                },
                WebLoginManagementTitle : {locateStrategy: 'xpath',
                    selector: '//title[contains(text(),"Participant Web Login Management Landing Page")]'
                },
                WebPasswordLinkinWebLoginManagementPage : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Issue Temp Web Password")]'
                },
                WebLoginManagementLink : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Web Login Management")]'
                },

            }
        },
        ApplicationLandingPage: {
            selector: 'body',
            elements: {
                ApplicationsTitleinAdministrationLandingPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Welcome to the Applications")]'
                },
                ViewlogsLinkinAdministrationLandingPage : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"View Logs")]'
                },
                LogViewerheader : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Log Viewer")]'
                },
                linkManagementAdministrators : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Manage Administrators")]'
                },

            }
        },
        ViewLogsPage: {
            selector: 'body',
            elements: {
                ViewLogsTitle : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Log Viewer")]'
                },
                SelectFromDate : {locateStrategy: 'xpath',
                    selector: '//td[text()="From Date:"]/../td[2]/select/option[8]'
                },

                SelectToDate : {locateStrategy: 'xpath',
                    selector: '//td[text()="To Date:"]/../td[2]/select/option[contains(text(),"Tomorrow")]'
                },

                SelectClientforQAWORK : {locateStrategy: 'xpath',
                    selector: '//td[text()="Client/User:"]/../td[2]/select/option[contains(text(),"QAWORK")]'
                },

                SelectClientforSLSDM : {locateStrategy: 'xpath',
                    selector: '//td[text()="Client/User:"]/../td[2]/select/option[contains(text(),"SLSDM")]'
                },

                SelectFiltersforAudit : {locateStrategy: 'xpath',
                    selector: '//td[text()="Filters"]/../td[2]/select/option[contains(text(),"Audit Messages")]'
                },

                btnSearchforViewLogs : {locateStrategy: 'xpath',
                    selector: '//td[text()="Filters"]/../../tr[5]/td/b/input'
                },
                btnSearchforViewLogsPROD : {locateStrategy: 'xpath',
                    selector: '//td[text()="Filters"]/../../tr[5]/td/input'
                },
                LogMessageHeader : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Log Message")]'
                },
                LogResults : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Log Message")]/../../tr[2]/td/span[2]//label[@class="RedHighlight"]'
                },










            }
        },
        ManageAdministrators: {
            selector: 'body',
            elements: {
                TtileManagementAdministrators : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Manage Administrators")]'
                },

                InputLookforfield : {locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Look for:")]/../../tr/td[2]/input'
                },
                SearchInDropDown : {locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Search In:")]/../../tr/td[4]//select/option[contains(text(),"Name (Last, First)")]'
                },

                btnFindNow : {locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Look for:")]/../../tr/td[5]/b/input'
                },
                btnFindNowPROD : {locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Look for:")]/../../tr/td[5]/input'
                },

                AdministratorSearchResultslink : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[2]/td[1]/a'
                },

                ClientnameinAdministratorinLandingPage : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"User ID:")]/../../h3'
                },
                GroupsinAdministratorinLandingPage : {locateStrategy: 'xpath',
                    selector: ' //*[contains(text(),"This administrator is currently assigned to the following groups")]'
                },
                RolesinAdministratorinLandingPage : {locateStrategy: 'xpath',
                    selector: ' //*[contains(text(),"This administrator is currently assigned to the following roles")]'
                },

            }
        },
        ExternalSSOTestPage: {
            selector: 'body',
            elements: {
                ExternalSSOTestLink : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"External SSO Test")]'
                },
                ExternalTestHarnessHeader : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"External Test Harness")]'
                },
                SelectSSOforQAWORK : {locateStrategy: 'xpath',
                    selector: '//td[text()="Select SSO Configuration"]/../td[2]/select/option[contains(text(),"AFToQAWORK")]'
                },
                TestSSObtn : {locateStrategy: 'xpath',
                    selector: '//*[@value="Test SSO"]'
                },
            }
        },
        EndImpersonationPage: {
            selector: 'body',
            elements: {
                EndImpersonationbtn : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"End Impersonation")]'
                },
            }
        },



    }
};
