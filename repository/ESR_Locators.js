﻿module.exports = {
    sections: {

        //ESR locators
        ESRHomePage: {
            selector: 'body',
            elements: {
                pageTitle: {locateStrategy: 'xpath', selector: '//a[contains(text(),"ESR")]'},
                pageHeadingHome: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Home")]'},

                linkClientSLS: {locateStrategy: 'xpath', selector: '//a[contains(text(),"SLSDM")]'},
                linkClientQAACME: {locateStrategy: 'xpath', selector: '//a[contains(text(),"QAACME")]'},

                linkClientESRTST: {locateStrategy: 'xpath', selector: '//a[contains(text(),"ESRTST")]'},
                linkClientChildR: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Hospital of Philadelphia")]'},

                linkClientAOLBAA: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Applied Materials")]'},
                linkClientACISTL: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Arch Coal, Inc.")]'},

                titleSLSDMPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"SLSDM")]'},
                titleQAACMEPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"QAACME")]'},

                titleESRTSTPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"ESRTST")]'},
                titleChildRPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"CHILDR")]'},

                titleAOLBAAPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"AOLBAA")]'},
                titleACISTLPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"ACISTL")]'},

               // tabReports: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Reports")]/..)[3]'},
                tabReports: {locateStrategy: 'xpath', selector: '(//dl[@class="link-list-horz col-24 inpage-navbar"])[1]//a[contains(text(),"Reports")]'},
              //  tabDataFiles: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Data Files")]/..)[1]'},
                tabDataFiles: {locateStrategy: 'xpath', selector: '(//dl[@class="link-list-horz col-24 inpage-navbar"])[1]//a[contains(text(),"Data Files")]'},
                //Feedback tab from Reports Tab
                tabFeedbackFiles1: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Feedback Files")])[4]'},
                //Feedback tab from Data Files Tab
                tabFeedbackFiles2: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Feedback Files")])[3]'},
                //Feedback tab generic
                tabFeedbackFiles: {locateStrategy: 'xpath', selector: '(//dl[@class="link-list-horz col-24 inpage-navbar"])[1]//a[contains(text(),"Feedback Files")]'},
                tabReportingYears: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Reporting Years")])[2]'},
                tabClientDetails: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Client Details")])[2]'},

                titleDataFiles: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Data Files")]'},
                titleReports: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Reports")]'},
                titleFeedbackFiles: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Feedback Files")]'},
                titleReportingYears: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Reporting Years")]'},
                titleClientDetails: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Client Details")]'},

                linkUploadFile: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Upload File")]'},
                linkUploadBWFile: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Upload BW File")]'},

                titlePopupUploadFile: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Upload File")]'},
                buttonUploadFile: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Upload File")]'},
                buttonFileUpload: { selector: '#buttonUpload'},
                buttonCancelFile: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[3]'},
                buttonBrowseFile: {locateStrategy: 'xpath', selector: '//input[@id="upload-file-input"]'},

                statusOfUpload: {locateStrategy: 'xpath', selector: '(//*[@class="tbl-200-col"])[3]'},
                fileNameUploaded: {locateStrategy: 'xpath', selector: '(//*[@class="tbl-200-col"])[1]'},

                titleUploadBWFilePopUp: {selector: '#ui-dialog-title-5'},
                dropdownSelectYear: {selector: '#bw-monthly-report-year'},
                dropdownSelectMonth: {selector: '#bw-monthly-report-month'},
                dropdownOption2015SelectYear: {locateStrategy: 'xpath', selector: '(//option[@value="2015"])[3]'},
                dropdownOptionJanSelectMonth: {locateStrategy: 'xpath', selector: '(//option[contains(text(),"Jan")])[2]'},
                buttonRadioNoUploadBWFilePopUp: {selector: '#BW_Correction_No'},
                buttonRadioYesUploadBWFilePopUp: {selector: '#BW_Correction_Yes'},
                buttonOKUploadBWFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[3]'},
                buttonCancelUploadBWFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[5]'},

                buttonMonthlyReport: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Create Monthly Report")]'},
                buttonAnnualReport: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Create Annual Report")]'},

                titleMonthlyReportPopUp: {selector: '#ui-dialog-title-2'},
                dropdownMonthlyReportSelectIRSYear: {selector: '#irs-monthly-report-year'},
                dropdownMonthlyReportOption2016: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option)[2]'},
                dropdownMonthlyReportSelectYear: {selector: '#monthly-report-year'},
                dropdownMonthlyReportOption2015: {locateStrategy: 'xpath', selector: '(//select[@id="monthly-report-year"]/..//option)[2]'},
                dropdownMonthlyReportSelectMonth: {selector: '#monthly-report-month'},
                dropdownMonthlyReportOptionJan: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Jan")]'},
                buttonOKMonthlyReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[1]'},
                buttonCancelMonthlyReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[1]'},
                messageSuccess: {locateStrategy: 'xpath', selector: '(//span[contains(text(),"You have successfully created")])[1]'},
                messageSuccessMonthlyReport: {locateStrategy: 'xpath', selector: '//span[contains(text(),"You have successfully created new monthly reports")]'},
                //messageSuccessAnnualReport: {locateStrategy: 'xpath', selector: '//span[contains(text(),"")]'},

                titleAnnualReportPopUp: {selector: '#ui-dialog-title-3'},
                dropdownAnnualReportSelectIRSYear: {selector: '#irs-annual-report-year'},
                dropdownAnnualReportOption2016: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option)[2]'},
                buttonOKAnnualReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[2]'},
                buttonCancelAnnualReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[2]'},

                reportReconciliation: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_Reconciliation_Monthly_JAN_2015")])[1]'},
                reportONG: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_ONG_Monthly_JAN_2015")])[1]'},
                reportNHR: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_NHR_Monthly_JAN_2015")])[1]'},
                reportAnnual: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"ANNUAL ESR REPORT_SLSDM_2016")])[1]'},
                feedbackFileMonthly: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_FF_ME")])[1]'},
                feedbackFileAnnual: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_FF_ME_ANNUAL")])[1]'},

                linkCreateFile: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Create File")]'},
                titlePopupCreateFile: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Create Feedback File")]'},
                dropdownCreateFeedbackFileType: {selector: '#file-type'},
                dropdownCreateFeedbackFileOptionMonthly: {locateStrategy: 'xpath', selector: '//select[@id="file-type"]/..//option[contains(text(),"Monthly")]'},
                dropdownCreateFeedbackFileOptionAnnual: {locateStrategy: 'xpath', selector: '//select[@id="file-type"]/..//option[contains(text(),"Annual")]'},
                inputDateCreateFeedbackFile: {locateStrategy: 'xpath', selector: '//input[@id="js-create-feeback-file-datepicker"]'},
                buttonOKCreateFeedbackFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[4]'},
                buttonCancelCreateFeedbackFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[7]'},

                tabAnnualFiles: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Annual Files")]'},

                datePicker: {locateStrategy: 'xpath', selector: '(//span[@class="ui-datepicker-month"])[1]/..'},
                datePickerMonth: {locateStrategy: 'xpath', selector: '(//span[@class="ui-datepicker-month"])[1]'},
                datePickerYear: {locateStrategy: 'xpath', selector: '(//span[@class="ui-datepicker-year"])[1]'},
                datePickerSelect31: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"31")])[1]'},
                datePickerSelect01: {locateStrategy: 'xpath', selector: '(//a[@class="ui-state-default"])[1]'},
                buttonPreviousDatePicker: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Prev")]'},

            }
        },

    }
};



