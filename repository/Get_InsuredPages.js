/**
 * Created by subhajit-chakraborty on 7/6/2017.
 */

module.exports = {
    sections: {
        LandingPage: {
            selector: 'body',
            elements: {
                //QAF
                GetStartedButton:  {locateStrategy: 'xpath',selector: "//a[contains(text(),'GET STARTED')]"},
                TollFreeNumberText: {locateStrategy: 'xpath',selector: "//a[@id='ClickTrackIVRNumberNull']/../a[@id='ClickTrackNumberNotNull']"},
                HomeLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Home')]"},
                ContactUsLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Contact Us')]"},
                HelpfulLinks: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Helpful Links')]"},
                HRARetireeAccount: {locateStrategy: 'xpath',selector: "//a[contains(text(),'HRA Retiree Account')]"},
                SignUpLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Sign Up')]"},
                LogInLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Log In')]"},

                MedicaIDLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Medicaid')]"},
                HIMLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Health Insurance Marketplace')]"},
                SocialSecurityLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Social Security')]"},
                FAQsLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'FAQs')]"},

                GetStartedLoginGatedPortal: {locateStrategy: 'xpath',selector: "//a[contains(.,'GET STARTED')]"},
                FirstnameTextbox: {selector: "#firstname"},
                LastnameTextbox: {selector: "#lastname"},
                SSNTextbox: {selector: "#ssn"},
                LoginContinueButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},

                StartShoppingLoginUngatedPortal: {locateStrategy: 'xpath',selector: "//a[contains(.,'Shopping')]/span[contains(text(),'Start')]"},
                ShoppingHeaderUnGatedPortal: {locateStrategy: 'xpath',selector: "//h1[contains(text(),' shop for Health Insurance')]"},

                //Footer
                FooterParaLink1: {selector: "p[footer-disclaimer-links='0']"},
                FooterParaLink2: {selector: "p[footer-disclaimer-links='1']"},

                PrivacyPolicyLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Privacy Policy')]"},
                TermsAndConditionLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Terms & Conditions')]"},



            }
        },

        ContactUsPage: {
            selector: 'body',
            elements: {
                //QAF
                ContactUsMarketplaceHeader:  {locateStrategy: 'xpath',selector: "//h1/strong[contains(text(),'Contact Mercer Marketplace')]"},
            }
        },

        MedicalIDPage: {
            selector: 'body',
            elements: {
                //QAF
                MedicalIDImg:  {selector: "a[title='Medicaid.gov Home'] img"},
            }
        },

        HIMPage: {
            selector: 'body',
            elements: {
                //QAF
                HealthInsuranceHeader:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Need health insurance?')]"},
            }
        },

        SocialSecurityPage: {
            selector: 'body',
            elements: {
                //QAF
                SocialSecurityHeader:  {locateStrategy: 'xpath',selector: "///header[@id='banner']//a[contains(text(),'Social Security')]"},
            }
        },

        FAQPage: {
            selector: 'body',
            elements: {
                //QAF
                FAQHeader:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),'How can we help you?')]"},
            }
        },

        ShopForHealthInsurancePage: {
            selector: 'body',
            elements: {
                //QAF
                HealthInsHeader:  {locateStrategy: 'xpath',selector: "//h1[contains(text(),' shop for Health Insurance')]"},
                LostCoverageLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Lost coverage')]"},
                SeeYourOptionsLink: {locateStrategy: 'xpath',selector: "//a[@id='eligibilityNext'][contains(text(),'See Your Options')]"},
                CompareYourOptionTitle: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'Compare your Options')]"},
                ShopWithEmpContButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'SHOP WITH EMPLOYER CONTRIBUTION')]"},
                ShopWithTaxCreditsButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Shop with tax credits')]"},
                HomeButton: {locateStrategy: 'xpath',selector: "(//a[contains(text(),'Home')])[1]"},
                ZipcodeTextbox: {selector: "#zipcode"},
                MonthTextbox: {locateStrategy: 'xpath',selector: "(//input[@type='tel'])[1]"},
                DaysTextbox: {locateStrategy: 'xpath',selector: "(//input[@type='tel'])[2]"},
                YearTextbox: {locateStrategy: 'xpath',selector: "(//input[@type='tel'])[3]"},

                SeePlansLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'See plans')]"},
                CheckForTaxCreditsButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Check for tax credits')]"},
                HouseholdIncomeTextbox: {selector: "#income"},
                CheckForTaxCreditsButtonNonGated: {locateStrategy: 'xpath',selector: "//a[contains(.,'for tax credits')]"},
                //ShopWithTaxCreditsButton: {selector: "//a[contains(text(),'Shop with tax credits')]"},

                HealthProductLink: {locateStrategy: 'xpath',selector : "//li[@id='isAffiliateHealthEnabled']/a"},
                MedicareProductLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateMedicareEnabled']/a"},
                DentalProductLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateDENTALEnabled']/a"},
                VisionProductLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateVISIONEnabled']/a"},
                OtherInsuranceLink: {locateStrategy: 'xpath',selector: "//li[@id='isAffiliateAMEEnabled']/a"},

















            }
        },
    }
};
