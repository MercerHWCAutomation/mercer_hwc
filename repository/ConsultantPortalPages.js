/**
 * Created by subhajit-chakraborty on 4/25/2017.
 */

module.exports = {
    sections: {
        LoginPageBP: {
            selector: 'body',
            elements: {
                //QAF
                inputUsernameBP: {selector: '#Username'},
                inputPasswordBP: {selector: '#Password'},
                ContinueButtonLoginBP: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
            }
        },

        DashboardPageBP: {
            selector: 'body',
            elements: {
                //QAF
                DashboardTitleBP: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'My Dashboard')]"},
                ResourcesTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Resources')]"},
                EducationTab: {selector: "li[ng-class=\"{active: menuItem == 'education'}\"]"},
                ActiveClientsLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Active Clients')][@class='ng-binding']"},
                PendingClientStatusText: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Pending Client Status')]"},
                ActiveClientStatusText: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Summary of current active clients')]"},
                DashboardTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Dashboard')]"},
                PendingClientLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Pending Clients')][@class='ng-binding']"},
                ViewActiveClientsDetails: {selector: "a[ui-sref='activeClients']"},
                ViewPendingClientsDetails: {selector: "a[ui-sref='pendingClients']"},
                FirstActiveClientDetails: {selector: "#DataTables_Table_0 tbody tr:nth-child(1) td a"},
                FirstReadyClientDetails: {selector: "#DataTables_Table_0 tbody tr:nth-child(1) td a"},
                AccessHRPotalTab: {selector: 'ul.side-actions li:nth-child(2) div i'},
                DashboardLink: {selector: "a[href='#/dashboard']"},
                ReadyClientsButton: {selector: "a[href='#/pendingClients/4']"},
                DocumentLibraryQuicklink: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..// a[contains(text(),'Document Library')]"},
                EducationQuicklink: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..// a[contains(text(),'Education')]"},
                AddANewClientTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Add a New Client')]"},
                QuicklinksHeader: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]"},

                DashboardLinkTab: {selector: "a[href='#/dashboard']"},
                ClientsLinkTab: {locateStrategy: 'xpath',selector: "//a[@href='#/dashboard']/../..//a[contains(text(),'Clients')]"},


            }
        },
        HrPortalProxyPage: {
            selector: 'body',
            elements: {
                //QAF
                EndImpersonationButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'End Proxy Session')]"},
            }
        },
        DocLibraryPage: {
            selector: 'body',
            elements: {
                //QAF
                DocLibTitle: {locateStrategy: 'xpath',selector: "//h1[contains(text(),' Document Library')]"},
                SolutionSupportTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Solution Support')]"},
                SalesTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Sales')]"},
                MarketingTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'RFP / Marketing')]"},
                ClientSetupTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Client Set-Up')]"},
                OngoingSupportTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Ongoing Support')]"},

                SalesTabHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Sales')]"},
                MarketingTabHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'RFP / Marketing')]"},
                ClientTabHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Client Set-Up')]"},
                OngoingSupportHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Ongoing Support')]"},
                SolutionSupportHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Solution Support')]"},
                FirstDocLink: {selector: "div[ng-repeat='document in page.documents']:nth-child(1) div a[target='_blank']"},
                PDFTab: {selector: "#plugin"},
                AddToZip: {locateStrategy: 'xpath',selector: "//div[@ng-repeat='document in page.documents'][1] //label[contains(text(),'Add to Zip')]"},
                AddedToZip: {locateStrategy: 'xpath',selector: "//div[@ng-repeat='document in page.documents'][1] //label[contains(text(),'Added')]"},
                DownloadSelectedTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Download Selected')]"},
                ListViewTab: {selector: "dl.switcher dd:nth-child(2) a"},
                TileViewTab: {selector: "dl.switcher dd:nth-child(1) a"},
                AddedToZipCheckedListView: {selector: "table.smb-table.smb-table-strip tr.smb-doc-list.smb-doc-list.ng-scope.smb-doc-list-active:nth-child(1)"},
                DescendingSearchLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Descending')]"},
                AscendingSearchLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Ascending')]"},


            }
        },

        AddClientPage: {
            selector: 'body',
            elements: {
                //QAF
                StartNowButton: {locateStrategy: 'xpath',selector: "//h1/..//button[contains(text(),'Start Now')]"},
                ClientNameTextbox: {selector: "input[name='clientName']"},
                ClientIndustryDropdown: {selector: "select[name='clientIndustry']"},
                AgricultureForestryOption: {selector: "select[name='clientIndustry'] option[value='number:2']"},
                EmployerEntityDropdown: {selector: "select[name='employerEntity']"},
                CorporationOption: {selector: "select[name='employerEntity'] option[value='number:1']"},

                AddressLine1USClientHeadquarters: {selector: "input[name='street']"},
                CityUSClientHeadquarters: {selector: "input[name='city']"},
                ZipUSClientHeadquarters: {selector: "input[name='zip']"},
                StateUSClientHeadquarterDropdown: {selector: "select[name='state']"},
                StateDropdownOption1: {selector: "select[name='state'] option[label='Arizona']"},

                ContactNameHR: {selector: "input[name='contactName']"},
                EmailHR: {selector: "input[name='email']"},
                ContactHR: {selector: "input[name='phone']"},

                ContinueButton1ClientProfile: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},

                EnrollmentStartDate: {selector: "input[name='openEnrollmentBeginDate']"},
                PlanYearEffectiveDate: {selector: "input[name='planBeginEffectiveDate']"},
                FederalTaxID: {selector: "input[name='federalTaxId']"},
                ClientOneCode: {selector: "input[name='clientOneCode']"},
                LengthOpenEnrollmentDropdown: {selector: "select[name='openEnrollmentLengthChoice']"},
                OneWeekOptionOEDropdown: {selector: "select[name='openEnrollmentLengthChoice'] option[label='One Week']"},
                SaveAndContinueButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Save & Continue')]"},

                //Plan decision Page Link
                PlanDecisionTitle: {locateStrategy: 'xpath',selector: "//h1[@class='ng-binding'][contains(text(),'Plan Decisions')]"},

                //Benefits Page
                BenefitPageTitle: {selector: "h1.header-lead.ng-binding.ng-scope"},
                GetStartedMedical: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Medical')]/../..//div[4]/a"},
                MedicalCarrierDropdown: {selector: "select[name='carrierId']"},
                ToggleArrow: {selector: "span.icon-toggle-arrow"},
                AetnaDropdownOption: {selector: "select[name='carrierId'] option[label='Aetna']"},
                MedicalPlanOfferedOption1: {selector: "label[for='PPOL003E01']"},
                MedicalPlanOfferedOption2: {selector: "label[for='PPOH003E01']"},

                SupplementalHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Supplemental')]"},
                SupplementalCarrierDropdown: {selector: "select[name='carrierId']"},
                AlfacDropdownOption1: {selector: "select[name='carrierId'] option[label='Aflac']"},
                AccidentInsuranceTitle: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Accident Insurance')]"},
                CriticalIllnessInsuranceTitle: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Critical Illness Insurance')]"},
                CriticalIllnessOfferedOption1: {selector: "label[for='plan0']"},
                CriticalIllnessOfferedOption2: {selector: "label[for='plan1']"},
                HospitalIndemnityInsuranceTitle: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Hospital Indemnity')]"},

                DentalHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Dental')]"},
                DentalCarrierDropdown: {selector: "select[name='carrierId']"},
                AetnaDentalOptionCarrier: {selector: "select[name='carrierId'] option[label='Aetna']"},
                DentalPlanOfferedOption1: {selector: "label[for='DENL003E01']"},
                DentalPlanOfferedOption2: {selector: "label[for='DENH003E01']"},

                VisionHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Vision')]"},
                VisionCarrierDropdown: {selector: "select[name='carrierId']"},
                AetnaOptionCarrier: {selector: "select[name='carrierId'] option[label='Aetna']"},

                LifeHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Life')]"},
                LifeCarrierDropdown: {selector: "select[name='carrierId']"},
                AIGOptionCarrierLife: {selector: "select[name='carrierId'] option[label='MetLife']"},
                BasicLIHeader: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Basic Employee Life Insurance')]"},
                BasicLIOption1: {selector: "label[for='checkbox10']"},
                VoluntaryLIHeader: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Voluntary Life Insurance')]"},
                VoluntaryLIOption1: {selector: "label[for='checkbox10']"},
                SpouseLIHeader: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Spouse Life Insurance')]"},
                SpouseLIOption1: {selector: "label[for='checkbox10']"},
                SpouseLIOption2: {selector: "label[for='checkbox11']"},
                ChildLIHeader: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Child Life Insurance')]"},
                VoluntaryADDHeader: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Voluntary AD&D')]"},
                VoluntaryADDOption1: {selector: "label[for='checkbox10']"},

                DisabilityHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Disability')]"},
                DisabilityOption1: {selector: "label[for='checkbox10']"},

                ProtectionHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Protection')]"},
                IdentityTheftHeader: {locateStrategy: 'xpath',selector: "//strong[contains(text(),'Identity Theft')]"},

                //Contribution Page:
                ContributionHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Contributions')]"},
                ContributionTypeDropdown: {selector: "select[ng-model='contribution.type']"},
                Option1ContributionType: {selector: "option[label='Defined Benefit']"},

                //Rates Page
                RatesHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Rates')]"},
                QuestionareHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Questionnaire')]"},
                WorksheetHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Worksheet')]"},
                ContinueToOnscreenEntry: {locateStrategy: 'xpath',selector: "//a[contains(.,'Continue to Onscreen Entry')]"},
                EnterAnnualRateHeader: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'entering annual rates')]"},
                UploadWorksheetLink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Upload Worksheet')]"},
                ChooseFileWorksheet: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Choose file')]"},
                UploadRatesWorksheet: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Upload Rates')]"},
                RatesSuccessMsg: {locateStrategy: 'xpath',selector: "//p[contains(text(),'Success! Your rates have been updated.')]"},
                InternalServerError: {locateStrategy: 'xpath',selector: "//small[contains(text(),'Internal server error.')]"},
                ContinueSuccessMsgButton: {locateStrategy: 'xpath',selector: "//p[contains(text(),'Success! Your rates have been updated.')]/../../div[2]/button"},

                //newly added
                SaveChangesButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Save Changes')]"},
                SupplementalTab: {locateStrategy: 'xpath',selector: "//h5[contains(text(),'Supplemental')]/.."},
                AccidentInsuranceTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Accident Insurance')]/../../dd[@heading='Accident Insurance']"},
                CriticalIllnessInsuranceTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Critical Illness Insurance')]/../../dd[@heading='Critical Illness Insurance']"},
                LowTab: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Low')]"},
                HospitalIndemnityTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Hospital Indemnity')]/../../dd[@heading='Hospital Indemnity']"},
                DentalTab: {locateStrategy: 'xpath',selector: "//h5[contains(text(),'Dental')]/.."},
                StandardPlanHeader: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Aetna Standard Plan')]"},
                VisionTab: {locateStrategy: 'xpath',selector: "//h5[contains(text(),'Vision')]/.."},
                VisionPlanHeader: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Aetna Vision Plan')]"},
                LifeTab: {locateStrategy: 'xpath',selector: "//h5[contains(text(),'Life')]/.."},
                BasicEmpLIHeader: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Basic Employee Life Insurance')]/../../dd[@heading='Basic Employee Life Insurance']"},
                VoluntaryLITab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Voluntary Life Insurance')]/../../dd[@heading='Voluntary Life Insurance']"},
                SpouseLITab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Spouse Life Insurance')]/../../dd[@heading='Spouse Life Insurance']"},
                ChildLITab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Child Life Insurance')]/../../dd[@heading='Child Life Insurance']"},
                VoluntaryAddTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Voluntary AD&D')]/../../dd[@heading='Voluntary AD&D']"},
                DisablityTab: {locateStrategy: 'xpath',selector: "//h5[contains(text(),'Disability')]/.."},
                DisabilityHeaderTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Long Term Disability')]"},
                ProtectionTab: {locateStrategy: 'xpath',selector: "//h5[contains(text(),'Protection')]/.."},
                IdentityTheftTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Identity Theft')]/../../dd[@heading='Identity Theft']"},
                SaveAndContAdminstrationPage: {selector: "//a[contains(text(),'Save & Continue')]"},






















                //Adminstration page
                EmployeeEventsHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Employee Events')]"},
                NewHireHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'NEW HIRES')]"},
                RehireHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'REHIRES')]"},
                TerminationHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'TERMINATION')]"},
                OtherEmpEventsHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Other Employer Events')]"},
                PayrollHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Payroll')]"},
                PayrollTemplateoption: {selector: "select[name='templateOption']"},
                PayrollTempOption1: {selector: "select[name='templateOption'] option:nth-child(2)"},
                PayrollFreqHeader: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'payroll frequencies')]"},
                WeeklyOption: {selector: "label[for='weekly']"},
                WeeklyPayrolldate: {selector: "input[name='weeklyDate']"},
                ContinueButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
                ClientLogoHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Client Logo')]"},
                MessagesHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Messages')]"},
                DocumentsHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Documents')]"},
                ContinueTabDocuments: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Continue')]"},
                COBRAHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'COBRA')]"},
                EnrolledCOBRAMemtextbox: {selector: "input[ng-model='model.cobraEnrolledCount']"},
                PendingCOBRAMemtextbox: {selector: "input[ng-model='model.cobraPendingCount']"},
                CallCenterHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Call Center')]"},
                CallCenterDropdown: {selector: "select[ng-model='model.callCenterFlag']"},
                CallCenterOption1: {selector: "select[ng-model='model.callCenterFlag'] option:nth-child(2)"},

                EmployeeDataHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'EMPLOYEE DATA')]"},
                NextButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Next')]"},
                ChooseFileEmpData: {selector: "input[type='file']"},
                UploadFileButton: {selector: "//span[contains(text(),' Upload File ')]/../div"},














               // D1Plan1EmpOnlyAEA: {selector: "//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee Only')]/../td[2]//input"},
               // D1Plan2EmpOnlyAEA: {selector: "//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee Only')]/../td[3]//input"},
               // D1Plan1EmpSpouseAEA: {selector: "//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee + Spouse')]/../td[2]//input"},
               // D1Plan2EmpSpouseAEA: {selector: "//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee + Spouse')]/../td[3]//input"},
               // D1Plan1EmpChildAEA: {selector: "//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee + Child')]/../td[2]//input"},
               // D1Plan2EmpChildAEA: {selector: "//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee + Child')]/../td[3]//input"},
               // D1Plan1EmpFamilyAEA: {selector:"//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee + Family')]/../td[2]//input"},
               // D1Plan2EmpFamilyAEA: {selector:"//h6[contains(text(),'$400 Deductible ')]/..//td[contains(text(),'Employee + Family')]/../td[3]//input"},
//
               // D2Plan1EmpOnlyAEA: {selector: "//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee Only')]/../td[2]//input"},
               // D2Plan2EmpOnlyAEA: {selector: "//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee Only')]/../td[3]//input"},
               // D2Plan1EmpSpouseAEA: {selector: "//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee + Spouse')]/../td[2]//input"},
               // D2Plan2EmpSpouseAEA: {selector: "//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee + Spouse')]/../td[3]//input"},
               // D2Plan1EmpChildAEA: {selector: "//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee + Child')]/../td[2]//input"},
               // D2Plan2EmpChildAEA: {selector: "//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee + Child')]/../td[3]//input"},
               // D2Plan1EmpFamilyAEA: {selector:"//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee + Family')]/../td[2]//input"},
               // D2Plan2EmpFamilyAEA: {selector:"//h6[contains(text(),'$900 Deductible ')]/..//td[contains(text(),'Employee + Family')]/../td[3]//input"},








            }
        },
    }
};
