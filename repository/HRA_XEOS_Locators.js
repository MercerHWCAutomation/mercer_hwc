﻿module.exports = {
    sections: {

        //HRA XEOS locators
        HRAPage: {
            selector: 'body',
            elements: {
                inputUsername: { selector: '#login_id'},
                inputPassword: { selector: '#password'},
                buttonLogin: {locateStrategy: 'xpath', selector: '//input[@value="Log In"]'},


                linkDashboard: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Dashboard")])[2]'},
                linkClaims: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Claims")])[2]'},
                linkHRAReports: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"HRA Reports")])[2]'},

                imageCreateClaim: {locateStrategy: 'xpath', selector: '//h5[contains(text(),"Create a Claim")]'},
                imagePersonalInfo: {locateStrategy: 'xpath', selector: '//h5[contains(text(),"Personal Information")]'},

                pageTitle: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Welcome")]'},
                titleCreateClaim: {locateStrategy: 'xpath', selector: '//*[contains(text(),"Claim Submission")]'},
                titlePersonalInfo: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Personal Info")]'},
                titleClaims: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Claims")]'},
                titleHRAReports: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"HRA Reports")]'},

                buttonCreateOneTimeClaim: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Create One-Time Claim")]'},
                buttonCreateAutomaticClaim: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Create Automatic Claim")]'},
                buttonSaveChanges: {locateStrategy: 'xpath', selector: '//input[@value="Save Changes"]'},
                buttonGenerateDenialLetters: {locateStrategy: 'xpath', selector: '//input[@value="Generate Denial Letters"]'},
                buttonPrint: { selector: '#print'},
                buttonDownload: { selector: '#download'},

                claimNumber: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"-")])[2]'},

                iconEditInfo :{selector: '#editaddress'},
                inputStreet: {locateStrategy: 'xpath', selector: '//input[@name="address"]'},
                inputCity: {locateStrategy: 'xpath', selector: '//input[@name="city"]'},
                inputZip: {locateStrategy: 'xpath', selector: '//input[@name="zip"]'},
                inputPhone: {locateStrategy: 'xpath', selector: '//input[@name="phone"]'},
                inputCellPhone: {locateStrategy: 'xpath', selector: '//input[@name="cellphone"]'},

                messageSuccess: {locateStrategy: 'xpath', selector: '(//button[@class="close-button"])[1]/..'},
            }
        },

    }
};



