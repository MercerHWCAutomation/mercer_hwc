/**
 * Created by subhajit-chakraborty on 6/9/2017.
 */

module.exports = {
    sections: {
        LoginPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                inputUsernameHRAP: {locateStrategy: 'xpath',selector: "//*[@id=\"Username\"]"},
                inputPasswordHRAP: {selector: '#Password'},
                ContinueButtonLoginHRAP: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
            }
        },

        DashboardPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                MyDashboardHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'MY DASHBOARD')]"},
                DayToDayTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Day to Day')]"},
                OpenEnrollmentTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Open Enrollment')]"},
                EmployeeSnapshotTab: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Employee Snapshot')]"},
                DocumentLibraryQuicklinks:{locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..//a[contains(text(),'Document Library')]"},
                EducationQuicklinks: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..//a[contains(text(),'Education')]"},
                ViewAllEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'View All Employees')]"},
                AddNewEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(.,' Add New Employee')]"},

                //new Added
                EmployeeSnapshotTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Employee Snapshot')]"},
                ActivityStreamTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Activity Stream')]"},
                MessageCenterTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Message Center')]"},
                PayrollTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Payroll')]"},
                ReportingTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Reporting')]"},
                DashboardMenuFlyoverTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Dashboard')]"},
                EmployeesMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(.,'Employees')]"},
                BenefitsMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Benefits')]"},
                ReportingMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Reporting')]"},
                PayrollMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Payroll')]"},
                ResourcesMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(.,'Resources ')]"},
                TestingMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Testing')]"},
            }
        },

        DocumentLibraryPageHRAP: {
            selector: 'body',
            elements: {
                //QAF

                DocumentLibraryTitle: {locateStrategy: 'xpath',selector: "//h1[contains(text(),' Document Library')]"},
                TileViewTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Tile View')]"},
                ListViewTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'List View')]"},
                SolutionOverviewTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Solution Overview')]"},
                DataManagementTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Data Management')]"},
                PayrollAndReportsTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Payroll & Reports')]"},
                SpendingAccountsTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Spending Accounts')]"},
                ProcessSupportTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Process Support')]"},
                COBRATab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'COBRA')]"},
                AscendingTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Ascending (A-Z)')]"},
                DescendingTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Descending (Z-A)')]"},




            }
        },

        EducationPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                CanvasButtonHRAP: {locateStrategy: 'xpath',selector: "//div[@id='6EuBeLDeMNm.6qdK8x09Vmg.6AhjQye8YU9']/canvas"},
                LearnMoreButton: {locateStrategy: 'xpath',selector: "//div[@id='5Xi2nfHUYZy.6Yp7JZBa9LE.6H3DdmjBPZS']/canvas"},
                VideoDashboardTab: {locateStrategy: 'xpath',selector: "//div[@id='5u105TYJXcm.5ix115EISrk.6kvzrg5l5we.6JFGXrba3OK.6YVXWNF7KkG']/canvas"},
            }
        },

        EmployeePageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                EmployeeTitleHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Employees')]"},
                FirstLName: {selector: "table[role='grid'] tbody tr:nth-child(1) td:nth-child(1) a"},
                ProxyAccessLinkFirst: {locateStrategy: 'xpath', selector: '/html/body/div[1]/div/div/div[2]/div/section/div/div[1]/ul[1]/li'},
                SubmitButtonProxyPage: {selector: "input[value='Submit']"},
                EndImpersonationButton: {locateStrategy: 'xpath', selector: "/html/body/div[3]/div[1]/div/span[2]/a"},

            }
        },

        ReportingPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                ReportingTitleHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Reporting')]"},
                SelectReportTypeDropdown: {selector: "select[ng-model='reportOption']"},
                AlllifeEventDropdown: {selector: "select[ng-model='reportOption'] option[label='All Life Events']"},
                CreateReportButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Create')]"},
                IncludeEventDropdown: {selector: "select[ng-model='result.value']"},
                AllEventOptionIEDropdown: {selector: "select[ng-model='result.value'] option[label='All Events']"},
                EffectiveDateStartValue: {selector: "input[ng-model='startdate.value']"},
                EffectiveDateEndValue: {selector: "input[ng-model='enddate.value']"},
                GenerateReportButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Generate Report')]"},
                ReportSuccessMsg: {locateStrategy: 'xpath',selector: "//div[contains(text(),'Your report was successfully created.')]"},
                ReportDownloadButton: {locateStrategy: 'xpath',selector: "//a[contains(.,'Download')]"},
                CloseButtonReportOption: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Close')]"},
                LatestCreatedReportValue: {locateStrategy: 'xpath',selector: "//tbody/tr[1]/td[4]//a[contains(text(),'All Life Events')]"},
                ErrorMsgReportDownload: {locateStrategy: 'xpath',selector: "//pre[contains(text(),'Requested action is forbidden for current user')]"},



            }
        },
    }
};
