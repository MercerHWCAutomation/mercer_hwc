Feature: HR Admin Portal Smoke Test cases

  @Smoke @HRAdminPortalPromotedClientsProxyAccess @1
  Scenario: Checking the Proxy Access in Promoted clients for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open the Employee Page screen
    Then    User able to navigate to proxy access for MBC Portal for Promoted Clients
    And     User able to logout Successfully form HR Admin Portal

  @Smoke @HRAdminPortalLoginFunctionality @2
  Scenario: Checking the Login flow for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    Then    HRAdmin Portal Dashboard should be displayed
    And     User able to logout Successfully form HR Admin Portal

  @Smoke @HRAdminPortalDashboardVerification @3
  Scenario: Checking the Dashboard Page for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    Dashboard Page should be in default view for HRAdmin Portal
    And     User able to logout Successfully form HR Admin Portal

  @Smoke @HRAdminPortalLogoutScenario @4
  Scenario: Checking the Proxy Access in Non-Promoted clients for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    User able to logout Successfully form HR Admin Portal

  @Smoke @HRAdminPortalDocumentLibrary @5
  Scenario: Checking the Document Library Page for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open Document Library Page HRAdmin Portal
    And     User able to view Tile and List view of document library for HRAdmin Portal
    Then    User able to perform download and sorting action on the files for HRAdmin Portal
    And     User able to logout Successfully form HR Admin Portal


  #@Smoke @HRAdminPortalNonPromotedClientsProxyAccess @6
  #Scenario: Checking the Proxy Access in Non-Promoted clients for the HRAdmin Portal
    #Given   User opens the browser and launch the HRAdmin Portal URl
    #When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    #And     HRAdmin Portal Dashboard should be displayed
    #And     User able to open the Employee Page screen
    #Then    User able to navigate to proxy access for MBC Portal for Promoted Clients

  @Smoke @HRAdminPortalEducationFunctionality @6
  Scenario: Checking the Education Page for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open Education Page for HRAdmin Portal
    #Then    Video should work and can move to next Video Page

  @Smoke @HRAdminPortalReportingReportGeneration @7
  Scenario: Checking the Report Generation Page for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User clicks on reporting tab and reporting Page is opened
    Then    User able to create and download link is enabled for the report to download
    And     New report is added to the row of all reports and reports can be downloaded




