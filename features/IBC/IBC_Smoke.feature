@Login
Feature: Scenarios includes Verification of Login, Logout, header and footer links, Proxy functionality and Forgot Password flow functionality.

 # @IBC-Smoke-1
 # Scenario: Navigate to IBC Home Page
 #   Given Once User is logged in using "IBCUSER" to IBC application
 #   Then  Verify Tabs displayed in IBC Home Page
 #   And   Containers displayed in Home Page
 #   And   Verify DC Plan link in Wealth Menu
#
 # @IBC-Smoke-2
 # Scenario: Verify Header and Footer Links in Home Page
 #   When  User Clicks on linkLoginManagement in Home Page
 #   Then  Verify that Login Management account information page loads successfully
 #   And   User should be able to see fields "User Name,Password,Email,SecurityQuestions,Answers,Change your User Name,Change your Password,Change your E-mail Address and Change your Security Questions" fields in Account login information page
 #   When  User Clicks on "Change your Security Questions" in Account Login Information Page
 #   When  Verify that Modify SQA Page loads successfully
 #   When  User Change Answer for "Security Question1" field
 #   And   User Clicks on "Save" button
 #   And   Verify User should be able to update account login information
 #   When  User Clicks on SiteMapLink in Home Page
 #   Then  Verify that "G2SiteMap" text is displayed in the URL
 #   When  User Clicks on ContactUs link in Header
 #   Then  Verify that Contact Us Page loads successfully
 #   When  User Clicks on Help Link in Header
 #   Then  Verify that Help Page loads successfully
 #   When  User Clicks on PrivacyPolicy Link from footer
 #   Then  Verify that Privacy Policy Page Opened in different window
 #   When  User Clicks on Terms of Use link from footer
 #   Then  Verify that Terms of Use Page Opened in different Window
#
 #  @IBC-Smoke-3
 #   Scenario: Proxy functionality
 #   When User Clicks on Administration link
 #   Then Verify that Admin Landing Page loads successfully
 #   When User Clicks on Employee Search link
 #   Then Verify that Employee Search Page loads successfully
 #   When User enter SSN and click on search button
 #   Then Verify that user should be able to see results
 #   When User clicks on Select link
 #   Then Verify User should be able to see heading You are currently impersonating xxxx in the header
 #   When User Clicks on Logout link in header
#
 # @IBC-Smoke-4
 # Scenario: Verify Logout functionality
 #   When  User Clicks on Logout link in header
 #   Then  Verify User should be able to log out successfully
 #   Given Once User is logged in using "IBCUSER" to IBC application
 #   And   Verify DB Plan link in Wealth Menu
 #   When  User Clicks on Logout link in header
 #   Then  Verify User should be able to log out successfully
#
 # @IBC-Smoke-5
 # Scenario: Forgot Password Flow
 #   Given User Navigate to IBC Login Page using "IBCUSER"
 #   When  User Enter Security Answer1
 #   And   User Enter Security Answer2
 #   And   User Enter New Password
 #   Then  Verify Confirmation Message after saving information
 #   When  Clicks on Continue button
 #   Then  Verify that Home Page loads successfully
 #   When  User Clicks on LoginManagementlink in Home Page
 #   Then  Verify that Login Management account information page loads successfully
 #   When  User Clicks on linkChangePassword
 #   Then  Verify that Change Your Password page loads successfully
 #   When  Change the Password
 #   And   Verify User should be able to update login information