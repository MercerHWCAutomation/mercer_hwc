Feature: Smoke Scenarios for Participant

 # @Afweb  @Participants
 # Scenario: Clients Search Page Validation
 #   When User Clicks on Participants Tab
 #   Then Verify that Participant Search Page loads successfully
 #   Then Verify Participant Search Page title
#
 # @Afweb @ParticipantSearchBySocialId
 # Scenario: Participant Search By SocialId
 #   When User Enters SSN in Social ID field and clicks on search button
 #   And  Verify Search Criteria Title and SSN Value in Participant Results Page
 #   When User Clicks on Participant Code
 #   Then Verify that Participant Summary Page loads successfully
#
###  @Afweb @ParticipantSearchByAlternateUsername
###  Scenario: Participant Search By Alternate Username
###    When User Enters Alternate Username and clicks on search button
###    And  Verify Search Criteria Title and Alternate Username Value in Participant Results Page
###    When User Clicks on first Participant Code
###    Then Verify that Participant Summary Page loads successfully
#
 # @Afweb  @ParticipantSummary @ParticipantProfile
 # Scenario: Participant Profile Section
 #   Then Verify Participant Profile Section Name in ParticipantSummary Page
#
 # @Afweb  @ParticipantSummary @ParticipantWebAccess
 # Scenario: Participant Web Access Section
 #   Then Verify Participant Web Access Section Name in ParticipantSummary Page
#
 # @Afweb  @ParticipantSummary @AdministrationHistory
 # Scenario: Administration History Section
 #   Then Verify Administration Section Name in ParticipantSummary Page
#
 # @Afweb  @ParticipantSummary @WebLoginManagement
 # Scenario: Web Login Management Section
 #   Then Verify Web Login Management Section Name in ParticipantSummary Page
 #   When User Clicks on Web Login Management link
 #   Then Verify WebLoginManagement Page loads successfully
#
 #  @Afweb @ParticipantWebLoginManagementLanding
 #   Scenario: Web Login Management Landing Page
 #   Then Verify Authentication Credentials Header in WebLoginManagementLanding Page
 #   And Verify Administrative Actions Header in WebLoginManagementLanding Page
#
#