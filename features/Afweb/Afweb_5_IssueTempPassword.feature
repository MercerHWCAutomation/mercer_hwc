#Feature: Smoke Scenarios for Temp Password
#
#  @Afweb  @Participants
#  Scenario: Clients Search Page Validation
#    Given User Navigate to AfWeb Home Page
#    Then Verify that Home Page loads successfully
#    When User Clicks on Participants Tab
#    Then Verify that Participant Search Page loads successfully
#    Then Verify Participant Search Page title
#
#  @Afweb @ParticipantSearchBySocialId
#  Scenario: Participant Search By SocialId
#    When User Enters SSN in Social ID field and clicks on search button
#    And  Verify Search Criteria Title and SSN Value in Participant Results Page
#    When User Clicks on Participant Code
#    Then Verify that Participant Summary Page loads successfully
#
#   @Afweb  @ParticipantSummary @WebLoginManagement
#  Scenario: Web Login Management Section
#    Then Verify Web Login Management Section Name in ParticipantSummary Page
#    When User Clicks on Web Login Management link
#    Then Verify WebLoginManagement Page loads successfully
#
##  @Afweb @Participant @IssueTempPassword
##  Scenario: Issue Temp Password
##    Then Verify Authentication Credentials Header in WebLoginManagementLanding Page
##    And Verify Administrative Actions Header in WebLoginManagementLanding Page
#
