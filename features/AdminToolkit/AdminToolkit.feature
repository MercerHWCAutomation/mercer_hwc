Feature: Admin Toolkit Smoke Test cases

  #@Smoke @AdminToolkitContentEPAddRule @1
  #Scenario: Accessing the Admin Toolkit Content Ep Add Rule
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User enter Content EP Values and Corresponding EP details should be displayed
  #  And     User clicks on EP description link and should navigate to Ep Rules page
  #  And     User Adds a new Rule for the Ep and Congratulation Msg Appears
  #  Then    User able to delete the Created Rule
#
  #@Smoke @AdminToolkitHybridEPEditRule @2
  #Scenario: Accessing the Admin Toolkit Edit Rule in Hybrid EP
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User enter Content Hybrid EP Values and Corresponding EP details should be displayed
  #  And     User clicks on EP description link and should navigate to Ep Rules page
  #  Then    User edit the corresponding EP and clicks on Save button for Confirmation Msg
#
  #@Smoke @AdminToolkitJavascriptAddRule @3
  #Scenario: Accessing the Admin Toolkit Javascript Ep Add Rule
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User enter Javascript EP Values and Corresponding EP details should be displayed
  #  And     User clicks on EP description link and should navigate to Ep Rules page
  #  And     User Adds a new Rule for Javascript Ep and Congratulation Msg Appears
  #  Then    User able to delete the Javascript Created Rule
#
  #@Smoke @AdminToolkitDeleteEPRule @4
  #Scenario: Accessing the Admin Toolkit Content Ep Add Rule
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User enter Content EP Values and Corresponding EP details should be displayed
  #  And     User clicks on EP description link and should navigate to Ep Rules page
  #  And     User Adds a new Rule for the Ep and Congratulation Msg Appears
  #  Then    User able to delete the Created Rule
#
  #@Smoke @AdminToolkitClientSettingHBDB @5
  #Scenario: Accessing the Admin Toolkit Client Setting for HB and DB
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User able to navigate to HB Client Page
  #  And     User should able to save changes in HB client setting tab
  #  And     User able to navigate to DB Client Page
  #  Then    User should able to save changes in DB client setting tab
#
  #@Smoke @AdminToolkitVisibilityConfigPortalView @6
  #Scenario: Accessing the Admin Toolkit Visibility Configuration and portal view
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User navigates to Visibility Configuration page and make changes
  #  And     User saves the Visibility configuration msg and Confirmation dialog appears
  #  And     User navigates to the Portal view tab and make changes
  #  Then    User saves the Portal view tab and Confirmation msg is displayed
#
  #@Smoke @AdminToolkitBPOCarrierDescription @7
  #Scenario: Accessing the Admin Toolkit for BPO and Carrier Description tab
  #  Given   User opens the browser and launch the AdminTool kit URl
  #  When    User clicks on Client Link and Web configuration Page is displayed
  #  And     User clicks on BPO_Carrier Description tab and page is loaded
  #  And     User clicks on Carrier Description link and edit Details on an EP
  #  Then    User saves the editing for BPO_CarrierDesc and Confirmation Msg is displayed
#
  ##@Smoke @AdminToolkitPromoteBestMatch @8
  ##Scenario: Accessing the Admin Toolkit for Promote and Best match tab
  ##  Given   User opens the browser and launch the AdminTool kit URl
  ##  When    User clicks on Client Link and Web configuration Page is displayed
  ##  And     User clicks the best match link and edit actuarial value
  ##  And     User clicks on save and confirmation for Best match is displayed
  ##  And     User opens the promote tab and select one and promote the EP
  ##  #Then    Confirmation msg is displayed for successful promotion
#
  ##test
 ## @Smoke @DemoTestRule
 ### Scenario: Accessing the Admin Toolkit Content Ep Add Rule
 ###   Given   User opens the browser and launch the AdminTool kit URl GG
  ##  When    User clicks on Client Link and Web configuration Page is displayed GG
#