var action = require('./Keywords.js');
var Objects = require(__dirname + '/../../repository/ESR_Locators.js');
var data = require('../../TestResources/ESR_TestData.js');

module.exports = function () {

    this.Given(/^User uploads the inbound file$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if(execEnv.toUpperCase() == "QA") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi",function (result) {
                console.log("File Uploaded"+result);
                action.performClick('ESRHomePage|buttonFileUpload',function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }
        else if(execEnv.toUpperCase() == "SANDBOX") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox",function (result) {
                console.log("File Uploaded"+result);
                action.performClick('ESRHomePage|buttonFileUpload',function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if(execEnv.toUpperCase() == "STRESS") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox",function (result) {
                console.log("File Uploaded"+result);
                action.performClick('ESRHomePage|buttonFileUpload',function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if(execEnv.toUpperCase() == "CITEST") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox",function (result) {
                console.log("File Uploaded"+result);
                action.performClick('ESRHomePage|buttonFileUpload',function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if(execEnv.toUpperCase() == "PROD") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox",function (result) {
                console.log("File Uploaded"+result);
                action.performClick('ESRHomePage|buttonFileUpload',function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        /*    action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi",function (result) {
         console.log("File Uploaded"+result);
         action.performClick('ESRHomePage|buttonFileUpload',function () {
         //browser.pause(data.miniWait);
         browser.timeoutsImplicitWait(data.shortWait);
         });
         });       */

        browser.timeoutsImplicitWait(data.shortWait);

    });

    this.Given(/^User uploads the BW file$/, function (browser) {

        action.performClick('ESRHomePage|dropdownSelectYear',function () {
            action.performClick('ESRHomePage|dropdownOption2015SelectYear',function () {
                action.performClick('ESRHomePage|dropdownSelectMonth',function () {
                    action.performClick('ESRHomePage|dropdownOptionJanSelectMonth',function () {
                        action.performClick('ESRHomePage|buttonRadioYesUploadBWFilePopUp',function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.performClick('ESRHomePage|buttonOKUploadBWFilePopUp', function () {
                                /*action.waitTillElementNotDisplayed('ESRHomePage|buttonCancelUploadBWFilePopUp',function () {
                                    browser.timeoutsImplicitWait(data.shortWait);

                                });  */
                                return true;
                            });
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User generates the monthly report$/, function (browser) {
        action.performClick('ESRHomePage|dropdownMonthlyReportSelectIRSYear', function () {
            browser.timeoutsImplicitWait(data.shortWait);
            action.performClick('ESRHomePage|dropdownMonthlyReportOption2016', function () {
                browser.timeoutsImplicitWait(data.shortWait);
                action.performClick('ESRHomePage|dropdownMonthlyReportSelectYear', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.performClick('ESRHomePage|dropdownMonthlyReportOption2015', function () {
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.performClick('ESRHomePage|dropdownMonthlyReportSelectMonth', function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.performClick('ESRHomePage|dropdownMonthlyReportOptionJan', function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                action.performClick('ESRHomePage|buttonOKMonthlyReportPopUp', function () {
                                    browser.timeoutsImplicitWait(data.shortWait);
                                    action.waitTillElementLoads('ESRHomePage|messageSuccessMonthlyReport', function () {
                                        browser.timeoutsImplicitWait(data.shortWait);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User generates the annual report$/, function (browser) {

        action.performClick('ESRHomePage|dropdownAnnualReportSelectIRSYear', function () {
            browser.timeoutsImplicitWait(data.shortWait);
            action.performClick('ESRHomePage|dropdownAnnualReportOption2016', function () {
                browser.timeoutsImplicitWait(data.shortWait);
                action.performClick('ESRHomePage|buttonOKAnnualReportPopUp', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.waitTillElementLoads('ESRHomePage|messageSuccess', function () {
                        browser.timeoutsImplicitWait(data.shortWait);
                    });
                });
            });
        });
    });

    this.Given(/^User generates the monthly feedback file$/, function (browser) {

        action.performClick('ESRHomePage|dropdownCreateFeedbackFileType', function () {
            browser.timeoutsImplicitWait(data.shortWait);
            action.performClick('ESRHomePage|dropdownCreateFeedbackFileOptionMonthly', function () {
                browser.timeoutsImplicitWait(data.shortWait);
                // --------- Insert a function to navigate to 31/10/2015
                action.performClick('ESRHomePage|inputDateCreateFeedbackFile', function () {
                    // action.navigateTillDisplayed('ESRHomePage|datePicker','ESRHomePage|buttonPreviousDatePicker',function () {
//                        browser.pause(3000);
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.performClick('ESRHomePage|datePickerSelect01', function () {
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.performClick('ESRHomePage|buttonOKCreateFeedbackFilePopUp',function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            browser.pause(data.shortWait);
                        });
                    });
                    //   });
                });
            });
        });
    });

    this.Given(/^User generates the annual feedback file$/, function (browser) {

        action.performClick('ESRHomePage|dropdownCreateFeedbackFileType', function () {
            browser.timeoutsImplicitWait(data.miniWait);
            action.performClick('ESRHomePage|dropdownCreateFeedbackFileOptionAnnual', function () {
                browser.timeoutsImplicitWait(data.miniWait);
                //   action.setText('ESRHomePage|inputDateCreateFeedbackFile',"01-Jan-2015", function () {
                action.performClick('ESRHomePage|inputDateCreateFeedbackFile', function () {
                    action.performClick('ESRHomePage|datePickerSelect01', function () {
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.performClick('ESRHomePage|buttonOKCreateFeedbackFilePopUp',function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            browser.pause(data.shortWait);
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User navigates to Data_Files Tab$/, function (browser) {
        browser.pause(data.miniWait);
        action.performClick('ESRHomePage|tabDataFiles', function () {
            browser.pause(data.shortWait);
            //browser.timeoutsImplicitWait(data.shortWait);
            action.isDisplayed('ESRHomePage|titleDataFiles', function () {
                browser.timeoutsImplicitWait(data.miniWait);

            });
        });

    });

    this.Given(/^User navigates to Feedback_Files Tab$/, function (browser) {
        browser.pause(data.shortWait);
        action.performClick('ESRHomePage|tabFeedbackFiles', function () {
            browser.pause(data.shortWait);
            action.isDisplayed('ESRHomePage|titleFeedbackFiles', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            });
        });

    });

    this.Given(/^The user navigates to Client Page$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if(execEnv.toUpperCase() == "QA") {
            action.performClick('ESRHomePage|linkClientSLS', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "SANDBOX") {
            action.performClick('ESRHomePage|linkClientESRTST', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "STRESS") {
            action.performClick('ESRHomePage|linkClientAOLBAA', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "CITEST") {
            action.performClick('ESRHomePage|linkClientSLS', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "PROD") {
            action.performClick('ESRHomePage|linkClientSLS', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }

    });

    this.Given(/^The corresponding page title is displayed$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if(execEnv.toUpperCase() == "QA") {
            action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "SANDBOX") {
            action.isDisplayed('ESRHomePage|titleESRTSTPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "STRESS") {
            action.isDisplayed('ESRHomePage|titleAOLBAAPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "CITEST") {
            action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "PROD") {
            action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }

    });

    this.Given(/^The user navigates to the appropriate Client Page$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if(execEnv.toUpperCase() == "QA") {
            action.performClick('ESRHomePage|linkClientQAACME', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "SANDBOX") {
            action.performClick('ESRHomePage|linkClientChildR', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "STRESS") {
            action.performClick('ESRHomePage|linkClientACISTL', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "CITEST") {
            action.performClick('ESRHomePage|linkClientSLS', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "PROD") {
            action.performClick('ESRHomePage|linkClientSLS', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }

    });

    this.Given(/^The page title is displayed$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if(execEnv.toUpperCase() == "QA") {
            action.isDisplayed('ESRHomePage|titleQAACMEPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "SANDBOX") {
            action.isDisplayed('ESRHomePage|titleChildRPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "STRESS") {
            action.isDisplayed('ESRHomePage|titleACISTLPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "CITEST") {
            action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }
        else if(execEnv.toUpperCase() == "PROD") {
            action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                browser.timeoutsImplicitWait(data.shortWait);
            })
        }

    });

};