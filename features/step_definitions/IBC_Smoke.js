var action = require('./Keywords.js');
var Objects = require(__dirname + '/../../repository/IBC_locators.js');
//var robot = require('robot-js');
var data = require('../../TestResources/IBC_TestData.js');
var browser;

module.exports = function () {
    this.Then(/^Verify that Home Page loads successfully$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                browser.pause(6000);
                browser.assert.urlContains("TobsHome")
            }
        }
    });
    this.Then(/^Verify that "([^"]*)" text is displayed in the URL$/, function (Expectedtext) {
        this.demoTest = function (browser){
            browser.assert.urlContains(Expectedtext);
        };
    });
    this.Given(/^User should be able to see "([^"]*)" tabs in Home Page$/, function (browser) {
        action.isDisplayed("IBCMenu|WealthTab",function () {
        });
        action.isDisplayed("IBCMenu|HealthTab",function () {
        });
        action.isDisplayed("IBCMenu|AbsenceTab",function () {
        });
    });
    this.Given(/^Also User should be able to see Wealth, Health and Absence containers in Home Page$/, function (browser) {
        action.isDisplayed("IBCMenu|ImagemyWealthContainer",function () {
        });
        action.isDisplayed("IBCMenu|ImagemyHealthContainer",function () {
        });
        action.isDisplayed("IBCMenu|ImagemyAbsenceContaier",function () {
        });
    });
    this.When(/^User Clicks on HealthTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|HealthTab",function () {
        });
    });
    this.When(/^User Clicks on WealthTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|WealthTab",function () {
        });
    });
    this.When(/^User Clicks on TotalRewardsTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|TotalRewardsTab",function () {
        });
    });
    this.When(/^User Clicks on linkCurrentCoverages Link under health page$/, function (browser) {
        action.performClick("IBCHealthPage|linkCurrentCoverages",function () {
        });
    });
    this.When(/^User Clicks on FormsTab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|FormsTab",function () {
        });
    });
    this.Given(/^"([^"]*)" tabs should be displayed in Forms Page$/, function (browser) {
        action.isDisplayed("IBCFormsPage|tabWealthForms",function () {
        });
        action.isDisplayed("IBCFormsPage|tabHealthForms",function () {
        });
    });
    this.When(/^User Clicks on ResourceCentertab in Home Page$/, function (browser) {
        action.performClick("IBCMenu|ResourceCenterTab",function () {
        });
    });
    this.When(/^User Clicks on linkLoginManagement in Home Page/, function (browser) {
      //  if(data.TestingEnvironment == "QAI" ) {
            action.performClick("IBCHeaderlinks|linkLoginManagement", function () {
            });
       // }
    });
    this.When(/^User Clicks on LoginManagementlink in Home Page/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.performClick("IBCHeaderlinks|linkLoginManagement", function () {
                });
            }
        }
    });

    this.Given(/^User should be able to see fields "([^"]*)" fields in Account login information page$/, function (browser) {
        browser.timeoutsImplicitWait(80000);
        action.isDisplayed("IBCSecurityQuestionsPage|labelUsername",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelPassword",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelEmail",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelAnswers",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|linkChangeUsername",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|linkChangeEmailaddress",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|labelSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(6000);
        });
        action.isDisplayed("IBCSecurityQuestionsPage|linkChangeSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(6000);
        });
    });
    this.When(/^User Clicks on "([^"]*)" in Account Login Information Page$/, function (browser) {
        action.performClick("IBCSecurityQuestionsPage|linkChangeSecurityQuestions",function () {
            // browser.timeoutsImplicitWait(6000);
        });
    });
    this.When(/^User Change Answer for "([^"]*)" field$/, function (browser) {
           action.getInputBoxText("IBCSecurityQuestionsPage|inputtxtfieldAnswer1", function(txt){
           action.setText("IBCSecurityQuestionsPage|inputtxtfieldAnswer1",txt);
        });
    });
    this.When(/^User Clicks on "([^"]*)" button$/, function (browser) {
        action.performClick("IBCSecurityQuestionsPage|btnSave",function () {
        });
    });
    this.When(/^Verify User should be able to update account login information$/, function (browser) {
       // if(data.TestingEnvironment == "QAI" ) {
            action.isDisplayed("IBCSecurityQuestionsPage|confirmationmsg", function () {
            });
        // }
    });
    this.When(/^Verify User should be able to update login information$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.isDisplayed("IBCSecurityQuestionsPage|confirmationmsg", function () {
                });
            }
        }
    });
    this.When(/^User should be able to navigate SiteMap Page and title "SiteMap" should be displayed$/, function (browser) {
        this.demoTest = function (browser) {
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
            });
        };
        action.isDisplayed("IBCSiteMapPage|HeadingSiteMap",function () {
        });
        this.demoTest = function (browser){
            browser.closeWindow();
        };
    });
    this.When(/^User Clicks on SiteMapLink in Home Page$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkSiteMap",function () {
        });
    });

    this.When(/^User Clicks on Help Link in Header$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkHelp",function () {
            browser.pause(3000);
        });
    });
    this.Then(/^Verify that Help Page loads successfully$/, function (browser) {
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("help")
                console.log("Help Page loaded successfully");
                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
        });
    });
    this.When(/^User Clicks on ContactUs link in Header$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkContactUsinheader", function () {
            browser.pause(6000);
            });
        });
    this.When(/^Verify that Contact Us Page loads successfully$/, function (browser) {
            browser.window_handles(function (result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("ContactUsHome");
                console.log("Contact Us Page loads successfully");
                if (data.TestingEnvironment == "QAI" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CSO")
                {
                    action.elementDisplayedStatus("IBCContactUsPage|linkChatOnline", function (value) {
                        if (value) {
                            action.performClick("IBCContactUsPage|linkChatOnline", function () {
                                browser.pause(9000);
                                browser.assert.urlContains("ChatInquiry");
                                browser.assert.urlContains("mchat");
                                console.log("Moxie Chat page is displayed");
                                browser.back();
                                browser.pause(7000);
                            });
                        }
                    })

                }
                action.elementDisplayedStatus("IBCContactUsPage|linkVisitMessageCenter", function (value) {
                    if (value) {
                        action.performClick("IBCContactUsPage|linkVisitMessageCenter", function () {
                            browser.pause(15000);
                            browser.assert.urlContains("MessageCenterHome");
                            console.log("Visit Message Center page is displayed");
                            browser.back();
                            browser.pause(7000);
                        });
                    }
                })
                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
            });
    });
    this.When(/^User Clicks on PrivacyPolicy Link from footer$/, function (browser) {
        action.performClick("IBCLoginPage|linkPrivacyPolicy",function () {
            browser.pause(6000);
        });
    });
    this.When(/^Verify that Privacy Policy Page Opened in different window$/, function (browser) {
           browser.pause(6000);
            browser.window_handles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.assert.urlContains("G2PrivacyPolicy")
                console.log("Privacy Policy Window loaded successfully");
                browser.closeWindow();
                var handle1 = result.value[0];
                browser.switchWindow(handle1);
        });
    });
    this.When(/^User Clicks on Terms of Use link from footer$/, function (browser) {
        action.performClick("IBCLoginPage|linkTermsofUse",function () {
            browser.pause(6000);
        });
    });
    this.Then(/^Verify that Terms of Use Page Opened in different Window$/, function (browser) {
            browser.pause(6000);
            browser.window_handles(function(result) {
            var handle = result.value[1];
            browser.switchWindow(handle);
            browser.assert.urlContains("G2TermsOfUse")
            console.log("Terms Of Use Window loaded successfully");
            browser.closeWindow();
            var handle1 = result.value[0];
            browser.switchWindow(handle1);
        });
      });
    this.Then(/^Verify that Wealth Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("WealthTOBOutage")
    });
    this.Then(/^Health Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("HealthLanding")
    });
    this.Then(/^Current Coverage Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("hb-")
    });
    this.Then(/^Verify that Total Rewards Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("OnlineTotalRewardsHome")
    });
    this.Then(/^Verify that Forms Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("G2Forms")
    });
    this.Then(/^Verify that Resource Center Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("ResourceCenterHome")
    });
    this.Then(/^Verify that Login Management account information page loads successfully$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                browser.pause(6000);
                browser.assert.urlContains("LoginManagementOverview")
            }
        }
    });
    this.Then(/^Verify that Modify SQA Page loads successfully$/, function (browser) {
        browser.pause(6000);
        browser.assert.urlContains("ModifySQA")
    });
    this.When(/^User Clicks on Logout link in header$/, function (browser) {
        action.performClick("IBCHeaderlinks|linkLogOut",function () {
         browser.pause(12000);
            browser
                .acceptAlert()
        });
    });
    this.Then(/^Verify User should be able to log out successfully$/, function (browser) {
        action.isDisplayed("IBCLoginPage|LogoutMessage",function () {
        });
    });
    this.When(/^User Enter Security Answer1$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.getText("IBCForgotPasswordYourSecurityQuestionsPage|inputlabelsecuriyquestion1", function (txt) {
                    var replacedstring = txt.replace("?", "");
                    var res = replacedstring.split(" ");
                    // console.log(res)
                    var lastarrayvalue = res[res.length - 1];
                    // console.log(lastarrayvalue)
                    action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputSecurityAnswer1", lastarrayvalue);
                    action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnSQANext", function () {
                        browser.timeoutsImplicitWait(30000);
                    });
                })
            }
        }
    });
    this.When(/^User Enter Security Answer2$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.getText("IBCForgotPasswordYourSecurityQuestionsPage|inputlabelsecuriyquestion1", function (txt) {
                    var replacedstring = txt.replace("?", "");
                    var res = replacedstring.split(" ");
                    // console.log(res)
                    var lastarrayvalue = res[res.length - 1];
                    // console.log(lastarrayvalue)
                    action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputSecurityAnswer1", lastarrayvalue);
                    action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnSQANext", function () {
                        browser.timeoutsImplicitWait(30000);
                    });
                })
            }
        }
    });
    this.When(/^User Enter New Password$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputnewpassword1", data.NewPassword);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputreenternewpassword1", data.NewPassword);
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnContainerIDNext", function () {
                    browser.timeoutsImplicitWait(30000);
                });
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnFinishNext", function () {
                    browser.timeoutsImplicitWait(30000);
                });
            }
        }
    });
    this.Then(/^Verify Confirmation Message after saving information$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.isDisplayed("IBCForgotPasswordYourSecurityQuestionsPage|TitleInformationsaved", function () {
                });
            }
        }
    });
    this.When(/^Clicks on Continue button$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnContinue", function () {
                    browser.pause(3000);
                });
            }
        }
    });
    this.When(/^User Clicks on linkChangePassword$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.performClick("IBCLoginManagementPage|linkChangePassword", function () {
                    browser.pause(3000);
                });
            }
        }
    });
    this.Then(/^Verify that Change Your Password page loads successfully$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                browser.pause(6000);
                browser.assert.urlContains("ModifyPassword")
            }
        }
    });
    this.When(/^Change the Password$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        if(n != true) {
            if (data.TestingEnvironment == "QAI") {
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputOldPassword", data.NewPassword);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputNewPassword", data.DefaultPassword);
                action.setText("IBCForgotPasswordYourSecurityQuestionsPage|inputReNewPassword", data.DefaultPassword);
                action.performClick("IBCForgotPasswordYourSecurityQuestionsPage|btnSaveModifyPassword", function () {
                    browser.timeoutsImplicitWait(30000);
                });
            }
        }
    });
    this.Given(/^Verify Tabs displayed in IBC Home Page$/, function (browser) {
        var n = data.urlQAI.includes("PBSPHL")
        action.elementDisplayedStatus("IBCMenu|WealthTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|WealthTab", function () {
                    browser.pause(6000);
                    browser.assert.urlContains("Wealth")
                    console.log("Wealth page is loaded successfully");
                });
            }
        })
        action.elementDisplayedStatus("IBCMenu|HealthTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|HealthTab", function () {
                    browser.pause(6000);
                    browser.assert.urlContains("Health")
                    console.log("Health page is loaded successfully");
                    // action.performClick("IBCHealthPage|linkCurrentCoverages", function () {
                    //     browser.pause(6000);
                    //     // browser.assert.urlContains("hb-")
                    //     browser.assert.urlContains("CurrentCoverage")
                    // });
                });
            }
        })
        //       action.elementDisplayedStatus("IBCMenu|AbsenceTab",function(value){
        // if (value) {
        // action.performClick("IBCMenu|AbsenceTab", function () {
        //     browser.pause(5000);
        //     browser.assert.urlContains("DocProdCommunicationHistory");
        //     console.log("DocProdCommunicationHistory page is displayed");
        // });
        //  }
        // })
        var n = data.urlQAI.includes("PBSPHL")
        if (n && data.TestingEnvironment == "QAI") {
             console.log("Total Rewards tab is not found in Headerlist");
        }
        else {
            action.performClick("IBCMenu|TotalRewardsTab", function () {
                browser.pause(6000);
                browser.assert.urlContains("OnlineTotalRewardsHome")
                console.log("Total Rewards page is loaded successfully");
            });
        }

        action.elementDisplayedStatus("IBCMenu|FormsTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|FormsTab", function () {
                    browser.pause(6000);
                    browser.assert.urlContains("G2Forms")
                    console.log("Forms page is loaded successfully");
                });
            }
        })
        action.elementDisplayedStatus("IBCMenu|ResourceCenterTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|ResourceCenterTab", function () {
                    browser.pause(6000);
                    browser.assert.urlContains("ResourceCenterHome.")
                    console.log("Resource Center Page is loaded successfully");
                });
            }
        })

        var n = data.urlQAI.includes("PBSPHL")
        if (n && data.TestingEnvironment == "QAI") {
            action.elementDisplayedStatus("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.pause(6000);
                        browser.assert.urlContains("G2AdminLandingPage")
                        console.log("Administration Page is loaded successfully");
                    });
                }
            })
        }
        if (data.TestingEnvironment == "PROD" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CSO" || data.TestingEnvironment == "CIDEV") {
            action.elementDisplayedStatus("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.pause(4000);
                        browser.assert.urlContains("G2AdminLandingPage.")
                        console.log("Administration Page is loaded successfully");
                    });
                }
            })
        }
        action.elementDisplayedStatus("IBCMenu|HomeTab", function (value) {
            if (value) {
                action.performClick("IBCMenu|HomeTab", function () {
                    browser.pause(6000);

                    browser.assert.urlContains("TobsHome")
                    console.log("Home Page is loaded successfully");
                });
            }
        })

    });
    this.Then(/^Containers displayed in Home Page$/, function (browser) {
        action.performClick("IBCMenu|HomeTab",function () {
            browser.pause(1000);
        });
        action.elementDisplayedStatus("IBCMenu|ImagemyWealthContainer",function (value) {
            if (value) {
                console.log("Wealth Container displayed in Home Page")
            }
        })
        action.elementDisplayedStatus("IBCMenu|ImagemyHealthContainer",function (value) {
            if (value) {
                console.log("Health Container displayed in Home Page")
            }
        })
        // action.elementDisplayedStatus("IBCMenu|ImagemyAbsenceContaier",function (value) {
        //     if (value) {
        //         console.log("Absence Container displayed in Home Page")
        //     }
        // })
    });
    this.Given(/^User Clicks on Administration link$/, function (browser) {
        if (data.TestingEnvironment == "PROD" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CSO" || data.TestingEnvironment == "QAI"  || data.TestingEnvironment == "CIDEV")
        {
            action.elementDisplayedStatus("IBCMenu|AdministrationTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|AdministrationTab", function () {
                        browser.pause(4000);
                        browser.assert.urlContains("G2AdminLandingPage")
                        console.log("Administration Page is loaded successfully");
                    });
                }
            })
        }
    });
    this.Then(/^Verify that Admin Landing Page loads successfully$/, function (browser) {
        browser.pause(4000);
        browser.assert.urlContains("G2AdminLandingPage")
        console.log("Administration Page is loaded successfully");
    });
    this.Given(/^User Clicks on Employee Search link$/, function (browser) {
            action.elementDisplayedStatus("IBCAdminLandingPage|linkEmployeesearch", function (value) {
                if (value) {
                    action.performClick("IBCAdminLandingPage|linkEmployeesearch", function () {
                        browser.pause(4000);
                        browser.assert.urlContains("G2EmployeeSearch")
                        console.log("Employee Search Page is loaded successfully");
                    });
                }
            })
    });
    this.Then(/^Verify that Employee Search Page loads successfully$/, function (browser) {
        browser.pause(4000);
        browser.assert.urlContains("G2EmployeeSearch")
        console.log("Employee Search Page is loaded successfully");
    });
    this.When(/^User enter SSN and click on search button$/, function (browser) {
        // Search by SSN
        action.performClick("IBCAdminLandingPage|SearchBySSN", function () {
            browser.timeoutsImplicitWait(3000);
        });

        if(data.TestingEnvironment == "QAI") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyQAI);
        }
        if(data.TestingEnvironment == "CIDEV") {
            action.setText("IBCAdminLandingPage|inputKeyword", data.SSNforProxyCIDEV);
        }

        action.performClick("IBCAdminLandingPage|btnSearchProxy", function () {
            browser.timeoutsImplicitWait(30000);
        });
        browser.pause(4000);
        browser.assert.urlContains("G2EmployeeSearch")
        console.log("Results displayed successfully");
        // Search by Lastname
        action.performClick("IBCAdminLandingPage|SearchByLastName", function () {
            browser.timeoutsImplicitWait(3000);
        });
        action.getText("IBCAdminLandingPage|SSNfirstrowresult", function (txt) {
            var n = txt.includes("*****");
            if(n){
                console.log("Five digit of the SSN Number is masked");
            }
        })
        action.getText("IBCAdminLandingPage|LastNamefirstrowresult", function (txt) {
            var res = txt.trim();
            action.setText("IBCAdminLandingPage|inputKeyword", res);
            action.performClick("IBCAdminLandingPage|btnSearchProxy", function () {
                browser.timeoutsImplicitWait(30000);
            });
            browser.pause(4000);
            browser.assert.urlContains("G2EmployeeSearch")
            console.log("Results displayed successfully");
        })

    });
    this.Then(/^Verify that user should be able to see results$/, function (browser) {
        browser.pause(4000);
        browser.assert.urlContains("G2EmployeeSearch")
        console.log("Results displayed successfully");
    });
    this.When(/^User clicks on Select link$/, function (browser) {
            action.elementDisplayedStatus("IBCAdminLandingPage|selectlink", function (value) {
                if (value) {
                    action.performClick("IBCAdminLandingPage|selectlink", function () {
                        browser.pause(6000);
                        browser.assert.urlContains("WelcomePage")
                        console.log("Welcome Page loads after impersonate");
                    });
                }
            })
    });
    this.Then(/^Verify User should be able to see heading You are currently impersonating xxxx in the header$/, function (browser) {
        action.elementDisplayedStatus("IBCAdminLandingPage|labelImpersonate", function (value) {
            if (value) {
                console.log("You are currently impersonating 'xxxxx' user heading is displayed")
            }
        })
    });
    this.Given(/^Verify DC Plan link in Wealth Menu$/, function (browser) {
        if (data.TestingEnvironment == "PROD" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CIDEV") {
            action.elementDisplayedStatus("IBCMenu|WealthTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|WealthTab", function () {
                        browser.pause(6000);
                        browser.assert.urlContains("Wealth")
                        console.log("Wealth page is loaded successfully");
                        var objLocation = Objects.sections.IBCMenu.elements.PlansDropdown.selector;
                        var locateStrategy = Objects.sections.IBCMenu.elements.PlansDropdown.locateStrategy;
                        browser.useXpath().moveToElement(objLocation, 3, 3);
                    });
                }
            })
            action.elementDisplayedStatus("IBCMenu|Plan401", function (value) {
                if (value) {
                    action.performClick("IBCMenu|Plan401", function () {
                        browser.pause(8000);
                        browser.assert.urlContains("dcaccounts")
                        console.log("dc-accounts Page is loaded successfully");
                        browser.back();
                        browser.pause(4000);
                    });
                }
            })
        }
    });
    this.Given(/^Verify DB Plan link in Wealth Menu$/, function (browser) {
        if (data.TestingEnvironment == "PROD" || data.TestingEnvironment == "CITEST" || data.TestingEnvironment == "CIDEV") {
            action.elementDisplayedStatus("IBCMenu|WealthTab", function (value) {
                if (value) {
                    action.performClick("IBCMenu|WealthTab", function () {
                        browser.pause(6000);
                        browser.assert.urlContains("Wealth")
                        console.log("Wealth page is loaded successfully");
                        var objLocation = Objects.sections.IBCMenu.elements.PlansDropdown.selector;
                        var locateStrategy = Objects.sections.IBCMenu.elements.PlansDropdown.locateStrategy;
                        browser.useXpath().moveToElement(objLocation, 3, 3);
                    });
                }
            })
            action.elementDisplayedStatus("IBCMenu|WealthTab", function (value) {
                if (value) {
                        action.performClick("IBCMenu|PlanMCNNonRepresented", function () {
                            browser.pause(8000);
                            browser.assert.urlContains("pm4")
                            console.log("pm4 Page is loaded successfully");
                            browser.back();
                            browser.pause(4000);
                    });
                }
            })
        }
    });
 };


