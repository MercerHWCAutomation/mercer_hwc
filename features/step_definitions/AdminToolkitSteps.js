/**
 * Created by subhajit-chakraborty on 5/16/2017.
 */

var data = require('../../TestResources/AdminToolkitGlobalData.js');
var BrowserData = require('../../TestResources/GlobalTestData.js');
var Objects = require(__dirname + '/../../repository/AdminToolkitPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');

var ClientListPage,WebConfigPage,EpRulesPage,ClientSettingPage,BestMatchPage,PromotePage,BPOCarrierDescPage;

function initializePageObjects(browser, callback) {
    var ToolkitPage = browser.page.AdminToolkitPages();
    ClientListPage = ToolkitPage.section.ClientListPage;
    WebConfigPage = ToolkitPage.section.WebConfigurationPage;
    EpRulesPage = ToolkitPage.section.EPRulesPage;
    ClientSettingPage = ToolkitPage.section.ClientSettingPage;
    BestMatchPage = ToolkitPage.section.BestMatchPage;
    PromotePage = ToolkitPage.section.PromotePage;
    BPOCarrierDescPage = ToolkitPage.section.BPOCarrierDescPage;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the browser and launch the AdminTool kit URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QASANDBOX"|| execEnv.toUpperCase() == "QAINT") {
            URL = data.AdminToolkitURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                ClientListPage.waitForElementVisible('@BenefitsHubHeader', data.longWait)
                    .waitForElementVisible('@ClientListHeader', data.longWait)
                    .waitForElementVisible('@FirstClientListName', data.longWait)
            });
        }
    });

    this.When(/^User clicks on Client Link and Web configuration Page is displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
       // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
                ClientListPage.waitForElementVisible('@FilterTabBenHubPage', data.longWait)
                    .setValue('@FilterTabBenHubPage',data.ClientNameContentEPAddRule)
                    browser.pause(data.shortWait);
                ClientListPage.waitForElementVisible('@FirstClientListName', data.longWait)
                    .click('@FirstClientListName');
                browser.pause(data.MedWait);
                WebConfigPage.waitForElementVisible('@WebConfigHeader',data.longWait)
        }
    });

    this.When(/^User enter Content EP Values and Corresponding EP details should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            WebConfigPage.waitForElementVisible('@WebConfigHeader',data.longWait)
                .waitForElementPresent('@ContentEPValueFilter',data.longWait)
                .setValue('@ContentEPValueFilter',data.ContentEPValue);
            browser.pause(data.longWait);
            WebConfigPage.waitForElementVisible('@EPValue1Description',data.longWait);
        }
    });

    this.When(/^User clicks on EP description link and should navigate to Ep Rules page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            WebConfigPage.waitForElementVisible('@EPValue1Description',data.longWait)
                .click('@EPValue1Description')
            EpRulesPage.waitForElementPresent('@RulesHeader',data.LoginWait);
            browser.pause(data.MedWait);
        }
    });

    this.When(/^User Adds a new Rule for the Ep and Congratulation Msg Appears$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            EpRulesPage.waitForElementVisible('@AddRuleButton',data.LoginWait)
                .click('@AddRuleButton')

                //Enter all the data
                .waitForElementVisible('@CreateNewRuleHeader',data.longWait)
                .waitForElementVisible('@NewRuleName',data.longWait)
                .setValue('@NewRuleName',data.NewRuleName)
                .waitForElementVisible('@NewRuleDescription',data.longWait)
                .setValue('@NewRuleDescription',data.NewRuleDescription)


                .waitForElementVisible('@ExpressionTextarea',data.longWait)
                .click('@ExpressionTextarea');
                 browser.pause(data.shortWait);
            EpRulesPage.sendKeys('@ExpressionTextAreaValue',data.ExpressionNewRule)
                // .sendKeys(data.ExpressionNewRule)
               //browser.setValue('@ExpressionTextAreaValue',resolve(data.ExpressionNewRule))


                .waitForElementVisible('@BenefitSummaryTextbox',data.longWait)
                .setValue('@BenefitSummaryTextbox',data.NewRuleBenSummary)
                .waitForElementVisible('@YourBenHeaderTextbox',data.longWait)
                .setValue('@YourBenHeaderTextbox',data.BenefitHeaderTextbox)
                .waitForElementVisible('@BenefitContentTextbox',data.longWait)
                .setValue('@BenefitContentTextbox',data.BenefitContentTextbox)

                //Select Value from the dropdown
                .waitForElementVisible('@SpendingDDList',data.longWait)
                .click('@SpendingDDList');

            if(data.ClientNameContentEPAddRule=="QAACME")
            {
                EpRulesPage.waitForElementVisible('@RetMedUOption',data.longWait)
                    .click('@RetMedUOption');
            }
            else if(data.ClientNameContentEPAddRule=="UHCMSP" || data.ClientNameContentEPAddRule=="MBC QA CLIENT THREE")
            {
                EpRulesPage.waitForElementVisible('@HSAOptionSpendingDDList',data.longWait)
                    .click('@HSAOptionSpendingDDList');
            }

                //Validate the Syntax
            EpRulesPage.waitForElementVisible('@ValidateSyntaxButton',data.longWait)
                .click('@ValidateSyntaxButton');
                 browser.pause(data.shortWait);
            //EpRulesPage.waitForElementVisible('@ConfirmationMsgSyntax',data.LoginWait);
            //      browser.pause(data.shortWait);


                //Click on the Save Button
            EpRulesPage.waitForElementVisible('@RuleSaveButton',data.longWait)
                  .click('@RuleSaveButton')

                //checking the Confirmation Msg Appearence
                 .waitForElementVisible('@ConfirmationMsgRule',data.LoginWait);
        }
    });

    this.Then(/^User able to delete the Created Rule$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
      //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            EpRulesPage.waitForElementVisible('@FilterEPTextbox',data.longWait)
                .setValue('@FilterEPTextbox',data.NewRuleName);
            browser.pause(data.shortWait)
            EpRulesPage.waitForElementVisible('@RuleDescDeletePage',data.longWait)
                .click('@RuleDescDeletePage');
            browser.pause(data.shortWait)
            EpRulesPage.waitForElementVisible('@DeleteRuleLink',data.longWait)
                .click('@DeleteRuleLink')
                .waitForElementVisible('@DeleteTabConfirmation',data.longWait)
                .click('@DeleteTabConfirmation')
                .waitForElementVisible('@ConfirmationDeleteRule',data.LoginWait)
                .waitForElementNotPresent('@RuleNameSearch1',data.LoginWait)
        }
    });

    this.When(/^User enter Content Hybrid EP Values and Corresponding EP details should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            WebConfigPage.waitForElementVisible('@WebConfigHeader',data.longWait)
                .waitForElementPresent('@ContentEPValueFilter',data.longWait)
                .setValue('@ContentEPValueFilter',data.HybridEPValue);
            browser.pause(data.longWait);
            WebConfigPage.waitForElementVisible('@EPValue1Description',data.longWait);
        }
    });

    this.When(/^User edit the corresponding EP and clicks on Save button for Confirmation Msg$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX") {
            EpRulesPage.waitForElementVisible('@EditRule',data.LoginWait)
                .click('@EditRule')
                .waitForElementVisible('@EditNameHybridEdit',data.LoginWait)
                .clearValue('@EditNameHybridEdit')
                .setValue('@EditNameHybridEdit',data.HybridEpUpdatedName)
                .waitForElementVisible('@SaveButtonEditHybridRule',data.LoginWait)
                .click('@SaveButtonEditHybridRule')
                .waitForElementVisible('@ConfirmationEditedRule',data.LoginWait);
        }
        else if (execEnv.toUpperCase() == "QAINT") {
            EpRulesPage.waitForElementVisible('@EditRule',data.LoginWait)
                .click('@EditRule')
                .waitForElementVisible('@SelectDropdownINT',data.LoginWait)
                .click('@SelectDropdownINT')
                .waitForElementVisible('@AccidentOptionSDINT',data.LoginWait)
                .click('@AccidentOptionSDINT')
                .waitForElementVisible('@SaveButtonEditHybridRule',data.LoginWait)
                .click('@SaveButtonEditHybridRule')
                .waitForElementVisible('@ConfirmationEditedRule',data.LoginWait);
        }
    });

    this.When(/^User enter Javascript EP Values and Corresponding EP details should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            WebConfigPage.waitForElementVisible('@WebConfigHeader',data.longWait)
                .waitForElementPresent('@ContentEPValueFilter',data.longWait)
                .setValue('@ContentEPValueFilter',data.JavascriptEPValue);
            browser.pause(data.longWait);
            WebConfigPage.waitForElementVisible('@EPValue1Description',data.longWait);
        }
    });

    this.When(/^User Adds a new Rule for Javascript Ep and Congratulation Msg Appears$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            EpRulesPage.waitForElementVisible('@AddRuleButton',data.LoginWait)
                .click('@AddRuleButton')

                //Enter all the data
                .waitForElementVisible('@CreateNewRuleHeader',data.longWait)
                .waitForElementVisible('@NewRuleName',data.longWait)
                .setValue('@NewRuleName',data.JavascriptNewRuleName)
                .waitForElementVisible('@NewRuleDescription',data.longWait)
                .setValue('@NewRuleDescription',data.JavascriptNewRuleDescription)


                .waitForElementVisible('@ExpressionTextarea',data.longWait)
                .click('@ExpressionTextarea');
            browser.pause(data.shortWait);
            EpRulesPage.sendKeys('@ExpressionTextAreaValue',data.JavascriptExpressionNewRule)


            //Validate the Syntax
                .waitForElementVisible('@ValidateSyntaxButton',data.longWait)
                .click('@ValidateSyntaxButton');
                browser.pause(data.shortWait);


            //Click on the Save Button
            EpRulesPage.waitForElementVisible('@RuleSaveButton',data.longWait)
                .click('@RuleSaveButton')

            //checking the Confirmation Msg Appearence
                .waitForElementVisible('@ConfirmationMsgRule',data.LoginWait);
        }
    });

    this.When(/^User able to delete the Javascript Created Rule$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            EpRulesPage.waitForElementVisible('@FilterEPTextbox',data.longWait)
                .setValue('@FilterEPTextbox',data.JavascriptNewRuleName);
            browser.pause(data.shortWait)
            EpRulesPage.waitForElementVisible('@RuleDescDeletePage',data.longWait)
                .click('@RuleDescDeletePage');
            browser.pause(data.shortWait)
            EpRulesPage.waitForElementVisible('@DeleteRuleLink',data.longWait)
                .click('@DeleteRuleLink')
                .waitForElementVisible('@DeleteTabConfirmation',data.longWait)
                .click('@DeleteTabConfirmation')
                .waitForElementVisible('@ConfirmationDeleteRule',data.LoginWait)
                .waitForElementNotPresent('@RuleNameSearch1',data.LoginWait)
        }
    });

    this.When(/^User able to navigate to HB Client Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@ClientSettingLink',data.LoginWait)
                .click('@ClientSettingLink')
                .waitForElementVisible('@HBClientSettingLink',data.LoginWait)
                .click('@HBClientSettingLink')
                .waitForElementVisible('@ClientSettingHeader',data.LoginWait);

            //Condition for clicking Radio Button Based on User for HB
            if (data.Dynamic == "Y") {ClientSettingPage.click('@DynamicRadioButton')}
            else if(data.Precalculated =="Y") {ClientSettingPage.click('@PreCalculateRadioButton')}

            //ClientSettingPage.waitForElementVisible('@SaveButtonClientSetting',data.LoginWait)
            //    .click('@SaveButtonClientSetting')
            //    .waitForElementVisible('@CongratulationMsgHB',data.LoginWait)


        }
    });

    this.When(/^User should able to save changes in HB client setting tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@SaveButtonClientSetting',data.LoginWait)
                .click('@SaveButtonClientSetting')
                .waitForElementVisible('@CongratulationMsgHB',data.LoginWait);
        }
    });

    this.When(/^User able to navigate to DB Client Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX") {
            ClientSettingPage.waitForElementVisible('@DBClientSettingLink',data.LoginWait)
                .click('@DBClientSettingLink')
                .waitForElementVisible('@DBSettingHeader',data.LoginWait)
                .waitForElementVisible('@RelativeFolderTextbox',data.LoginWait)
                .clearValue('@RelativeFolderTextbox')
                .setValue('@RelativeFolderTextbox',data.RelativeFolderValue);
        }
        else if (execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@DBClientSettingLink',data.LoginWait)
                .click('@DBClientSettingLink')
                .waitForElementVisible('@DBSettingHeader',data.LoginWait)
                .waitForElementVisible('@HideRadioButton',data.LoginWait)
                .click('@HideRadioButton');
        }
    });

    this.Then(/^User should able to save changes in DB client setting tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@SaveButtonDB',data.LoginWait)
                .click('@SaveButtonDB')
                .waitForElementVisible('@CongratulationMsgDB',data.LoginWait);
        }
    });

    this.When(/^User navigates to Visibility Configuration page and make changes$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@ClientSettingLink',data.LoginWait)
                .click('@ClientSettingLink');
            ClientSettingPage.waitForElementVisible('@VisibiltyClientConfigLink',data.LoginWait)
                .click('@VisibiltyClientConfigLink')
                .waitForElementVisible('@VisibilityDateEnableRadioButton',data.LoginWait);

            if (data.VisibilityDateEnable=="Y") {ClientSettingPage.click('@VisibilityDateEnableRadioButton')}
            else if(data.VisibilityDateEnable=="N") {ClientSettingPage.click('@VisibilityDateDisableRadioButton')}
        }
    });

    this.When(/^User saves the Visibility configuration msg and Confirmation dialog appears$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@VisibilitySaveButton',data.LoginWait)
                .click('@VisibilitySaveButton')
                .waitForElementVisible('@ConfirmationMsgVisibilityConfig',data.LoginWait);
        }
    });

    this.When(/^User navigates to the Portal view tab and make changes$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@PortalLink',data.LoginWait)
                .click('@PortalLink')
                .waitForElementVisible('@OETileDisplayTextbox',data.LoginWait)
                .setValue('@OETileDisplayTextbox',data.OETileDisplayOrderValue)
                //.waitForElementVisibility('@PortalSaveButton',data.LoginWait)
                //.click('@PortalSaveButton')
                //.waitForElementVisible('@ConfirmationMsgPortal',data.LoginWait);
        }
    });

    this.Then(/^User saves the Portal view tab and Confirmation msg is displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientSettingPage.waitForElementVisible('@PortalSaveButton',data.LoginWait)
                .click('@PortalSaveButton')
                .waitForElementVisible('@ConfirmationMsgPortal',data.LoginWait);
        }
    });

    this.When(/^User clicks the best match link and edit actuarial value$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            BestMatchPage.waitForElementVisible('@BestMatchLink',data.LoginWait)
                .click('@BestMatchLink')
            browser.pause(data.shortWait)
            BestMatchPage
                //.waitForElementVisible('@EditFirstBestMatch',data.LoginWait)
                //.click('@EditFirstBestMatch')
                .waitForElementVisible('@ActuarialValue',data.CarrierDescLoadTime)
                .clearValue('@ActuarialValue')
                .setValue('@ActuarialValue',data.ActuarialValue);
        }
    });

    this.When(/^User clicks on save and confirmation for Best match is displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            browser.pause(data.shortWait)
            BestMatchPage.waitForElementVisible('@SaveButtonBestMatch',data.LoginWait)
                         .click('@SaveButtonBestMatch')
                         .waitForElementVisible('@BestMatchConfirmationMsg',data.LoginWait);

        }
    });

    this.When(/^User opens the promote tab and select one and promote the EP$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            PromotePage.waitForElementVisible('@PromoteLink',data.LoginWait)
                .click('@PromoteLink');
            browser.pause(60000)

            //clicking the checkbox for the Desired User given Value
            var xpath1 = "//td[contains(text(),'".concat(data.ItemToPromoteName).concat("')]/../td[1]/input");
            browser.useXpath().waitForElementPresent(xpath1,data.LoginWait).click(xpath1);
            browser.pause(data.LoginWait)



        }
    });

    this.When(/^User clicks on BPO_Carrier Description tab and page is loaded$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            BPOCarrierDescPage.waitForElementVisible('@BPOCarrDescLink',data.LoginWait)
                .click('@BPOCarrDescLink')
            //browser.pause(data.CarrierDescLoadTime)
            //BPOCarrierDescPage.waitForElementVisible('@CarrierDescLink',data.CarrierDescLoadTime)
        }
    });

    this.When(/^User clicks on Carrier Description link and edit Details on an EP$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX") {
            BPOCarrierDescPage.waitForElementVisible('@CarrierDescLink',data.CarrierDescLoadTime)
                .click('@CarrierDescLink')
                .waitForElementVisible('@CarrierDescHeader',data.LoginWait)
                .waitForElementPresent('@FirstEditOption',data.LoginWait)
                .click('@FirstEditOption')
                .waitForElementVisible('@FirstLongDescTextbox',data.LoginWait)
                .clearValue('@FirstLongDescTextbox')
                .setValue('@FirstLongDescTextbox',data.LongDescriptionText);
        }

        else if (execEnv.toUpperCase() == "QAINT") {
            BPOCarrierDescPage.waitForElementVisible('@CarrierDescLink',data.CarrierDescLoadTime)
                .click('@CarrierDescLink')
                .waitForElementVisible('@CarrierDescHeader',data.LoginWait)
        }
    });

    this.Then(/^User saves the editing for BPO_CarrierDesc and Confirmation Msg is displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //  console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            BPOCarrierDescPage.waitForElementVisible('@SaveButtonBPO',data.LoginWait)
                .click('@SaveButtonBPO');
            BPOCarrierDescPage.waitForElementVisible('@ConfirmationMsgBPO',data.LoginWait);
        }
    });




    //new addition
    this.Given(/^User opens the browser and launch the AdminTool kit URl GG$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QASANDBOX"|| execEnv.toUpperCase() == "QAINT") {
            URL = data.AdminToolkitURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                ClientListPage.waitForElementVisible('@BenefitsHubHeader', data.longWait)
                    .waitForElementVisible('@ClientListHeader', data.longWait)
                    .waitForElementVisible('@FirstClientListName', data.longWait)
            });
        }
    });

    this.When(/^User clicks on Client Link and Web configuration Page is displayed GG$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QASANDBOX" || execEnv.toUpperCase() == "QAINT") {
            ClientListPage.waitForElementVisible('@FilterTabBenHubPage', data.longWait)
                .setValue('@FilterTabBenHubPage',data.ClientNameContentEPAddRule)
            browser.pause(data.shortWait);
            ClientListPage.waitForElementVisible('@FirstClientListName', data.longWait)
                .click('@FirstClientListName');
            browser.pause(data.MedWait);
            WebConfigPage.waitForElementVisible('@WebConfigHeader',data.longWait)
        }
    });



}
