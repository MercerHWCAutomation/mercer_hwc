/**
 * Created by subhajit-chakraborty on 7/6/2017.
 */

var data = require('../../TestResources/Get_InsuredGlobalData.js');
var BrowserData = require('../../TestResources/GlobalTestData.js');
var Objects = require(__dirname + '/../../repository/Get_InsuredPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');

var GetInsuredPage,LandingPageGI,ContactUsPage,MedicalIDPage,HIMPage,SocialSecurityPage,FAQPage,ShopForHealthInsurancePage;

function initializePageObjects(browser, callback) {
    var GetInsuredPage = browser.page.Get_InsuredPages();
    LandingPageGI = GetInsuredPage.section.LandingPage;
    ContactUsPage = GetInsuredPage.section.ContactUsPage;
    MedicalIDPage = GetInsuredPage.section.MedicalIDPage;
    HIMPage = GetInsuredPage.section.HIMPage;
    SocialSecurityPage = GetInsuredPage.section.SocialSecurityPage;
    FAQPage = GetInsuredPage.section.FAQPage;
    ShopForHealthInsurancePage = GetInsuredPage.section.ShopForHealthInsurancePage;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the getInsured URl "([^"]*)"$/, function (GI_URL) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                console.log(GI_URL);
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(GI_URL);
                browser.timeoutsImplicitWait(30000);
                LandingPageGI.waitForElementVisible('@TollFreeNumberText',data.LoginWait)
                    .waitForElementVisible('@HomeLink',data.LoginWait)
                    .waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .waitForElementVisible('@HRARetireeAccount',data.LoginWait)
                   // .waitForElementVisible('@SignUpLink',data.LoginWait)
                    .waitForElementVisible('@LogInLink',data.LoginWait)
               // action.elementDisplayedStatus("LandingPage|GetStartedButton", function (status) {
               //     if (status == true) {
               //         LandingPageGI.waitForElementVisible('@GetStartedButton',data.LoginWait)
               //     }
               //     else
               //     {
               //         console.log("ABC");
               //     }
               // });


            });
        }
    });

    this.When(/^User able to verify the tollfree number "([^"]*)"$/, function (TollNumber) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                browser.useXpath().assert.containsText("//a[@id='ClickTrackIVRNumberNull']/../a[@id='ClickTrackNumberNotNull']",TollNumber);
            });
        }
    });

    this.When(/^User able to Verify Contact Us header of that page$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .click('@ContactUsLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.longWait)
                });
                ContactUsPage.waitForElementVisible('@ContactUsMarketplaceHeader',data.LoginWait);
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
            });
        }
    });

    this.When(/^User able to navigate to hompage after clicking Home Button$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@HomeLink',data.LoginWait)
                    .click('@HomeLink');
                LandingPageGI.waitForElementVisible('@HomeLink',data.LoginWait)
                    .waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .waitForElementVisible('@HRARetireeAccount',data.LoginWait)
            });
        }
    });

    this.When(/^User able to validate help link headers of Landing Pages$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
       // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                //Medical ID Link Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@MedicaIDLink',data.LoginWait)
                    .click('@MedicaIDLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //MedicalIDPage.waitForElementVisible('@MedicalIDImg',data.LoginWait);
                browser.assert.urlContains('https://www.medicaid.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);


                //Health Insurance Marketplace Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@HIMLink',data.LoginWait)
                    .click('@HIMLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //HIMPage.waitForElementVisible('@HealthInsuranceHeader',data.LoginWait);
                browser.assert.urlContains('//www.healthcare.gov/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);


                //Social Security Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@SocialSecurityLink',data.LoginWait)
                    .click('@SocialSecurityLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

               // SocialSecurityPage.waitForElementVisible('@SocialSecurityHeader',data.LoginWait);
                browser.assert.urlContains('https://www.ssa.gov/');

                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);


                // FAQ Page Verification
                LandingPageGI.waitForElementVisible('@HelpfulLinks',data.LoginWait)
                    .click('@HelpfulLinks')
                    .waitForElementVisible('@FAQsLink',data.LoginWait)
                    .click('@FAQsLink');

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                //HIMPage.waitForElementVisible('@HealthInsuranceHeader',data.LoginWait);
                browser.assert.urlContains('https://www.healthcare.gov/get-answers/');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                LandingPageGI.waitForElementVisible('@ContactUsLink',data.LoginWait)
                    .waitForElementVisible('@HelpfulLinks',data.LoginWait);

            });
        }
    });

    this.When(/^User should able to verify the login page of "([^"]*)" Portals$/, function (Test_URL) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
              if(Test_URL.includes("subsidy"))
              {
                  LandingPageGI.waitForElementVisible('@GetStartedLoginGatedPortal',data.LoginWait)
                      .click('@GetStartedLoginGatedPortal')
                      .waitForElementVisible('@FirstnameTextbox',data.LoginWait)
                      .waitForElementVisible('@LastnameTextbox',data.LoginWait)
                      .waitForElementVisible('@SSNTextbox',data.LoginWait)
                      .waitForElementVisible('@HomeLink',data.LoginWait)
                      .click('@HomeLink')
                      .waitForElementVisible('@GetStartedLoginGatedPortal',data.LoginWait);
              }
              else if(Test_URL.includes("accessonly"))
              {
                  LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait)
                      .click('@StartShoppingLoginUngatedPortal')
                      .waitForElementVisible('@ShoppingHeaderUnGatedPortal',data.LoginWait)
                      .waitForElementVisible('@HomeLink',data.LoginWait)
                      .click('@HomeLink')
                      .waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait);
              }
            });
        }
    });

    this.When(/^User should able to verify the footer Paragraphs$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementPresent('@FooterParaLink1',data.LoginWait)
                    .waitForElementPresent('@FooterParaLink2',data.LoginWait);
                browser.getText("p[footer-disclaimer-links='0']", function(result) {
                    this.assert.equal(typeof result, "object");
                    this.assert.equal(result.status, 0);
                    this.assert.equal(result.value, data.FooterPara1);
                });

                browser.getText("p[footer-disclaimer-links='1']", function(result) {
                    this.assert.equal(typeof result, "object");
                    this.assert.equal(result.status, 0);
                    this.assert.equal(result.value, data.FooterPara2);
                });
            });
        }
    });

    this.Then(/^User should able to verify terms and Condition and Privacy Policy link$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementPresent('@PrivacyPolicyLink',data.LoginWait)
                    .waitForElementPresent('@TermsAndConditionLink',data.LoginWait)
                    .click('@PrivacyPolicyLink')
            });
        }
    });

    this.When(/^User clicks on Get Started Link and login with "([^"]*)" "([^"]*)" and "([^"]*)"$/, function (First_Name,Last_Name,SSN) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                LandingPageGI.waitForElementVisible('@GetStartedLoginGatedPortal',data.LoginWait)
                    .click('@GetStartedLoginGatedPortal')
                    .waitForElementVisible('@FirstnameTextbox',data.LoginWait)
                    .waitForElementVisible('@LastnameTextbox',data.LoginWait)
                    .waitForElementVisible('@SSNTextbox',data.LoginWait)
                    .setValue('@FirstnameTextbox',First_Name)
                    .setValue('@LastnameTextbox',Last_Name)
                    .setValue('@SSNTextbox',SSN)
                    .waitForElementVisible('@LoginContinueButton',data.LoginWait)
                    .click('@LoginContinueButton')
            });
        }
    });

    this.When(/^User able to login Successfully in Gated portal and Shop for Header Insurance Page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@HealthInsHeader',data.LoginWait);
            });
        }
    });

    this.When(/^User able to Complete the Off Exchange flow for gated portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')
                    .waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink')
                    .waitForElementVisible('@CompareYourOptionTitle',data.LoginWait)
                    .waitForElementVisible('@ShopWithEmpContButton',data.LoginWait)
                    .click('@ShopWithEmpContButton');

                //Verify Off-Exchange Url
                browser.pause(data.shortWait);
                browser.assert.urlContains('exchangeType=OFF');




            });
        }
    });

    this.When(/^User able to complete the On Exchange flow for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')
                    .waitForElementVisible('@SeeYourOptionsLink',data.LoginWait)
                    .click('@SeeYourOptionsLink')
                    .waitForElementVisible('@CompareYourOptionTitle',data.LoginWait)
                    .waitForElementVisible('@ShopWithTaxCreditsButton',data.LoginWait)
                    .click('@ShopWithTaxCreditsButton');

                //Verify Off-Exchange Url
                browser.pause(data.shortWait);
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                browser.assert.urlContains('exchangeType=ON');

                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });
                ShopForHealthInsurancePage.waitForElementVisible('@CompareYourOptionTitle',data.LoginWait);
            });
        }
    });

    this.When(/^User able to login Successfully in Non-Gated portal and Shop for Header Insurance Page should be displayed$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait)
                    .click('@StartShoppingLoginUngatedPortal');
            });
        }
    });

    this.Then(/^User able to Complete the Off Exchange flow for Non-Gated portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                ShopForHealthInsurancePage.waitForElementVisible('@HealthInsHeader',data.LoginWait)
                    .waitForElementVisible('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')

                    //change Zipcode
                    .waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)

                    //change Month
                    .waitForElementVisible('@MonthTextbox',data.LoginWait)
                    .clearValue('@MonthTextbox')
                    .setValue('@MonthTextbox',data.Month)

                    //change Day
                    .waitForElementVisible('@DaysTextbox',data.LoginWait)
                    .clearValue('@DaysTextbox')
                    .setValue('@DaysTextbox',data.Day)

                    //change Year
                    .waitForElementVisible('@YearTextbox',data.LoginWait)
                    .clearValue('@YearTextbox')
                    .setValue('@YearTextbox',data.Year)

                    .waitForElementVisible('@SeePlansLink',data.LoginWait)
                    .click('@SeePlansLink')

                browser.pause(data.shortWait)
                browser.assert.urlContains('exchangeType=OFF');
            });
        }
    });

    this.Then(/^User able to Complete the On Exchange flow for Non-Gated portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                ShopForHealthInsurancePage.waitForElementVisible('@HealthInsHeader',data.LoginWait)
                    .waitForElementVisible('@LostCoverageLink',data.LoginWait)
                    .click('@LostCoverageLink')

                    //change Zipcode
                    .waitForElementVisible('@ZipcodeTextbox',data.LoginWait)
                    .clearValue('@ZipcodeTextbox')
                    .setValue('@ZipcodeTextbox',data.Zipcode)

                    //change Month
                    .waitForElementVisible('@MonthTextbox',data.LoginWait)
                    .clearValue('@MonthTextbox')
                    .setValue('@MonthTextbox',data.Month)

                    //change Day
                    .waitForElementVisible('@DaysTextbox',data.LoginWait)
                    .clearValue('@DaysTextbox')
                    .setValue('@DaysTextbox',data.Day)

                    //change Year
                    .waitForElementVisible('@YearTextbox',data.LoginWait)
                    .clearValue('@YearTextbox')
                    .setValue('@YearTextbox',data.Year)

                    .waitForElementVisible('@CheckForTaxCreditsButton',data.LoginWait)
                    .click('@CheckForTaxCreditsButton')

                    .waitForElementVisible('@HouseholdIncomeTextbox',data.LoginWait)
                    .clearValue('@HouseholdIncomeTextbox')
                    .setValue('@HouseholdIncomeTextbox',data.HouseholdIncomeValue)

                    .waitForElementVisible('@CheckForTaxCreditsButtonNonGated',data.LoginWait)
                    .click('@CheckForTaxCreditsButtonNonGated')

                    .waitForElementVisible('@ShopWithTaxCreditsButton',data.LoginWait)
                    .click('@ShopWithTaxCreditsButton')

                browser.pause(data.shortWait)
                browser.assert.urlContains('exchangeType=ON');
            });
        }
    });

    this.When(/^User able to click and verify Health Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@HealthProductLink',data.LoginWait)
                    .click('@HealthProductLink');
                LandingPageGI.waitForElementVisible('@GetStartedButton',data.LoginWait);
            });
        }
    });

    this.When(/^User able to click and verify Medicare Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@MedicareProductLink',data.LoginWait)
                    .click('@MedicareProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('https://mercermarketplace.destinationrx.com/PlanCompare/Consumer/type1/2017/Compare/PinHome');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Dental Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@DentalProductLink',data.LoginWait)
                    .click('@DentalProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('dental');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Vision Product Link for Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@VisionProductLink',data.LoginWait)
                    .click('@VisionProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('vision');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to verify Other Insurance Product link in Motorola Clients "([^"]*)" for Gated Portal$/, function (URL_Moto) {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {

                if(URL_Moto.includes("Motorola"))
                {
                    ShopForHealthInsurancePage.waitForElementPresent('@OtherInsuranceLink',data.LoginWait)
                        .click('@OtherInsuranceLink')

                    browser.pause(data.shortWait)
                    browser.windowHandles(function(result) {
                        var handle = result.value[1];
                        browser.switchWindow(handle);
                        browser.maximizeWindow();
                        browser.pause(data.shortWait)
                    });

                    browser.assert.urlContains('Other-Insurance');
                    browser.closeWindow();

                    browser.pause(data.shortWait)
                    browser.windowHandles(function(result) {
                        var handle = result.value[0];
                        browser.switchWindow(handle);
                        browser.maximizeWindow();
                        browser.pause(data.shortWait)
                    });
                }

            });
        }
    });

    this.When(/^User able to click and verify Health Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@HealthProductLink',data.LoginWait)
                    .click('@HealthProductLink');
                LandingPageGI.waitForElementVisible('@StartShoppingLoginUngatedPortal',data.LoginWait);
            });
        }
    });

    this.When(/^User able to click and verify Medicare Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@MedicareProductLink',data.LoginWait)
                    .click('@MedicareProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('https://mercermarketplace.destinationrx.com/PlanCompare/Consumer/type1/2017/Compare/PinHome');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Dental Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@DentalProductLink',data.LoginWait)
                    .click('@DentalProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('dental');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });

    this.When(/^User able to click and verify Vision Product Link for Non-Gated Portal$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        // console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "PROD") {
            initializePageObjects(browser, function () {
                ShopForHealthInsurancePage.waitForElementPresent('@VisionProductLink',data.LoginWait)
                    .click('@VisionProductLink')

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[1];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

                browser.assert.urlContains('vision');
                browser.closeWindow();

                browser.pause(data.shortWait)
                browser.windowHandles(function(result) {
                    var handle = result.value[0];
                    browser.switchWindow(handle);
                    browser.maximizeWindow();
                    browser.pause(data.shortWait)
                });

            });
        }
    });






}

