var data = require('../../TestResources/Afweb_TestData.js');
var Objects = require(__dirname + '/../../repository/Afweb_locators.js');
//var robot = require('robot-js');
var clipboard = require('copy-paste');

var page, browser, homePageAfweb;

function initializePageObjects(client, callback) {
    browser = client;

    var locatorsAfweb = browser.page.Afweb_locators();

    page = locatorsAfweb.section;

    homePageAfweb = locatorsAfweb.section.HomePage;
    callback();
}

module.exports = function(){

    this.Given(/^User Navigate to AfWeb Home Page$/, function (browser) {
        var URL;
        initializePageObjects(browser, function () {
            if(data.TestingEnvironment == "QAI")
                URL = data.urlQAI;
            else if(data.TestingEnvironment == "CSO")
                URL = data.urlCSO;
            else if(data.TestingEnvironment == "CITEST")
                URL = data.urlCITEST;
            else if(data.TestingEnvironment == "CIDEV")
                URL = data.urlCIDEV;
            else{
                URL = data.urlPROD;
            }
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.pause(data.shortwaittime);
        });
    });

    this.Given(/^Link navigation validation - "([^"]*)" "([^"]*)"$/, function (link, landingPage) {
        browser = this;
        performClick(link, function () {
            isDisplayed(landingPage, function () {
                browser.back();
            });
        });
    });

    this.When(/^User Clicks on "([^"]*)" Link or Button$/, function(browser,locator) {
        performClick(locator, function () {
            browser.pause(data.shortpause);
        });
    });

    this.Then(/^"([^"]*)" page or message should be displayed$/, function(browser,locator){
        isDisplayed(locator, function () {
                        browser.pause(data.shortpause)
        });
    });

    this.When(/^Enter "([^"]*)" field with "([^"]*)" value$/, function (locator, value) {
       setText(locator, value)
    });

    this.When(/^Select "([^"]*)" in "([^"]*)" dropdown$/, function (browser,value, locator) {
        //selectDropdown(locator, value, function (callback) {})
        selectWithVisibleText(locator, value, function (selected) {
            browser.pause(data.shortpause);
        });
    });

    this.When(/^Randomly select in "([^"]*)" dropdown$/, function (locator, callback) {
        selectRandomly(locator, function (selectedValue) {
            callback(selectedValue);
        });
    });

    this.When(/^Select "([^"]*)" in "([^"]*)" dropdown as user$/, function (browser,value, locator) {
        selectDropdownUI(locator, value);
    });

    this.Then(/^"([^"]*)" alert should be displayed$/, function(txt){
        verifyAlert(txt, this);
    });

    this.Then(/^"([^"]*)" page or message should not be displayed$/, function (locator) {
        isNotDisplayed(locator, function () {
            browser.pause(data.shortpause)
        });
    });

    this.Then(/^"([^"]*)" page or message should not be present$/, function (locator) {
        isNotPresent(locator, function () {
            browser.pause(data.shortpause)
        });
    });

    this.Then(/^"([^"]*)" field should be in disabled state$/, function (locator) {
        isDisabled(locator, function () {
            browser.pause(data.shortpause)
        });
    });

    this.Given(/^Once the user navigate to Money page$/, function () {
        browser = this;
        performClick("HomePage|linkMenu1", function () {
            performClick("HomePage|linkMoneyMenuSelect", function () {
                isDisplayed("PensionPage|labelPensionSandboxPage", function () {
                });
            });
        });
    });

    this.Given(/^Once the user navigate to Profile page$/, function () {
        browser = this;
        var objLocation = Objects.sections.ProfilePage.elements.userProfileDDMenu.selector;
        var locateStrategy = Objects.sections.ProfilePage.elements.userProfileDDMenu.locateStrategy;

        if (locateStrategy == 'xpath') {
            browser.useXpath().moveToElement(objLocation, 0, 0);
        } else {
            browser.useCss().moveToElement(objLocation, 0, 0);
        }
        browser.mouseButtonDown(0);
        performClick("ProfilePage|userProfileLink", function () {
            isDisplayed("ProfilePage|labelProfilePageAboutMe", function () {
            });
        });
        browser.mouseButtonUp(0);

    });

    this.Given(/^Enter in "([^"]*)" from data sheet "([^"]*)"$/, function (locator, dataObject) {
        var arr = dataObject.split("|");
        var obj = arr[0];
        var field = arr[1];
        if ((data.TestingEnvironment).toUpperCase() == "QA") {
            var value = data.usersQA_V2[obj][field];
        } else {
            var value = data.usersUAT_V2[obj][field];
        }
        console.log(value);
        setText(locator, value);
    });

    this.Given(/^Verify "([^"]*)" field displays "([^"]*)" value$/, function (locator, expectedValue) {
        var arr = locator.split("|");
        if (arr[1].includes("input")) {
            verifyInputBoxText(locator, expectedValue);
        } else {
            verifyText(locator, expectedValue);
        }
    });

};

/*   ------------ (Reusable) Commonly Used Functions Below------------- */

var scroll = function (locator, callback) {
    var pageAndObject = locator.split("|");
    var pg = pageAndObject[0];
    var obj = '@'+pageAndObject[1];

    page[pg].getLocation(obj, function (position) {
        browser.execute(function (x, y) {
            window.scrollTo(x - 300, y - 300);
            return true
        }, [position.value.x, position.value.y]);
        callback();
    });
};

var verifyAlert = function (txt, browser) {
    browser.getAlertText(function (alertText) {
        browser.assert.equal(alertText.value, txt)
    });
    browser.acceptAlert();
};

var isDisplayed = function (locator, callback) {
    var pageAndObject = locator.split("|");
    var pg = pageAndObject[0];
    var object = '@'+pageAndObject[1];
    page[pg].waitForElementVisible(object, data.shortwaittime, function () {
        callback();
    });
};

var isNotDisplayed = function (locator, callback) {
    browser.pause(data.averagepause);
    var pageAndObject = locator.split("|");
    var mpage = pageAndObject[0];
    var object = '@' + pageAndObject[1];
    page[mpage].waitForElementNotVisible(object, data.averagepause, function () {
        callback()
    })
};

var elementDisplayedStatus = function (locator, callback) {
    var pageObject = locator.split("|");
    var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
    var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
    var status;

    if (locateStrategy == 'xpath') locateStrategy = 'xpath';
    else locateStrategy = 'css selector';

    browser.element(locateStrategy, object, function (obj) {
        browser.elementIdDisplayed(obj.value.ELEMENT, function (object_displayed_status) {
            if (object_displayed_status.state == 'success') status = true;
            else status = false;
            callback(status);
        });
    });
};

var isNotPresent = function (locator, callback) {
    browser.pause(data.averagepause);
    var pageAndObject = locator.split("|");
    var mpage = pageAndObject[0];
    var object = '@' + pageAndObject[1];
    page[mpage].waitForElementNotPresent(object, data.averagepause, function () {
        callback()
    })
};

var getText = function (locator, callback) {
    if (locator.includes("|")) {
        var pageAndObject = locator.split("|");
        var mpage = pageAndObject[0];
        var object = '@' + pageAndObject[1];
        page[mpage].getText(object, function (result) {
            return callback(result.value)
        });
    } else {
        browser.useXpath().getText(locator, function (result) {
            return callback(result.value)
        });
    }
};

var setText = function (locator, content) {
    var pageAndObject = locator.split("|");
    var mpage = pageAndObject[0];
    var object = '@' + pageAndObject[1];
    var value = content.toString();
    page[mpage].clearValue(object);
    page[mpage].setValue(object, value);
};

var fnRandomEmailId = function (callback) {
    var max = 9, min = 0, str="", i;
    for(i=0; i < 5; i++){
        str = str + Math.floor(Math.random() * (max - min) + min);
    }
    return callback('email'+ str +'@mercer.com');
};

var fnRandomPhoneNumber = function (callback) {
    var max = 9, min = 0, str="", i;
    for(i=0; i < 10; i++){
        str = str + Math.floor(Math.random() * (max - min) + min);
    }
    return callback(str);
};

var wait_a_Second = function (callback) {
    browser.pause(data.averagepause, function () {
        callback();
    })
};

var wait_a_bit_long = function (callback) {
    browser.pause(data.shortwaittime, function () {
        callback();
    })
};

var verifyText = function (locator, value) {
    var pageAndObject = locator.split("|");
    var mpage = pageAndObject[0];
    var object = '@' + pageAndObject[1];
    isDisplayed(locator, function () {
        page[mpage].getText(object, function (response) {
            console.log("Excepted: " + value);
            console.log("Actual: " + response.value);
            browser.assert.equal(response.value, value)
            if (response.value != value) {
                console.log("Text Content mismatch: ");
            }
        });
    });
};

var verifyInputBoxText = function (locator, value) {
    getInputBoxText(locator, function (inputBoxText) {
        console.log("Excepted: " + inputBoxText.toUpperCase());
        console.log("Actual: " + value.toUpperCase());
        browser.assert.equal(inputBoxText.toUpperCase(), value.toUpperCase())
        if (inputBoxText.toUpperCase() != value.toUpperCase()) {
            console.log("Text Content mismatch: ");
        }
    });
};

var verifyValuesEqual = function (msg, value1, value2) {
    console.log(msg);
    console.log('Expected: ' + value1);
    console.log('  Actual: ' + value2);
    browser.verify.equal(value1, value2);
};

var performClick = function (locator, callback) {
    var pageAndObject = locator.split("|");
    var mpage = pageAndObject[0];
    var object = '@'+pageAndObject[1];
    scroll(locator, function () {
        page[mpage].click(object);
        browser.pause(data.shortpause);
        callback();
    });
};

var fnRandomInteger = function (min, max, callback) {
    var random = min + Math.floor(Math.random() * (max - min + 1));
    return callback(random);
};

var selectWithVisibleText = function (locator, value, callback) {
    var pageObject = locator.split("|");
    var objLocation = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
    var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
    var options_location, count;

    if (locateStrategy == 'xpath') {
        options_location = objLocation + '//option';
    } else {
        locateStrategy = 'css selector';
        options_location = objLocation + ' > option';
    }

    page[[pageObject[0]]].click('@' + pageObject[1]);

    browser.elements(locateStrategy, options_location, function (webElementsArray) {
        count = webElementsArray.value.length;
        webElementsArray.value.forEach(function (webEle) {
            browser.elementIdText(webEle.ELEMENT, function (ele) {
                if (ele.value == value) {
                    browser.elementIdClick(webEle.ELEMENT)
                    browser.pause(data.averagepause);
                    page[[pageObject[0]]].click('@' + pageObject[1]);
                    callback();
                }
            })
        });
    });

};

var selectRandomly = function (locator, callback) {
    var pageObject = locator.split("|");
    var objLocation = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
    var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
    var options_location, count, i = 0;

    if (locateStrategy == 'xpath') {
        options_location = objLocation + '//option';
    } else {
        locateStrategy = 'css selector';
        options_location = objLocation + ' > option';
    }

    getInputBoxText(locator, function (selected_value) {
        page[[pageObject[0]]].click('@' + pageObject[1]);
        browser.elements(locateStrategy, options_location, function (webElementsArray) {
            count = parseInt(webElementsArray.value.length) - 1;
            fnRandomInteger(0, count, function (index) {
                webElementsArray.value.forEach(function (webEle) {
                    browser.elementIdText(webEle.ELEMENT, function (ele) {
                        if (i == parseInt(index)) {
                            if (ele.value == selected_value) {
                                selectRandomly(locator, function (data) {
                                    page[[pageObject[0]]].click('@' + pageObject[1]);
                                    callback(data);
                                });
                            } else {
                                browser.elementIdClick(webEle.ELEMENT);
                                browser.pause(data.shortpause);
                                page[[pageObject[0]]].click('@' + pageObject[1]);
                                callback(ele.value);
                            }
                        }
                        i++;
                    })
                });
            });
        });
    });
};

var selectDropdownUI = function(locator, value, callback) {
    var pageObject = locator.split("|");
    var mpage = pageObject[0];
    var object = '@' + pageObject[1];

    var options = Objects.sections[mpage].elements[pageObject[1]].selector;
    options = options + '//option[starts-with(text(),"' + value + '")]';
    page[mpage].click(object);
    browser.pause(data.shortpause);
    getText(options, function (selectedOptions) {
        browser.useXpath().waitForElementVisible(options, data.averagepause);
        browser.useXpath().click(options);
        browser.pause(data.shortpause);
        browser.useXpath().click(options);
        browser.pause(data.shortpause);
        browser.keys(browser.Keys.ENTER);
        browser.pause(data.shortpause);
        //callback(selectedOptions)
    });
};

var selectDropdown = function (locator, value) {
    var pageObject = locator.split("|");
    var identifier = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
    browser.execute(function (identifier, value) {
        var objSelect = document.getElementById(identifier);
        setSelectedValue(objSelect, value);
        function setSelectedValue(selectObj, valueToSet) {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].text== valueToSet) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }
    }, [identifier, value], function (result) {});
};

var readListData = function (locator, callback) {
    var data = "", count = 0;
    var pageObject = locator.split("|");
    var mpage = pageObject[0];
    var object = pageObject[1];
    browser.elements('xpath', Objects.sections[mpage].elements[object].selector, function (webElementsArray) {
        count = webElementsArray.value.length;
        webElementsArray.value.forEach(function (webEle) {
            browser.elementIdText(webEle.ELEMENT, function (result) {
                count--;
                data = data + '-' + result.value
                if (count <= 0) {
                    data = data.substring(1, data.length);
                    callback(data);
                }
            });
        });
    });
};

var getWebElementsCount = function (locator, callback) {
    var count = 0;
    if (locator.includes("|")) {
        var pageObject = locator.split("|");
        var mpage = pageObject[0];
        var object = pageObject[1];
        browser.elements('xpath', Objects.sections[mpage].elements[object].selector, function (webElementsArray) {
            count = webElementsArray.value.length;
            callback(count);
        });
    } else {
        browser.elements('xpath', locator, function (webElementsArray) {
            count = webElementsArray.value.length;
            callback(count);
        });
    }

};

var readListDataOnlyDisplayedValues = function (locator, callback) {
    var data = "", count = 0;
    browser.elements('xpath', locator, function (webElementsArray) {
        count = webElementsArray.value.length;
        webElementsArray.value.forEach(function (webEle) {
            browser.elementIdText(webEle.ELEMENT, function (result) {
                count--;
                if (result.value != '' && result.value != undefined)
                    data = data + '-' + result.value
                if (count <= 0) {
                    data = data.substring(1, data.length);
                    callback(data);
                }
            });
        });
    });
};

var dragAndDropOverAnotherElement = function (webElement1, webElement2, callback) {
    var pageObject = webElement1.split("|");
    var mpage = pageObject[0];
    var Object1 = Objects.sections[mpage].elements[pageObject[1]].selector;

    var pageObject = webElement2.split("|");
    var mpage = pageObject[0];
    var Object2 = Objects.sections[mpage].elements[pageObject[1]].selector;

    browser.useXpath()
    browser.moveToElement(Object1, 0, 0)
    browser.mouseButtonDown(0)
    browser.moveToElement(Object2, 0, 0)
    browser.mouseButtonUp(0)
    browser.pause(data.shortpause);
    callback();
};

var getInputBoxText = function (locator, callback) {
    var pageAndObject = locator.split("|")
    var mpage = pageAndObject[0]
    var object = '@' + pageAndObject[1]
    page[mpage].getValue(object, function (result) {
        callback(result.value);
    })
};

var FocusOnFileNameInput = function (callback) {
    var Keyboard = robot.Keyboard;
    var k = Keyboard();
    var tabIndex = [];
    var times = 4;
    for (var i = 0; i < times; i++) {
        tabIndex.push(i + 1);
    }
    browser.pause(data.shortpause);
    k.click(robot.KEY_F3);
    k.click(robot.KEY_ENTER);
    browser.pause(data.shortpause);
    tabIndex.forEach(function (index) {
        setTimeout(function () {
            k.click(robot.KEY_TAB);
            browser.pause(data.shortpause);
            if (index == times) {
                k.click(robot.KEY_ENTER);
                browser.pause(data.averagepause);
                callback();
            }
        }, 2000);

    });
};

var pastePath = function (callback) {
    var Keyboard = robot.Keyboard;
    var k = Keyboard();
    k.press(robot.KEY_CONTROL);
    browser.pause(data.shortpause);
    k.click(robot.KEY_V);
    browser.pause(data.shortpause);
    k.release(robot.KEY_CONTROL);
    browser.pause(data.shortpause);
    k.click(robot.KEY_ENTER);
    browser.pause(data.shortpause);
    callback();
};

var NavigateToFolderLocation = function (callback) {
    browser.pause(data.shortpause);
    FocusOnTopFolderNavigator(function () {
        browser.pause(data.shortpause);
        pastePath(function () {
            callback();
        })
    })
};

var FocusOnTopFolderNavigator = function (callback) {
    var Keyboard = robot.Keyboard;
    var k = Keyboard();
    setTimeout(function () {
        k.click(robot.KEY_F4);
        browser.pause(data.shortpause);
        k.press(robot.KEY_CONTROL);
        browser.pause(data.shortpause);
        k.click(robot.KEY_A);
        browser.pause(data.shortpause);
        k.release(robot.KEY_CONTROL);
        browser.pause(data.shortpause);
        k.click(robot.KEY_BACKSPACE);
        browser.pause(data.shortpause);
        callback();
    }, 4000);
};

var TypeFileName = function (filename, callback) {
    var Keyboard = robot.Keyboard;
    var k = Keyboard();
    var keystoke = [];
    FocusOnFileNameInput(function () {
        browser.pause(data.shortpause);
        for (var i = 0, len = filename.length; i < len; i++) {
            if (filename[i] == '_') {
                keystoke.push("KEY_SLASH");
            } else if (filename[i] == '-') {
                keystoke.push("KEY_MINUS");
            } else {
                keystoke.push("KEY_" + filename[i].toUpperCase());
            }
        }
        var count = keystoke.length;
        keystoke.forEach(function (key) {
            k.click(robot[key]);
            count = count - 1;
            if (count < 1) {
                k.click(robot.KEY_ENTER);
                callback();
            }
        });
    })
};

var UploadFile = function (file, callback) {
    file = file.replace('\\', '/');
    var slashIndex = file.lastIndexOf('/');
    var path = file.substring(0, slashIndex);
    var filename = file.substring(parseInt(slashIndex) + 1, file.length);
    clipboard.copy(path, function () {
        browser.pause(data.shortpause);
        NavigateToFolderLocation(function () {
            browser.pause(data.shortpause);
            TypeFileName(filename, function () {
                browser.pause(data.shortpause);
                callback('File uploading Completed!!');
            });
        });
    });
};

var fnRandomDate = function (callback) {
    fnRandomInteger(1, 30, function (day) {
        fnRandomInteger(3, 12, function (month) {
            fnRandomInteger(1999, 2014, function (year) {
                if (parseInt(day) < 10) {
                    day = '0' + day;
                }
                if (parseInt(month) < 10) {
                    month = '0' + month;
                }
                var date = day + "/" + month + "/" + year;
                callback(date);
            })
        })
    })
};

var splitAndSort = function (input, callback) {
    var arr = input.split("-");
    arr = arr.sort();
    var output = "", count = 0;
    arr.forEach(function (each) {
        output = output + "-" + each;
        count++;
        if (count == arr.length) {
            output = output.replace("--", "-")
            output = output.substring(1, output.length);
            callback(output);
        }
    });
};

var getCheckboxState = function (locator, callback) {
    var pageObject = locator.split("|");
    var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
    var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
    var status;

    if (locateStrategy == 'xpath') locateStrategy = 'xpath';
    else locateStrategy = 'css selector';

    browser.element(locateStrategy, object, function (obj) {
        browser.elementIdSelected(obj.value.ELEMENT, function (object_selected_status) {
            callback(object_selected_status.value);
        });
    });
};

var isElementEnabled = function (locator, callback) {
    var pageObject = locator.split("|");
    var locateStrategy = Objects.sections[pageObject[0]].elements[pageObject[1]].locateStrategy;
    var object = Objects.sections[pageObject[0]].elements[pageObject[1]].selector;
    var status;

    if (locateStrategy == 'xpath') locateStrategy = 'xpath';
    else locateStrategy = 'css selector';

    browser.element(locateStrategy, object, function (obj) {
        browser.elementIdEnabled(obj.value.ELEMENT, function (object_selected_status) {
            callback(object_selected_status.value);
        });
    });
};

var verifyTextNotEqual = function (locator, value) {
    var pageAndObject = locator.split("|");
    var mpage = pageAndObject[0];
    var object = '@' + pageAndObject[1];
    isDisplayed(locator, function () {
        page[mpage].getText(object, function (response) {
            console.log("Excepted: " + value);
            console.log("Actual: " + response.value);
            browser.assert.notEqual(response.value, value)
            if (response.value = value) {
                console.log("Text Content match: ");
            }
        });
    });
};

var getTodaysDate = function (format, callback) {
    var day = new Date().getDate();
    var month = new Date().getMonth();
    var year = new Date().getFullYear();
    var monthName;

    switch (parseInt(month) + 1) {
        case 1:
            monthName = "January";
            break;
        case 2:
            monthName = "Febuary";
            break;
        case 3:
            monthName = "March";
            break;
        case 4:
            monthName = "April";
            break;
        case 5:
            monthName = "May";
            break;
        case 6:
            monthName = "June";
            break;
        case 7:
            monthName = "July";
            break;
        case 8:
            monthName = "August";
            break;
        case 9:
            monthName = "September";
            break;
        case 10:
            monthName = "October";
            break;
        case 11:
            monthName = "November";
            break;
        case 12:
            monthName = "December";
            break;
    }
    if (format == 'DD/MM/YYYY') {
        month = (parseInt(month) + 1);
        if (parseInt(day) < 10) day = '0' + parseInt(day);
        if (parseInt(month) < 10) month = '0' + parseInt(month);
        callback(day + '/' + month + '/' + year);
    }
    else if(format == 'MM DD, YYYY'){
        if (parseInt(day) < 10) day = '0' + parseInt(day);
        callback(monthName.substring(0, 3) + ' ' + day + ', ' + year);
    }else {
        callback(day + ' ' + monthName + ', ' + year);
    }
};

var getLoggedUserEmail = function (callback) {
    callback(user_email.toLowerCase());
};
var AcceptingPopup = function(){
    var Keyboard = robot.Keyboard;
    var k = Keyboard();
    //browser.pause(data.shortpause);
    k.click(robot.KEY_ENTER);
    //browser.pause(data.shortpause);
};
var RejectingPopup = function(){
    var Keyboard = robot.Keyboard;
    var k = Keyboard();
    k.click(robot.KEY_TAB);
    k.click(robot.KEY_ENTER);
};

var waitTillElementLoads = function (locator,callback) {
    elementDisplayedStatus(locator, function (displayStatus) {
        if(displayStatus){
            callback();
        }else{
            waitTillElementLoads(locator, callback);
        }
    })
};


module.exports.getCheckboxState = getCheckboxState;
module.exports.isElementEnabled = isElementEnabled;
module.exports.verifyValuesEqual = verifyValuesEqual;
module.exports.splitAndSort = splitAndSort;
module.exports.fnRandomDate = fnRandomDate;
module.exports.performClick = performClick;
module.exports.getText = getText;
module.exports.setText = setText;
module.exports.isDisplayed = isDisplayed;
module.exports.isNotDisplayed = isNotDisplayed;
module.exports.verifyText = verifyText;
module.exports.wait_a_Second = wait_a_Second;
module.exports.wait_a_bit_long = wait_a_bit_long;
module.exports.fnRandomPhoneNumber = fnRandomPhoneNumber;
module.exports.fnRandomEmailId = fnRandomEmailId;
module.exports.verifyAlert = verifyAlert;
module.exports.fnRandomInteger = fnRandomInteger;
module.exports.readListData = readListData;
module.exports.dragAndDropOverAnotherElement = dragAndDropOverAnotherElement;
module.exports.getInputBoxText = getInputBoxText;
module.exports.readListDataOnlyDisplayedValues = readListDataOnlyDisplayedValues;
module.exports.UploadFile = UploadFile;
module.exports.getWebElementsCount = getWebElementsCount;
module.exports.verifyInputBoxText = verifyInputBoxText;
module.exports.isNotPresent = isNotPresent;
module.exports.elementDisplayedStatus = elementDisplayedStatus;
module.exports.selectWithVisibleText = selectWithVisibleText;
module.exports.selectRandomly = selectRandomly;
module.exports.selectDropdownUI = selectDropdownUI;
module.exports.selectDropdown = selectDropdown;
module.exports.getLoggedUserEmail = getLoggedUserEmail;
module.exports.verifyTextNotEqual = verifyTextNotEqual;
module.exports.getTodaysDate = getTodaysDate;
module.exports.waitTillElementLoads = waitTillElementLoads;
//module.exports.initializePageObjects = initializePageObjects;