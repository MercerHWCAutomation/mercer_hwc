/**
 * Created by subhajit-chakraborty on 6/9/2017.
 */

var data = require('../../TestResources/HR_AdminPortalGlobalData.js');
var Objects = require(__dirname + '/../../repository/HRAdminPortalPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var LoginPageHRAP,DashboardPageHRAP,DocumentLibraryPageHRAP,EducationPageHRAP,EmployeePageHRAP,ReportingPageHRAP;

function initializePageObjects(browser, callback) {
    var BPPage = browser.page.HRAdminPortalPages();
    LoginPageHRAP = BPPage.section.LoginPageHRAP;
    DashboardPageHRAP = BPPage.section.DashboardPageHRAP;
    DocumentLibraryPageHRAP = BPPage.section.DocumentLibraryPageHRAP;
    EducationPageHRAP = BPPage.section.EducationPageHRAP;
    EmployeePageHRAP = BPPage.section.EmployeePageHRAP;
    ReportingPageHRAP = BPPage.section.ReportingPageHRAP;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the browser and launch the HRAdmin Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL = data.HRAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageHRAP.waitForElementVisible('@inputUsernameHRAP', data.longWait);
            });
        }
    });

    this.When(/^Enter valid username and password and click Login on HRAdmin Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            LoginPageHRAP.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername)
                .setValue('@inputPasswordHRAP',data.HRAdminPortalPassword)
                .click('@ContinueButtonLoginHRAP');
        }
    });

    this.When(/^HRAdmin Portal Dashboard should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DashboardPageHRAP.waitForElementVisible('@MyDashboardHeader', data.longWait)
                .waitForElementVisible('@ViewAllEmployeeButton', data.longWait)
                .waitForElementVisible('@EmployeeSnapshotTile', data.longWait)
                .waitForElementVisible('@ActivityStreamTile', data.longWait)
                .waitForElementVisible('@MessageCenterTile', data.longWait)
                .waitForElementVisible('@PayrollTile', data.longWait)
                .waitForElementVisible('@ReportingTile', data.longWait)
                .waitForElementVisible('@DashboardMenuFlyoverTab', data.longWait)
                .waitForElementVisible('@EmployeesMenuFlyover', data.longWait)
                .waitForElementVisible('@BenefitsMenuFlyover', data.longWait)
                .waitForElementVisible('@ReportingMenuFlyover', data.longWait)
                .waitForElementVisible('@PayrollMenuFlyover', data.longWait)
                .waitForElementVisible('@ResourcesMenuFlyover', data.longWait)
                .waitForElementVisible('@TestingMenuFlyover', data.longWait);
        }
    });

    this.Then(/^Dashboard Page should be in default view for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DashboardPageHRAP.waitForElementVisible('@DayToDayTab', data.longWait)
            // .waitForElementVisible('@OpenEnrollmentTab', data.longWait)
            // .waitForElementVisible('@EmployeeSnapshotTab', data.longWait);
        }
    });

    this.When(/^User able to open Document Library Page HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DashboardPageHRAP.waitForElementVisible('@DocumentLibraryQuicklinks',data.longWait)
                .click('@DocumentLibraryQuicklinks');
            DocumentLibraryPageHRAP.waitForElementVisible('@DocumentLibraryTitle',data.longWait)

        }
    });

    this.When(/^User able to view Tile and List view of document library for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DocumentLibraryPageHRAP.waitForElementVisible('@TileViewTab',data.longWait)
                .waitForElementVisible('@ListViewTab',data.longWait)
                .waitForElementVisible('@SolutionOverviewTab',data.longWait)
                .click('@SolutionOverviewTab')
                .waitForElementVisible('@DataManagementTab',data.longWait)
                .click('@DataManagementTab')
                .waitForElementVisible('@PayrollAndReportsTab',data.longWait)
                .click('@PayrollAndReportsTab')
                .waitForElementVisible('@SpendingAccountsTab',data.longWait)
                .click('@SpendingAccountsTab')
                .waitForElementVisible('@ProcessSupportTab',data.longWait)
                .click('@ProcessSupportTab')
                .waitForElementVisible('@COBRATab',data.longWait)
                .click('@COBRATab')


        }
    });

    this.Then(/^User able to perform download and sorting action on the files for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DocumentLibraryPageHRAP.waitForElementVisible('@AscendingTab',data.longWait)
                .waitForElementVisible('@DescendingTab',data.longWait)
                .click('@AscendingTab')
                .click('@DescendingTab')


        }
    });

    this.When(/^User able to open Education Page for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DashboardPageHRAP.waitForElementVisible('@EducationQuicklinks',data.longWait)
                .click('@EducationQuicklinks');

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });
        }
    });

    this.Then(/^Video should work and can move to next Video Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            EducationPageHRAP.waitForElementVisible('@CanvasButtonHRAP', data.longWait)
                .click('@CanvasButtonHRAP')
                .waitForElementVisible('@LearnMoreButton', data.longWait)
                .click('@LearnMoreButton')
                .waitForElementVisible('@VideoDashboardTab', data.longWait)
        }
    });

    this.Then(/^User able to open the Employee Page screen$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            DashboardPageHRAP.waitForElementVisible('@ViewAllEmployeeButton',data.longWait)
                .click('@ViewAllEmployeeButton')
                .waitForElementVisible('@AddNewEmployeeButton',data.longWait)
        }
    });

    this.Then(/^User able to navigate to proxy access for MBC Portal for Promoted Clients$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            EmployeePageHRAP.waitForElementVisible('@EmployeeTitleHeader',data.longWait)
                .waitForElementVisible('@FirstLName',data.longWait)
                .click('@FirstLName')
                .waitForElementVisible('@ProxyAccessLinkFirst',data.LoginWait)
                .click('@ProxyAccessLinkFirst')

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });

            EmployeePageHRAP.waitForElementVisible('@EndImpersonationButton',data.LoginWait)
                .click('@EndImpersonationButton')

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[0];
                browser.switchWindow(handle);
                browser.pause(data.LoginWait)
                //browser.maximizeWindow();
            });
        }
    });

    this.Then(/^User able to logout Successfully form HR Admin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            var xpath="//div[@class='user-pic small']";
            var xpath1="//a[contains(text(),'Log Out')]";
            browser.useXpath().moveToElement(xpath,0,1).pause(3000);
            browser.click(xpath1);
            browser.pause(5000);
        }
        LoginPageHRAP.waitForElementVisible('@inputUsernameHRAP',data.LoginWait)
    });

    this.When(/^User clicks on reporting tab and reporting Page is opened$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@ReportingMenuFlyover',data.LoginWait)
                .click('@ReportingMenuFlyover');
            ReportingPageHRAP.waitForElementVisible('@ReportingTitleHeader',data.LoginWait)
        }
    });

    this.Then(/^User able to create and download link is enabled for the report to download$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            ReportingPageHRAP.waitForElementVisible('@SelectReportTypeDropdown',data.LoginWait)
                .click('@SelectReportTypeDropdown')
                .waitForElementVisible('@AlllifeEventDropdown',data.LoginWait)
                .click('@AlllifeEventDropdown')
                .waitForElementVisible('@CreateReportButton',data.LoginWait)
                .click('@CreateReportButton')
                .waitForElementVisible('@IncludeEventDropdown',data.LoginWait)
                .click('@IncludeEventDropdown')
                .waitForElementVisible('@AllEventOptionIEDropdown',data.LoginWait)
                .click('@AllEventOptionIEDropdown')
                .waitForElementVisible('@EffectiveDateStartValue',data.LoginWait)
                .waitForElementVisible('@EffectiveDateEndValue',data.LoginWait)
                .setValue('@EffectiveDateStartValue',data.ReportStartDate)
                .setValue('@EffectiveDateEndValue',data.ReportEndDate)
                .waitForElementVisible('@GenerateReportButton',data.LoginWait)
                .click('@GenerateReportButton')
                .waitForElementVisible('@ReportSuccessMsg',data.LoginWait)
                .waitForElementVisible('@ReportDownloadButton',data.LoginWait)

        }
    });

    this.Then(/^New report is added to the row of all reports and reports can be downloaded$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            ReportingPageHRAP.waitForElementVisible('@CloseButtonReportOption',data.LoginWait)
                .click('@CloseButtonReportOption')
                .waitForElementVisible('@LatestCreatedReportValue',data.LoginWait)
                .click('@LatestCreatedReportValue')
            //.waitForElementNotVisible('@ErrorMsgReportDownload',data.LoginWait)


        }
    });



}
