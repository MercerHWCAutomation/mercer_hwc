var action = require('./Keywords.js');
var Objects = require(__dirname + '/../../repository/Afweb_locators.js');
//var robot = require('robot-js');
var data = require('../../TestResources/Afweb_TestData.js');
var browser;

module.exports = function () {
    this.Then(/^Verify that Home Page loads successfully$/, function (browser) {
        action.isDisplayed("HomePage|ParticipantsTab",function () {
            browser.saveScreenshot('./screenshots/AfwebHomePage.png')
            // browser.assert.urlContains("home")
            browser.url(function (current_url) {
                var pageurl = current_url.value
                var pageurl = pageurl.toUpperCase();
                if (pageurl.includes("HOME")) {
                    console.log("Home Page loaded successfully")
                }
                else if(pageurl.includes("ERROR")) {
                    console.log("Error loaded for Home Page")
                }
            })
        });
    });

    this.Then(/^Verify AdministrationFoundation title in home page$/, function (browser) {
        this.demoTest = function (browser) {
            browser.getTitle(function(title) {
                this.assert.equal(typeof title, 'string');
                this.assert.equal(title, 'Administration Foundation');
            });
        };
    });
    
    this.Then(/^Verify Page title in Client Search page$/, function (browser) {
        this.demoTest = function (browser) {
            browser.getTitle(function(title) {
                this.assert.equal(typeof title, 'string');
                this.assert.equal(title, 'Client Search');
            });
        };
    });

    this.Then(/^Verify Welcome Title in home Page$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("HomePage|WelcomeTitle",function () {
        });
    });

    this.Then(/^Verify Groups and Roles Section in home page$/, function (browser) {
        action.getText("HomePage|RolesandPermissiontext", function (txt) {
            var res = txt.split(":");
            var lastarrayvalue = res[res.length - 1];
            var n = txt.includes("You have been assigned the following roles");
            if (n) {
                // console.log("'You have been assigned the following roles' title is displayed")
            }
            var role1 = lastarrayvalue.includes(data.Role1)
            var role2 = lastarrayvalue.includes(data.Role2)
            if (role1 && role2) {
                // console.log("Roles and Permissions are displayed in home Page")
            }
        })

    });

    this.When(/^User Clicks on Clients Tab$/, function (browser) {
        action.performClick("HomePage|ClientsTab",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify that Clients Search Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        browser.saveScreenshot('./screenshots/ClientSearchPage.png')
        browser.assert.urlContains("ClientSearch")
    });

    this.When(/^User Enters Client Name and clicks on search button$/, function (browser) {
        action.setText("ClientsSearchPage|inputClientName",data.ClientName);
        if(data.TestingEnvironment == "PROD") {
            action.performClick("ClientsSearchPage|SearchbuttonPROD", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
        else {
            action.performClick("ClientsSearchPage|Searchbutton", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
    });

    this.Then(/^Verify Client Search Results Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.saveScreenshot('./screenshots/ClientSearchResultsPage.png')
        browser.assert.urlContains("ClientSearchResults")
    });

    this.Then(/^Verify Page title in Client Search Results page$/, function (browser) {
        this.demoTest = function (browser) {
            browser.getTitle(function(title) {
                this.assert.equal(typeof title, 'string');
                this.assert.equal(title, 'Client Search');
            });
        };
    });

    this.When(/^User Clicks on Client Name Link$/, function (browser) {
        action.performClick("ClientsResultsPage|Clientnamelink",function () {
        });
    });

    this.Then(/^Verify Client Summary Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        browser.saveScreenshot('./screenshots/ClientSummaryPage.png')
        browser.assert.urlContains("ClientSummary")
    });

    this.Then(/^Verify Page title in Client Summary page$/, function (browser) {
        this.demoTest = function (browser) {
            browser.getTitle(function(title) {
                this.assert.equal(typeof title, 'string');
                this.assert.equal(title, 'Client Summary');
            });
        };
    });
    this.Then(/^Verify Profile Summary Section Name in ClientSummary Page$/, function (browser) {
        browser.pause(data.shortpause);
        action.isDisplayed("ClientSummaryPage|ProfileSummaryTitle",function () {
        });
    });
    this.Then(/^Verify Client Name Title in Profile Summary Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|ClientNameTitleInSummaryPage",function () {
        });
    });
    this.Then(/^Verify Client Name Value in Profile Summary Section$/, function (browser) {
        action.getText("ClientSummaryPage|ClientNameValueInSummaryPage", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Client Value is empty")
            }
        });
    });
    this.Then(/^Verify One Code Title in Profile Summary Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|OneCodeTitleInSummaryPage",function () {
        });
    });
    this.Then(/^Verify One Code Value in Profile Summary Section$/, function (browser) {
        action.getText("ClientSummaryPage|OneCodeValueInSummaryPage", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("One Code Value is empty")
            }
        });
    });
    this.Then(/^Verify Address Title in Profile Summary Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|AddressTitleInSummaryPage",function () {
        });
    });
    this.Then(/^Verify Address Value in Profile Summary Section$/, function (browser) {
        action.getText("ClientSummaryPage|AddressValueInSummaryPage", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Address Value is empty")
            }
        });
    });

    this.Then(/^Verify LOB Section Name in ClientSummary Page$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|LOBTitle",function () {
        });
    });
    this.Then(/^Verify LoB Title in LOB Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|LOBColumnName",function () {
        });
    });
    this.Then(/^Verify ClientName Title in LOB Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|ClientorControlIdColumnName",function () {
        });
    });
    this.Then(/^Verify Application Title in LOB Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|ApplicationColumnName",function () {
        });
    });
    this.Then(/^Verify Application Enabled Title in LOB Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|ApplicationEnabledColumnName",function () {
        });
    });

    this.Then(/^Verify LOB Value in LOB Section$/, function (browser) {
        action.getText("ClientSummaryPage|LOBSecondrowFirstColumnValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("LoB Value is empty")
            }
        });
    });
    this.Then(/^Verify ClientIDorName Value in LOB Section$/, function (browser) {
        action.getText("ClientSummaryPage|ClientIdSecondrowSecondColumnValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("ClientIDorApplication Value is empty")
            }
        });
    });
    this.Then(/^Verify ApplicationName Value in LOB Section$/, function (browser) {
        action.getText("ClientSummaryPage|ApplicationSecondrowThirdColumnValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Application Value is empty")
            }
        });
    });
    this.Then(/^Verify ApplicationEnabled Value in LOB Section$/, function (browser) {
        action.getText("ClientSummaryPage|ApplicationSecondrowFourthColumnValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Application Enabled Status Value is empty")
            }
        });
    });

    this.Then(/^Verify Feautres Section Name in ClientSummary Page$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|FeaturesTitle",function () {
        });
    });
    this.Then(/^Verify Links in Feautres Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|AuthenticationLink",function () {
        });
        action.isDisplayed("ClientSummaryPage|SecondLink",function () {
        });
    });

    this.When(/^User Clicks on ViewDetails link in Features Section$/, function (browser) {
        action.performClick("ClientSummaryPage|ViewDetailslinkInSummaryPage",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });
    this.Then(/^Verify Client Servicing Options Summary Page Loads Successfully$/, function (browser) {
        browser.saveScreenshot('./screenshots/ClientServicingOptionsSummaryPage.png')
        browser.assert.urlContains("ClientServicingOptionsSummary")
    });
    this.Then(/^Verify Authentication Rules Title ClientSummary Page$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|ParticipantAuthenticTitle",function () {
        });
    });

    this.Then(/^Verify Webbsite Title in Authentication Rules Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|WebsiteTitle", function () {
        });
    });
    this.Then(/^Verify WebSite Value in Authentication Rules Section$/, function (browser) {
        action.getText("ClientSummaryPage|WebsiteValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Website Value is empty")
            }
        });
    });

    this.Then(/^Verify IVRValidation Title in Authentication Rules Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|IVRValidationTitle", function () {
        });
    });
    this.Then(/^Verify IVRValidation Value in Authentication Rules Section$/, function (browser) {
        action.getText("ClientSummaryPage|IVRValidationValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("IVR Validation Value is empty")
            }
        });
    });

    this.Then(/^Verify WebUsername Title in Authentication Rules Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|WebUsernameFormatTitle", function () {
        });
    });
    this.Then(/^Verify WebUsername Value in Authentication Rules Section$/, function (browser) {
        action.getText("ClientSummaryPage|WebUsernameFormatValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("WebUsername Value is empty")
            }
        });
    });

    this.Then(/^Verify WebPassword Title in Authentication Rules Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|WebPasswordFormatTitle", function () {
        });
    });
    this.Then(/^Verify WebPassword Value in Authentication Rules Section$/, function (browser) {
        action.getText("ClientSummaryPage|WebPasswordFormatValue", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("WebPassword Value is empty")
            }
        });
    });

    this.When(/^User Clicks on Participants Tab$/, function (browser) {
        action.performClick("HomePage|ParticipantsTab",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });
    this.Then(/^Verify Participant Search Page title$/, function (browser) {
        this.demoTest = function (browser) {
            browser.getTitle(function(title) {
                this.assert.equal(typeof title, 'string');
                this.assert.equal(title, 'View Participant Search Page');
            });
        };
    });
    this.Then(/^Verify that Participant Search Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ParticipantSearchPage|Titleparticipantsearch",function () {
        });
        browser.saveScreenshot('./screenshots/ParticipantSearchPage.png')
    });

    this.When(/^User Enters SSN in Social ID field and clicks on search button$/, function (browser) {
        action.setText("ParticipantSearchPage|inputsocialid",data.SSN);
        if(data.TestingEnvironment == "PROD") {
            action.performClick("ParticipantSearchPage|btnSearchbySocialIdPROD", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
        else {
            action.performClick("ParticipantSearchPage|btnSearchbySocialId", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
    });

    this.When(/^User Enters Alternate Username and clicks on search button$/, function (browser) {
        action.setText("ParticipantSearchPage|inputalternativeusername",data.AlternateUsername);
        action.performClick("ParticipantSearchPage|btnSearchbyAlternativeUsername",function () {
            browser.timeoutsImplicitWait(data.averagewaittime);
        });
    });

    this.Then(/^Verify that Participant Results Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ParticipantResultsPage|linkFirstParticipantCode",function () {
        });
        browser.saveScreenshot('./screenshots/ParticipantResultsPage.png')
        browser.assert.urlContains("ParticipantResults")
    });

    this.Then(/^Verify Search Criteria Title and Alternate Username Value in Participant Results Page$/, function (browser) {
        action.getText("ParticipantResultsPage|Titlesearchcriteria", function (txt) {
            var n = txt.includes("Alternate Username:"&" "&data.AlternateUsername);
            if (n) {
            }
            else {
                console.log("Search Criteria Title is not displayed")
            }
        });

    });

    this.Then(/^Verify Search Criteria Title and SSN Value in Participant Results Page$/, function (browser) {
        browser.pause(data.averagepause);
        action.getText("ParticipantResultsPage|SearchTitlebySocialID", function (txt) {
            var n = txt.includes("Social ID:"&" "&data.SSN);
            if (n) {
            }
            else {
                console.log("Search Criteria Title is not displayed")
            }
        });

    });

    this.When(/^User Clicks on Participant Code$/, function (browser) {

        if (data.ClientName == "QAWORK") {
            action.performClick("ParticipantResultsPage|linkParticipantCodeforQAWORK", function () {
                browser.pause(data.averagepause)
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
        else if (data.ClientName == "SLSDM")
        {
            action.performClick("ParticipantResultsPage|linkParticipantCodeforSLSDM", function () {
                browser.pause(data.averagepause)
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
        else {
            action.performClick("ParticipantResultsPage|linkParticipantCodeforanyotherclients", function () {
                browser.pause(data.averagepause)
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
    });
    this.Then(/^Verify that Participant Summary Page loads successfully$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|TitleParticipantProfile",function () {
        });
        browser.saveScreenshot('./screenshots/ParticipantSummaryPage.png')
        browser.assert.urlContains("ParticipantSummary")
    });

    this.Then(/^Verify Participant Profile Section Name in ParticipantSummary Page$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ParticipantSummaryPage|TitleParticipantProfile",function () {
        });
    });
    this.Then(/^Verify Name Title in ParticipantProfile Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|NameTitleinParticipantProfile",function () {
        });
    });
    this.Then(/^Verify Name Value in ParticipantProfile Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|NameValueInParticipantProfile", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Name Value is empty")
            }
        });
    });

    this.Then(/^Verify Client Title in ParticipantProfile Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|ClientTitleinParticipantProfile",function () {
        });
    });
    this.Then(/^Verify Client Value in ParticipantProfile Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|ClientValueInParticipantProfile", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Client Value is empty")
            }
        });
    });

    this.Then(/^Verify SSN Title in ParticipantProfile Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|SSNTitleinParticipantProfile",function () {
        });
    });
    this.Then(/^Verify SSN Value in ParticipantProfile Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|SSNValueInParticipantProfile", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("SSN Value is empty")
            }
        });
    });

    this.Then(/^Verify DOB Title in ParticipantProfile Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|DOBTitleinParticipantProfile",function () {
        });
    });
    this.Then(/^Verify DOB Value in ParticipantProfile Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|DOBValueInParticipantProfile", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("DOB Value is empty")
            }
        });
    });

    this.Then(/^Verify Address Title in ParticipantProfile Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|AddressTitleinParticipantProfile",function () {
        });
    });
    this.Then(/^Verify Address Value in ParticipantProfile Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|AddressValueInParticipantProfile", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Address Value is empty")
            }
        });
    });

    this.Then(/^Verify Participant Web Access Section Name in ParticipantSummary Page$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|TitleParticipantWebAccess",function () {
        });
    });

    this.Then(/^Verify Administration Section Name in ParticipantSummary Page$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|TitleAdministrationHistory",function () {
        });
    });


    this.Then(/^Verify Lob OR Application Title in Participant Web Access Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|LoBTitleinParticipantWebAccess",function () {
        });
    });
    this.Then(/^Verify Access Allowed Title in Participant Web Access Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|AccessAllowedTitleinParticipantWebAccess",function () {
        });
    });

    this.Then(/^Verify DefinedBenefits Title in Participant Web Access Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|DefinedBenefitsTitleinParticipantWebAccess",function () {
        });
    });
    this.Then(/^Verify DefinedBenefits Value in Participant Web Access Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|DefinedBenefitsValueinParticipantWebAccess", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("DefinedBenefits Value is empty")
            }
        });
    });

    this.Then(/^Verify HealthandBenefits Title in Participant Web Access Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|HealthandBenefitsTitleinParticipantWebAccess",function () {
        });
    });
    this.Then(/^Verify HealthandBenefits Value in Participant Web Access Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|HealthandBenefitsValueinParticipantWebAccess", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Health and Benefits Value is empty")
            }
        });
    });

    this.Then(/^Verify SingleSignOnValue Title in ParticipantProfile Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|SingleSignOnTitleinParticipantWebAccess",function () {
        });
    });
    this.Then(/^Verify SingleSignOnValue Value in ParticipantProfile Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|SingleSignOnValueinParticipantWebAccess", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Single Sign On Value is empty")
            }
        });
    });


    this.Then(/^Verify Web Login Management Section Name in ParticipantSummary Page$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|TitleWebLoginManagement",function () {
        });
    });
    this.Then(/^Verify Last Successfull login Title in WebLoginManagement Section$/, function (browser) {
        action.isDisplayed("ParticipantSummaryPage|LastsuccessfulloginTitleinLoginManagement",function () {
        });
    });
    this.Then(/^Verify Last Successfull login Value in WebLoginManagement Section$/, function (browser) {
        action.getText("ParticipantSummaryPage|LastsuccessfulloginValueinLoginManagement", function (txt) {
            if (txt !== "") {
            }
            else {
                console.log("Last Successfull login Value is empty")
            }
        });
    });

    this.When(/^User Clicks on Web Login Management link$/, function (browser) {
        action.performClick("WebLoginManagementPage|WebLoginManagementLink",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify WebLoginManagement Page loads successfully$/, function (browser) {
        browser.pause(data.shortwaittime);
        browser.assert.urlContains("ParticipantWebLoginManagementLanding");
        browser.saveScreenshot('./screenshots/ParticipantWebLoginManagementLandingPage.png')
    });

    this.Then(/^Verify Authentication Credentials Header in WebLoginManagementLanding Page$/, function (browser) {
        action.isDisplayed("WebLoginManagementPage|AuthenticationTitleinWebLoginManagementPage",function () {
        });
    });

    this.Then(/^Verify Administrative Actions Header in WebLoginManagementLanding Page$/, function (browser) {
        action.isDisplayed("WebLoginManagementPage|AdministrationActionsTitleinWebLoginManagementPage",function () {
        });
    });

    this.When(/^User Clicks on Administration Tab$/, function (browser) {
        action.performClick("HomePage|AdministrationTab",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify Application Landing Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ApplicationLandingPage|ApplicationsTitleinAdministrationLandingPage",function () {
        });
        browser.saveScreenshot('./screenshots/ApplicationLandingPage.png')
    });

    this.When(/^User Clicks on ViewLogs link$/, function (browser) {
        action.performClick("ApplicationLandingPage|ViewlogsLinkinAdministrationLandingPage",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify ViewLogs Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ApplicationLandingPage|LogViewerheader",function () {
        });
        browser.saveScreenshot('./screenshots/LogViewerPage.png')
    });


    this.When(/^User Select From Date as oldest date form the drop down$/, function (browser) {
        action.performClick("ViewLogsPage|SelectFromDate",function () {
            browser.timeoutsImplicitWait(data.shortpause);
        });
    });

    this.When(/^User Select To Date as Tomorrow's Date$/, function (browser) {
        action.performClick("ViewLogsPage|SelectToDate",function () {
            browser.timeoutsImplicitWait(data.shortpause);
        });
    });

    this.When(/^User Select Client$/, function (browser) {

        if (data.ClientName == "QAWORK") {
            action.performClick("ViewLogsPage|SelectClientforQAWORK",function () {
                browser.timeoutsImplicitWait(data.shortpause);
            });
        }
        else {
            action.performClick("ViewLogsPage|SelectClientforSLSDM",function () {
                browser.timeoutsImplicitWait(data.shortpause);
            });
        }
    });

    this.When(/^User Select Audit Messages from Filters Drop down$/, function (browser) {
        action.performClick("ViewLogsPage|SelectFiltersforAudit",function () {
            browser.timeoutsImplicitWait(data.shortpause);
        });
    });

    this.When(/^User Clicks on Search button$/, function (browser) {

        if(data.TestingEnvironment == "PROD") {
            action.performClick("ViewLogsPage|btnSearchforViewLogsPROD",function () {
                browser.pause(data.averagepause)
                browser.timeoutsImplicitWait(data.averagepause);
            });
        }
        else {
            action.performClick("ViewLogsPage|btnSearchforViewLogs",function () {
                browser.pause(data.averagepause)
                browser.timeoutsImplicitWait(data.averagepause);
            });
        }
    });


    this.When(/^Verify Logs displayed for Selected Client$/, function (browser) {
        action.getText("ViewLogsPage|LogResults", function (txt) {
            var n = txt.includes(data.ClientName);
            if (n) {
            }
            else {
                console.log("No Search Results found")
            }
            browser.saveScreenshot('./screenshots/ViewLogsSearchResults.png')
        });
    });

    this.When(/^User Clicks on Management Administrators link$/, function (browser) {
        action.performClick("ApplicationLandingPage|linkManagementAdministrators",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify Manage Administrators Page loads successfully$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ManageAdministrators|TtileManagementAdministrators",function () {
        });
        browser.saveScreenshot('./screenshots/ManageAdministratorsPage.png')
    });

    this.When(/^User Enters Administrator name in Look for field and clicks on Find now button$/, function (browser) {
        action.setText("ManageAdministrators|InputLookforfield",data.AdministratorName);
        if(data.TestingEnvironment == "PROD") {
            action.performClick("ManageAdministrators|btnFindNowPROD",function () {
                browser.timeoutsImplicitWait(data.shortwaittime);
            });
        }
        else {
            action.performClick("ManageAdministrators|btnFindNow",function () {
                browser.timeoutsImplicitWait(data.shortwaittime);
            });
        }
    });

    this.Then(/^Verify User should be able to search Administrator$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ManageAdministrators|AdministratorSearchResultslink",function () {
        });
        browser.saveScreenshot('./screenshots/AdministratorSearchResults.png')
    });

    this.When(/^User Selects Administrator$/, function (browser) {
        action.performClick("ManageAdministrators|AdministratorSearchResultslink",function () {
            browser.timeoutsImplicitWait(data.averagewaittime);
        });
    });

    this.Then(/^Verify Administration Landing Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.averagepause);
        action.isDisplayed("ManageAdministrators|ClientnameinAdministratorinLandingPage",function () {
        });
        browser.saveScreenshot('./screenshots/AdministrationLandingPage.png')
    });

    this.Then(/^Verify Groups and Roles Section in AdministratorLandingPage$/, function (browser) {
        browser.timeoutsImplicitWait(data.averagewaittime);
        action.isDisplayed("ManageAdministrators|GroupsinAdministratorinLandingPage",function () {
            action.isDisplayed("ManageAdministrators|RolesinAdministratorinLandingPage",function () {
            });
        });
    });

    this.When(/^User Selects View Details link in Features and Services Section$/, function (browser) {
        action.performClick("ClientSummaryPage|ViewDetailslinkInSummaryPage",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify Features and Services Landing Page loads Successfully$/, function (browser) {
        browser.pause(data.averagepause);
        action.isDisplayed("ClientSummaryPage|FeaturesandServicesheader",function () {
            browser.assert.urlContains("ClientServicingOptionsSummary")
        });
        browser.saveScreenshot('./screenshots/FeaturesandServicesLandingPage.png')
    });

    this.Then(/^Verify Assigned Features and Services Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|AssignedFeaturesandServicesheader",function () {
        });
    });

    this.Then(/^Verify Available Features and Services Section$/, function (browser) {
        action.isDisplayed("ClientSummaryPage|AvailableFeaturesandServicesheader",function () {
        });
    });

    this.When(/^User Clicks on External SSO Test link$/, function (browser) {
        action.performClick("ExternalSSOTestPage|ExternalSSOTestLink",function () {
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });

    this.Then(/^Verify Participant SSO To Portal Page loads successfully$/, function (browser) {
        browser.timeoutsImplicitWait(data.shortwaittime);
        action.isDisplayed("ExternalSSOTestPage|ExternalTestHarnessHeader",function () {
        });
        browser.saveScreenshot('./screenshots/ParticipantSSOToPortalPage.png')
    });

    this.When(/^User Select SSO Configuration$/, function (browser) {
        action.performClick("ExternalSSOTestPage|SelectSSOforQAWORK",function () {
            browser.pause(data.shortpause);
        });
    });

    this.When(/^User Clicks on Test SSO button$/, function (browser) {
        action.performClick("ExternalSSOTestPage|TestSSObtn",function () {
            browser.pause(data.longwaittime);
        });
    });

    this.Then(/^Verify User should be able to redirect to IBC Portal successfully$/, function (browser) {
        // browser.timeoutsImplicitWait(data.longwaittime);
        browser.window_handles(function(result) {
            browser.pause(data.averagepause);
            var handle = result.value[1];
            browser.switchWindow(handle);
            browser.saveScreenshot('./screenshots/ExternalSSOTestRedirectedpage.png')
            // browser.assert.urlContains("Home")
            browser.assert.urlContains(data.RedirectedURL)
            browser.closeWindow();
            var handle1 = result.value[0];
            browser.pause(data.longpause);
            browser.switchWindow(handle1);
        });
    });

    this.When(/^User Clicks on Impersonate button$/, function (browser) {
        action.performClick("ClientSummaryPage|Impersonatebtn",function () {
            browser.pause(data.longwaittime);
        });
    });

    this.Then(/^Verify User should be able to see end impersonation button on top right corner of the page$/, function (browser) {
        // browser.timeoutsImplicitWait(data.longwaittime);
        browser.window_handles(function(result) {
            browser.pause(data.averagepause);
            var handle = result.value[1];
            browser.switchWindow(handle);
            // var url = data.RedirectedURL
            // sVal = url.split(":")
            // console.log(sVal)
            browser.assert.urlContains(data.RedirectedURL)
            browser.saveScreenshot('./screenshots/EndImpersonationPage.png')
            browser.closeWindow();
            var handle1 = result.value[0];
            browser.switchWindow(handle1);
        });
    });


};


