var action = require('./Keywords.js');
var Objects = require(__dirname + '/../../repository/HRA_XEOS_Locators.js');
var data = require('../../TestResources/HRA_TestData.js');
//var robot = require('robot-js');

module.exports = function () {

    this.Given(/^Edits some user Information and verifies it$/, function (browser) {

        action.setText('HRAPage|inputStreet', "abc", function(){
            browser.timeoutsImplicitWait(data.shortWait);
            action.setText('HRAPage|inputCity', "xyz", function(){
                browser.timeoutsImplicitWait(data.shortWait);
                action.setText('HRAPage|inputZip', "65432", function(){
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.setText('HRAPage|inputPhone', "0123456789", function(){
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.setText('HRAPage|inputCellPhone', "9876543210", function(){
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.performClick('HRAPage|buttonSaveChanges', function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                browser.pause(data.longWait);
                                action.isDisplayed('HRAPage|messageSuccess', function () {
                                    browser.timeoutsImplicitWait(data.shortWait);
                                });
                            });
                        });
                    });
                });
            });
        });

    });

    this.Given(/^User saves and verifies the file$/, function (browser) {
        var Keyboard = robot.Keyboard;
        var k = Keyboard();
        k.click(robot.KEY_S);
        browser.pause(data.shortWait);
        k.click(robot.KEY_ENTER);
    });

};
