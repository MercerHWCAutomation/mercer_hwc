var data = require('../../TestResources/ConsultantPortalGlobalData.js');
var Objects = require(__dirname + '/../../repository/ConsultantPortalPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');

var LoginPage, DashboardPage,HrProxyPage,DocumentLibraryPage,AddClientpage;

function initializePageObjects(browser, callback) {
    var BPPage = browser.page.ConsultantPortalPages();
    LoginPage = BPPage.section.LoginPageBP;
    DashboardPage= BPPage.section.DashboardPageBP;
    HrProxyPage=BPPage.section.HrPortalProxyPage;
    DocumentLibraryPage=BPPage.section.DocLibraryPage;
    AddClientpage=BPPage.section.AddClientPage;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the browser and launch the Broker Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            URL = data.BrokerPortalURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPage.waitForElementVisible('@inputUsernameBP', data.longWait);
            });
        }
    });

    this.When(/^Enter valid username and password and click Login on Broker Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            LoginPage.waitForElementVisible('@inputPasswordBP', data.longWait)
                .setValue('@inputUsernameBP',data.BrokerPortalUsername)
                .setValue('@inputPasswordBP',data.BrokerPortalPassword)
                .click('@ContinueButtonLoginBP');
        }
    });

    this.Then(/^Broker Portal Dashboard should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementVisible('@DashboardTitleBP', data.longWait)
        }
    });

    this.Given(/^User is on Broker Portal Dashboard Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementVisible('@DashboardTitleBP', data.longWait)
        }
    });

    this.When(/^User clicks on the Education Tag from Resources Tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

            //  action.initializePageObjects(browser, function () {
            //      action.performClick("DashboardPage|ResourcesTab", function () {
            //          action.performClick("DashboardPage|EducationTab", function () {
//
            //          })
            //      })
            //  })

            DashboardPage.waitForElementVisible('@ResourcesTab', data.longWait)
                .click('@ResourcesTab');
            browser.url(data.EducationURL);
            browser.pause(data.LoginWait);


            // DashboardPage.waitForElementVisible('@ResourcesTab',data.LoginWait);
            //   // moveToElement can also accept offsets
            //     DashboardPage.click('@ResourcesTab');
            //     DashboardPage.moveToElement('@EducationTab', 0, 0, function() {
            //     DashboardPage.click('@EducationTab');
            //     });

            // browser.waitForElementVisible("//a[contains(text(),'Resources')]", 1000, function () {
            //     // moveToElement can also accept offsets
            //     browser.moveToElement("//a[contains(text(),'Resources')]", 100, 100, function() {
            //         browser.waitForElementVisible("//a[contains(text(),'Resources')]/../ul/li[2]/a", 500, function () {
            //             browser.click("//a[contains(text(),'Resources')]/../ul/li[2]/a");
            //         }, "Click share icon. ");
            //     });
            // }, "Find a recommendation ");
        }
    });

    this.Then(/^User should able to check the Video Page is Opened$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
            });
            //var test= browser.getUrl()
            //console.log(test);
            browser.pause(this.LoginWait);
        }
    });

    this.Then(/^User opens the active clients list$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@ActiveClientsLink',data.LoginWait)
                .click('@ActiveClientsLink')
                .waitForElementPresent('@ViewActiveClientsDetails',data.LoginWait)
                .click('@ViewActiveClientsDetails')
                .waitForElementPresent('@FirstActiveClientDetails',data.LoginWait);
        }
    });

    this.Then(/^User opens HR Admin Proxy for an active clients$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@FirstActiveClientDetails',data.LoginWait)
                .click('@FirstActiveClientDetails')
                .waitForElementPresent('@AccessHRPotalTab',data.LoginWait)
                .click('@AccessHRPotalTab');

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });

        }
    });

    this.Then(/^User should able to access HR Proxy site$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            HrProxyPage.waitForElementPresent('@EndImpersonationButton',data.LoginWait)
                .click('@EndImpersonationButton');

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[0];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });

        }
    });

    this.Then(/^User get backs to Broker Application once HR Proxy site closed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@AccessHRPotalTab',data.LoginWait)
        }
    });

    this.Then(/^User opens Ready client list$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@DashboardLink',data.LoginWait)
                .click('@DashboardLink')
                .waitForElementPresent('@ReadyClientsButton',data.LoginWait)
                .click('@ReadyClientsButton')
                .waitForElementPresent('@FirstReadyClientDetails',data.LoginWait)
        }
    });

    this.Then(/^User opens HR Admin Proxy for an Ready clients$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@FirstReadyClientDetails',data.LoginWait)
                .click('@FirstReadyClientDetails')
                .waitForElementPresent('@AccessHRPotalTab',data.LoginWait)
                .click('@AccessHRPotalTab');

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });
        }
    });

    this.Then(/^User should able to access HR Proxy site for Ready Client$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            HrProxyPage.waitForElementPresent('@EndImpersonationButton',data.LoginWait)
                .click('@EndImpersonationButton');

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[0];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });

        }
    });

    this.When(/^User opens the Document Library Page and checks the default view$/, function () {
        var URL;
        //var xpath="//a[text()='Resources']";
        //var xpath1="(//a[text()='Education'])[1]";
        //When   User selects a file and it opens in new window as a PDF
        //And    User able to download ZIP file and able to open it
        //And    User select the files and switch modes
        //Then   User should able to perform Sorting Function on files
        browser = this;
        var execEnv = data["TestingEnvironment"];
        //browser.useXpath().moveToElement(xpath,0,1).pause(2000);
        //browser.click(xpath1);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@DocumentLibraryQuicklink',data.LoginWait)
                .click('@DocumentLibraryQuicklink');
            DocumentLibraryPage.waitForElementPresent('@DocLibTitle',data.LoginWait)
                .waitForElementPresent('@SolutionSupportTab',data.LoginWait)
                .waitForElementPresent('@SalesTab',data.LoginWait)
                .click('@SalesTab')
                .waitForElementPresent('@SalesTabHeader',data.LoginWait)
                .waitForElementPresent('@MarketingTab',data.LoginWait)
                .click('@MarketingTab')
                .waitForElementPresent('@MarketingTabHeader',data.LoginWait)
                .waitForElementPresent('@ClientSetupTab',data.LoginWait)
                .click('@ClientSetupTab')
                .waitForElementPresent('@ClientTabHeader',data.LoginWait)
                .waitForElementPresent('@OngoingSupportTab',data.LoginWait)
                .click('@OngoingSupportTab')
                .waitForElementPresent('@OngoingSupportHeader',data.LoginWait);
        }
    });

    this.When(/^User selects a file and it opens in new window as a PDF$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DocumentLibraryPage.click('@MarketingTab')
                .waitForElementPresent('@MarketingTabHeader',data.LoginWait)
                .waitForElementPresent('@FirstDocLink',data.LoginWait)
                .click('@FirstDocLink');
            this.pause(data.LoginWait)

            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
            });
            DocumentLibraryPage.waitForElementPresent('@PDFTab',data.LoginWait)
            browser.closeWindow();
            this.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[0];
                browser.switchWindow(handle);
            });

        }
    });

    this.When(/^User able to download ZIP file and able to open it$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DocumentLibraryPage.click('@ClientSetupTab')
                .waitForElementPresent('@ClientTabHeader',data.LoginWait)
                .waitForElementPresent('@AddToZip',data.LoginWait)
                .click('@AddToZip')
                .waitForElementPresent('@AddedToZip',data.LoginWait)
                .waitForElementPresent('@DownloadSelectedTab',data.LoginWait)
                .click('@DownloadSelectedTab');
            browser.pause(data.shortWait);

            //var path = 'C:/Health_Automation/Downloads';
            //path.exists('download.zip', function(exists) {
            //    if (exists) {
            //       console.log('File Downloaded Successfully');
            //    }
            //});

            //if (fs.statSync('C:/Users/subhajit-chakraborty/Downloads/documents')) {
            //    console.log('Found file');
            //}
            //else {
            //    console.log('File not Found')
            //}

            fs.stat('C:/Users/subhajit-chakraborty/Downloads/documents', function(err, stat) {
                if(err == null) {
                    console.log('Exists');
                } else {
                    console.log('Exists not');
                }
            });



        }
    });

    this.When(/^User select the files and switch modes$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DocumentLibraryPage.waitForElementPresent('@ListViewTab',data.LoginWait)
                .click('@ListViewTab')
                .waitForElementPresent('@AddedToZipCheckedListView',data.LoginWait);

        }
    });

    this.Then(/^User should able to perform Sorting Function on files$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DocumentLibraryPage.waitForElementPresent('@DescendingSearchLink',data.LoginWait)
                .click('@DescendingSearchLink')
                .waitForElementPresent('@AscendingSearchLink',data.LoginWait)
                .click('@AscendingSearchLink');
        }
    });

    this.Then(/^User able to click logout button$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

            var xpath="//div[@class='user-pic small']";
            var xpath1="//a[contains(text(),'Log Out')]";
            browser.useXpath().moveToElement(xpath,0,1).pause(3000);
            browser.click(xpath1);
            browser.pause(5000);
        }
    });

    this.Then(/^User should get logged out successfully$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

            LoginPage.waitForElementPresent('@inputUsernameBP',data.LoginWait);
        }
    });

    this.When(/^Pending and Active clients tabs should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

            DashboardPage.waitForElementPresent('@ActiveClientsLink',data.LoginWait)
                .waitForElementPresent('@PendingClientLink',data.LoginWait)
                .waitForElementPresent('@PendingClientStatusText',data.LoginWait)
                .waitForElementPresent('@ViewPendingClientsDetails',data.LoginWait)
                .waitForElementPresent('@AddANewClientTab',data.LoginWait)
                .waitForElementPresent('@QuicklinksHeader',data.LoginWait)
                .click('@ActiveClientsLink')
                .waitForElementPresent('@ViewActiveClientsDetails',data.LoginWait)
                .waitForElementPresent('@AddANewClientTab',data.LoginWait)
                .waitForElementPresent('@QuicklinksHeader',data.LoginWait)
                .waitForElementPresent('@ActiveClientStatusText',data.LoginWait);
        }
    });

    this.Then(/^Other Links on the Dashboard Pages should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPage.waitForElementPresent('@DashboardLinkTab',data.LoginWait)
                .waitForElementPresent('@ClientsLinkTab',data.LoginWait)
                .waitForElementPresent('@ResourcesTab',data.LoginWait);
        }
    });

    this.When(/^User opens and fills Client Profile Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

            //User clicks on the Client Start Button
            DashboardPage.waitForElementPresent('@AddANewClientTab',data.LoginWait)
                .click('@AddANewClientTab');
            AddClientpage.waitForElementPresent('@StartNowButton',data.LoginWait)
                .click('@StartNowButton')

                //enter client profile 1st 3 details:
                .waitForElementPresent('@ClientNameTextbox',data.LoginWait)
                .sendKeys('@ClientNameTextbox',data.ClientName)
                .click('@ClientIndustryDropdown')
                .waitForElementPresent('@AgricultureForestryOption',data.LoginWait)
                .click('@AgricultureForestryOption')
                .waitForElementPresent('@EmployerEntityDropdown',data.LoginWait)
                .click('@EmployerEntityDropdown')
                .waitForElementPresent('@CorporationOption',data.LoginWait)
                .click('@CorporationOption');

            //client US Headquarter Details:
            AddClientpage.waitForElementPresent('@AddressLine1USClientHeadquarters',data.LoginWait)
                .setValue('@AddressLine1USClientHeadquarters',data.Address1USHeadQuarter)
                .waitForElementPresent('@CityUSClientHeadquarters',data.LoginWait)
                .setValue('@CityUSClientHeadquarters',data.CityUSHeadquarter)
                .waitForElementPresent('@ZipUSClientHeadquarters',data.LoginWait)
                .setValue('@ZipUSClientHeadquarters',data.ZipCodeUSHeadquarter)
                .waitForElementPresent('@StateUSClientHeadquarterDropdown',data.LoginWait)
                .click('@StateUSClientHeadquarterDropdown')
                .waitForElementPresent('@StateDropdownOption1',data.LoginWait)
                .click('@StateDropdownOption1');

            //primary Contact HR Information
            AddClientpage.waitForElementPresent('@ContactNameHR',data.LoginWait)
                .setValue('@ContactNameHR',data.HRName)
                .waitForElementPresent('@EmailHR',data.LoginWait)
                .setValue('@EmailHR',data.HREmail)
                .waitForElementPresent('@ContactHR',data.LoginWait)
                .setValue('@ContactHR',data.HRContactNumber);

            //click on the continue button
            AddClientpage.waitForElementPresent('@ContactNameHR',data.LoginWait)
                .click('@ContinueButton1ClientProfile');

            //few more pieces of information page
            AddClientpage.waitForElementPresent('@EnrollmentStartDate',data.LoginWait)
                .setValue('@EnrollmentStartDate',data.EnrollmentStartDate)
                .waitForElementPresent('@PlanYearEffectiveDate',data.LoginWait)
                .setValue('@PlanYearEffectiveDate',data.PlanYearEffDate)
                .waitForElementPresent('@LengthOpenEnrollmentDropdown',data.LoginWait)
                .click('@LengthOpenEnrollmentDropdown')
                .waitForElementPresent('@OneWeekOptionOEDropdown',data.LoginWait)
                .click('@OneWeekOptionOEDropdown')
                .waitForElementPresent('@FederalTaxID',data.LoginWait)
                .setValue('@FederalTaxID',data.FederalTaxID)
                .waitForElementPresent('@ClientOneCode',data.LoginWait)
                .setValue('@ClientOneCode',data.ClientOneCode)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton');
        }
    });

    this.When(/^User able to navigates and fills Plan decision Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            AddClientpage.waitForElementPresent('@PlanDecisionTitle',data.ProfileCreationTime)
            AddClientpage.waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
           //     browser.pause(15000)
           // AddClientpage.waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
           //     .click('@SaveAndContinueButton');
        }
    });

    this.When(/^User able to navigates and fills Benefits Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            AddClientpage.waitForElementPresent('@BenefitPageTitle',data.LoginWait)

            //Filling the Medical Get Started Option
                .waitForElementPresent('@GetStartedMedical',data.LoginWait)
                .click('@GetStartedMedical')
                .waitForElementPresent('@MedicalCarrierDropdown',data.LoginWait)
                .click('@MedicalCarrierDropdown')
                .waitForElementPresent('@AetnaDropdownOption',data.LoginWait)
                .click('@AetnaDropdownOption')
                .click('@ToggleArrow')
                .waitForElementPresent('@MedicalPlanOfferedOption1',data.LoginWait)
                .click('@MedicalPlanOfferedOption1')
                .waitForElementPresent('@MedicalPlanOfferedOption2',data.LoginWait)
                .click('@MedicalPlanOfferedOption2')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                //Filling the Supplemental Header Option
                .waitForElementPresent('@SupplementalHeader',data.LoginWait)
                .waitForElementPresent('@SupplementalCarrierDropdown',data.LoginWait)
                .click('@SupplementalCarrierDropdown')
                .waitForElementPresent('@AlfacDropdownOption1',data.LoginWait)
                .click('@AlfacDropdownOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@AccidentInsuranceTitle',data.LoginWait)               //Accident
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@CriticalIllnessInsuranceTitle',data.LoginWait)        //critical Illness
                .waitForElementPresent('@CriticalIllnessOfferedOption1',data.LoginWait)
                .click('@CriticalIllnessOfferedOption1')
                .waitForElementPresent('@CriticalIllnessOfferedOption2',data.LoginWait)
                .click('@CriticalIllnessOfferedOption2')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@HospitalIndemnityInsuranceTitle',data.LoginWait)               //Hospital Indemnity
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                //filling the Dental Header Option
                .waitForElementPresent('@DentalHeader',data.LoginWait)
                .waitForElementPresent('@DentalCarrierDropdown',data.LoginWait)
                .click('@DentalCarrierDropdown')
                .waitForElementPresent('@AetnaDentalOptionCarrier',data.LoginWait)
                .click('@AetnaDentalOptionCarrier')
                .click('@ToggleArrow')
                .waitForElementPresent('@DentalPlanOfferedOption1',data.LoginWait)
                .click('@DentalPlanOfferedOption1')
                .waitForElementPresent('@DentalPlanOfferedOption2',data.LoginWait)
                .click('@DentalPlanOfferedOption2')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                //filling the Vision Header Option
                .waitForElementPresent('@VisionHeader',data.LoginWait)
                .waitForElementPresent('@VisionCarrierDropdown',data.LoginWait)
                .click('@VisionCarrierDropdown')
                .waitForElementPresent('@AetnaOptionCarrier',data.LoginWait)
                .click('@AetnaOptionCarrier')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                //filling for the Life header option
                .waitForElementPresent('@LifeHeader',data.LoginWait)
                .waitForElementPresent('@LifeCarrierDropdown',data.LoginWait)
                .click('@LifeCarrierDropdown')
                .waitForElementPresent('@AIGOptionCarrierLife',data.LoginWait)
                .click('@AIGOptionCarrierLife')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@BasicLIHeader',data.LoginWait)     //Basic Life Employee Insurance(one plan only)
                .waitForElementPresent('@BasicLIOption1',data.LoginWait)
                .click('@BasicLIOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@VoluntaryLIHeader',data.LoginWait)     //Voluntary Life Insurance(one plan only)
                .waitForElementPresent('@VoluntaryLIOption1',data.LoginWait)
                .click('@VoluntaryLIOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@SpouseLIHeader',data.LoginWait)     //Spouse Life Insurance(two plans)
                .waitForElementPresent('@SpouseLIOption1',data.LoginWait)
                .click('@SpouseLIOption1')
                .waitForElementPresent('@SpouseLIOption2',data.LoginWait)
                .click('@SpouseLIOption2')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@ChildLIHeader',data.LoginWait)     //child Life Insurance(default selected)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@VoluntaryADDHeader',data.LoginWait)     //Voluntary AD&D(two plans)
                .waitForElementPresent('@VoluntaryADDOption1',data.LoginWait)
                .click('@VoluntaryADDOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton');

                //filling for the disability option
            AddClientpage.waitForElementPresent('@DisabilityHeader',data.LoginWait)
                .waitForElementPresent('@DisabilityOption1',data.LoginWait)
                .click('@DisabilityOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                //filling the protection option
                .waitForElementPresent('@ProtectionHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@IdentityTheftHeader',data.LoginWait)       //Identity Theft Page
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton');
        }
    });

    this.When(/^User able to navigate and fills the Contribution Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            AddClientpage.waitForElementPresent('@ContributionHeader',data.LoginWait)
                .waitForElementPresent('@ContributionTypeDropdown',data.LoginWait)
                .click('@ContributionTypeDropdown')
                .waitForElementPresent('@Option1ContributionType',data.LoginWait)
                .click('@Option1ContributionType')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
        }
    });

    this.When(/^User able to navigate and fills the Rates Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            AddClientpage.waitForElementPresent('@RatesHeader',data.LoginWait)
                .waitForElementPresent('@QuestionareHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@WorksheetHeader',data.LoginWait)
                .waitForElementPresent('@ContinueToOnscreenEntry',data.LoginWait)
                .click('@ContinueToOnscreenEntry')
                .waitForElementPresent('@EnterAnnualRateHeader',data.LoginWait);

            //    (//input[@type='text'])[1]

            //for medical fill all coloumns
           for(var i=1;i<17;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.LoginWait)
            AddClientpage.waitForElementVisible('@SupplementalTab',data.LoginWait)
                .click('@SupplementalTab')
                .waitForElementVisible('@AccidentInsuranceTab',data.LoginWait)

            //to fill accident Insurance
            for(var i=1;i<9;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.AverageWait)

            //to fill Critical Illness insurance
            AddClientpage.waitForElementVisible('@CriticalIllnessInsuranceTab',data.LoginWait)
                .click('@CriticalIllnessInsuranceTab')
                .waitForElementVisible('@LowTab',data.LoginWait)
            for(var i=9;i<17;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //to fill hospital indemnity tab
            AddClientpage.waitForElementVisible('@HospitalIndemnityTab',data.LoginWait)
                .click('@HospitalIndemnityTab')
                browser.pause(data.longWait)
            for(var i=17;i<25;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //To fill the dental options:
            AddClientpage.waitForElementVisible('@DentalTab',data.LoginWait)
                .click('@DentalTab')
                .waitForElementVisible('@StandardPlanHeader',data.LoginWait)
            for(var i=1;i<17;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //To fill the Vision options:
            AddClientpage.waitForElementVisible('@VisionTab',data.LoginWait)
                .click('@VisionTab')
                .waitForElementVisible('@VisionPlanHeader',data.LoginWait)
            for(var i=1;i<9;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //To fill the Life options:

            //to fill Basic Employee Life Insurance
            AddClientpage.waitForElementVisible('@LifeTab',data.LoginWait)
                .click('@LifeTab')
                .waitForElementVisible('@BasicEmpLIHeader',data.LoginWait)
            for(var i=1;i<3;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //to fill Voluntary Life Insurance
            AddClientpage.waitForElementVisible('@VoluntaryLITab',data.LoginWait)
                .click('@VoluntaryLITab')
            browser.pause(data.longWait)
            for(var i=3;i<5;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //to fill Spouse Life Insurance
            AddClientpage.waitForElementVisible('@SpouseLITab',data.LoginWait)
                .click('@SpouseLITab')
            browser.pause(data.longWait)
            for(var i=5;i<9;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //to fill Child Life Insurance
            AddClientpage.waitForElementVisible('@ChildLITab',data.LoginWait)
                .click('@ChildLITab')
            browser.pause(data.longWait)
            for(var i=9;i<11;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //to fill Voluntary AD&D
            AddClientpage.waitForElementVisible('@VoluntaryAddTab',data.LoginWait)
                .click('@VoluntaryAddTab')
            browser.pause(data.longWait)
            for(var i=11;i<15;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //Filling the Disability Tab
            AddClientpage.waitForElementVisible('@DisablityTab',data.LoginWait)
                .click('@DisablityTab')
                .waitForElementVisible('@DisabilityHeaderTab',data.LoginWait)
            for(var i=1;i<3;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //Filling the Protection Tab
            AddClientpage.waitForElementVisible('@ProtectionTab',data.LoginWait)
                .click('@ProtectionTab')
                .waitForElementVisible('@IdentityTheftTab',data.LoginWait)

            //fill Leagal Assistant
            for(var i=1;i<3;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //fill Identity theft
            AddClientpage.waitForElementVisible('@IdentityTheftTab',data.LoginWait)
                .click('@IdentityTheftTab')
            browser.pause(data.longWait)
            for(var i=3;i<7;i++)
            {
                browser.useXpath().setValue("(//input[@type='text'])["+i+"]",1)
                browser.pause(100);
            }
            AddClientpage.waitForElementVisible('@SaveChangesButton',data.LoginWait)
                .click('@SaveChangesButton')
            browser.pause(data.longWait)

            //Final Save
            AddClientpage.waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')


















            browser.pause(15000);
           //     .click('@UploadWorksheetLink')
           //     .waitForElementPresent('@ChooseFileWorksheet',data.LoginWait)
           //     .click('@ChooseFileWorksheet');
           // browser.pause(15000);

            //browser.setValue('input[type="file"]',require('path').resolve('C:/Users/subhajit-chakraborty/Desktop/rates_8108.xlsx'));
            ////browser.pause(5000);
            //// ks.sendKeys('enter');
//
            ////Adding rates to the medical Plans:
            //// action.UploadFile("C:/Users/subhajit-chakraborty/Desktop/rates_8108",function (result) {
            ////     console.log("File Uploaded"+result);
            //// });
            //AddClientpage.waitForElementPresent('@UploadRatesWorksheet',data.LoginWait)
            //    .click('@UploadRatesWorksheet')
            //    .waitForElementNotPresent('@InternalServerError',data.LoginWait)
            //    .waitForElementPresent('@RatesSuccessMsg',data.LoginWait)
            //    .waitForElementPresent('@ContinueSuccessMsgButton',data.LoginWait)
            //    .click('@ContinueSuccessMsgButton')
            //browser.pause(data.LoginWait);






        }
    });

    this.When(/^User able to navigate and fills the Adminstration Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            AddClientpage.waitForElementPresent('@EmployeeEventsHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@NewHireHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@RehireHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@TerminationHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@OtherEmpEventsHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@PayrollHeader',data.LoginWait)             //Paroll Tabs Validations
                .waitForElementPresent('@PayrollTemplateoption',data.LoginWait)
                .click('@PayrollTemplateoption')
                .waitForElementPresent('@PayrollTempOption1',data.LoginWait)
                .click('@PayrollTempOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@PayrollFreqHeader',data.LoginWait)
                .waitForElementPresent('@WeeklyOption',data.LoginWait)
                .click('@WeeklyOption')
                .waitForElementPresent('@WeeklyPayrolldate',data.LoginWait)
                .setValue('@WeeklyPayrolldate',data.WeeklyDate)
                .waitForElementPresent('@SaveAndContAdminstrationPage',data.LoginWait)
                .click('@SaveAndContAdminstrationPage')

                .waitForElementPresent('@ContinueButton',data.LoginWait)        //confirmation Continue
                .click('@ContinueButton')

                .waitForElementPresent('@ClientLogoHeader',data.LoginWait)      //Employee Websites
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@MessagesHeader',data.LoginWait)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')
                .waitForElementPresent('@DocumentsHeader',data.LoginWait)
                .waitForElementPresent('@ContinueTabDocuments',data.LoginWait)
                .click('@ContinueTabDocuments')

                .waitForElementPresent('@COBRAHeader',data.LoginWait)           //Cobra
                .waitForElementPresent('@EnrolledCOBRAMemtextbox',data.LoginWait)
                .setValue('@EnrolledCOBRAMemtextbox',data.EnrolledCobraMem)
                .waitForElementPresent('@PendingCOBRAMemtextbox',data.LoginWait)
                .setValue('@PendingCOBRAMemtextbox',data.PendingCobraMember)
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton')

                .waitForElementPresent('@CallCenterHeader',data.LoginWait)          //call center
                .waitForElementPresent('@CallCenterDropdown',data.LoginWait)
                .click('@CallCenterDropdown')
                .waitForElementPresent('@CallCenterOption1',data.LoginWait)
                .click('@CallCenterOption1')
                .waitForElementPresent('@SaveAndContinueButton',data.LoginWait)
                .click('@SaveAndContinueButton');
        }
    });

    this.When(/^User able to navigate and fills Employee Data Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            AddClientpage.waitForElementPresent('@EmployeeDataHeader',data.LoginWait)
                .waitForElementPresent('@NextButton',data.LoginWait)
                .click('@NextButton')
                .waitForElementPresent('@ChooseFileEmpData',data.LoginWait)
                .click('@ChooseFileEmpData');
            browser.pause(5000);
            browser.setValue('input[type="file"]',require('path').resolve('C:/Users/subhajit-chakraborty/Desktop/Sprint22EmployeeDataFile1.xlsx'));
            AddClientpage.waitForElementPresent('@UploadFileButton',data.LoginWait)
                .click('@UploadFileButton')
            browser.pause(10000);








        }
    });

    this.When(/^User able to navigate and fills Account Structure Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

        }
    });

    this.When(/^User able to navigate and fills the Validation Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

        }
    });

    this.When(/^User able to successfully complete the setup file$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {

        }
    });

    this.Then(/^User able to send mail$/, function () {

        var sendmail = require('sendmail')({
            logger: {
                debug: console.log,
                info: console.info,
                warn: console.warn,
                error: console.error
            },
            silent: false,
           //dkim: { // Default: False
           //    privateKey: fs.readFileSync('./dkim-private.pem', 'utf8'),
           //    keySelector: 'mydomainkey'
           //},
            devPort: 1025 // Default: False
        })

        var sendmail1 = require('sendmail')();

        sendmail1({
            from: 'no-reply@yourdomain.com',
            to: 'subhajit.chakraborty@mercer.com',
            subject: 'test sendmail',
            html: 'Mail of test sendmail ',
        }, function(err, reply) {
            console.log(err && err.stack);
            console.dir(reply);
        });


    });



}