/**
 * Created by subhajit-chakraborty on 9/12/2017.
 */

var data = require('../../TestResources/VIPAdminData.js');
var BrowserData = require('../../TestResources/GlobalTestData.js');
var Objects = require(__dirname + '/../../repository/VIPAdminPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');

var ClientListPage,WebConfigPage,EpRulesPage,ClientSettingPage,BestMatchPage,PromotePage,BPOCarrierDescPage;

function initializePageObjects(browser, callback) {
    var ToolkitPage = browser.page.AdminToolkitPages();
    ClientListPage = ToolkitPage.section.ClientListPage;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the browser and launch the AdminTool kit URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        console.log('Browser: ' + BrowserData.BrowserInTest);
        if (execEnv.toUpperCase() == "QASANDBOX"|| execEnv.toUpperCase() == "QAINT") {
            URL = data.AdminToolkitURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                ClientListPage.waitForElementVisible('@BenefitsHubHeader', data.longWait)
                    .waitForElementVisible('@ClientListHeader', data.longWait)
                    .waitForElementVisible('@FirstClientListName', data.longWait)
            });
        }
    });





}